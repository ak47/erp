unit ufrmEntryStorage;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs,ufrmBaseController, ufrmNewBaseStorage,
  Vcl.ExtCtrls, RzPanel, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2013White, dxSkinOffice2016Colorful,
  dxSkinOffice2016Dark, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, Data.DB, cxDBData, cxTextEdit, cxCheckBox,
  cxCalendar, cxDropDownEdit, cxDBLookupComboBox, cxMemo, cxCurrencyEdit,
  cxSpinEdit, cxContainer, Vcl.Menus, cxLookupEdit, cxDBLookupEdit, cxDBEdit,
  Vcl.StdCtrls, cxButtons, cxLabel, cxMaskEdit, cxGroupBox, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses,
  cxGridCustomView, cxGrid, RzButton, ufrmBasePurchase,
  Datasnap.DBClient;

type
  TfrmEntryStorage = class( TfrmNewBaseStorage )
    procedure MasterAfterInsert(DataSet: TDataSet);
  private
    { Private declarations }
    procedure WndProc(var Message: TMessage); override;  // 第一优先权
  public
    { Public declarations }
  end;

var
  frmEntryStorage: TfrmEntryStorage;

implementation

uses
   uDataModule,ufunctions,global;

{$R *.dfm}

procedure TfrmEntryStorage.MasterAfterInsert(DataSet: TDataSet);
begin
  inherited;
  with DataSet do Self.tvViewColumn16.EditValue := '进库';
end;

procedure TfrmEntryStorage.WndProc(var Message: TMessage);
var
  pMsg : PProject;
  szCodeList : string;
  szTableList : array[0..1] of TTableList;
  szColName : string;

begin
  case Message.Msg of
    WM_FrameClose:
    begin

    end;
    WM_FrameView :
    begin
      pMsg := PProject(Message.LParam);
      if Assigned(pMsg) then
      begin
        g_PostCode    := pMsg.dwCode;
        g_ProjectName := pMsg.dwName;
        Self.RzPanel5.Caption := g_ProjectName;

        szColName := Self.tvViewColumn3.DataBinding.FieldName;
        with DM do
        begin
          ADOQuery1.Close;
          ADOQuery1.SQL.Text := 'Select pt.SignName AS Name,pycode from '+ g_Table_Maintain_Science +
                    ' AS PT left join '+ g_Table_CompanyManage +
                    ' AS PS on PT.SignName = PS.SignName Where ProjectName="'+
                    g_ProjectName +
                    '" group by pt.SignName,pycode';
          ADOQuery1.Open;
          Self.ADOSignName.Data  := DataSetProvider1.Data;
          Self.ADOSignName.Open;

          ADOQuery1.Close;
          ADOQuery1.SQL.Text := dwSQLtext;
          ADOQuery1.Open;
          Self.Master.Data := DataSetProvider1.Data;
          Self.Master.Open;

          ADOQuery1.Close;
          ADOQuery1.SQL.Text := 'Select * from '+ g_Table_MakingsList;// + ' Group BY spec';
          ADOQuery1.Open;
          Self.ADOSpec.Data := DataSetProvider1.Data;
          {
          with Self.ADOSpec do
          begin
            Close;
            SQL.Clear;
            SQL.Text := 'Select spec from '+ g_Table_MakingsList + ' Group BY spec';
            Open;
          end;
          }
        end;

      end;

    end;
    WM_FrameDele :
    begin

    end;
  end;
  // 一定要加上这句，否则编译通不过。因为绝大部分消息没人处理了
  inherited WndProc(Message); // 会一路向上调用，直到TControl.WndProc调用Dispatch来寻找消息处理函数
end;


end.
