object Form3: TForm3
  Left = 0
  Top = 0
  Caption = #37319#36141#27979#24335
  ClientHeight = 530
  ClientWidth = 1068
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  WindowState = wsMaximized
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter
    Left = 274
    Top = 0
    Width = 10
    Height = 530
    Color = 16250613
    ParentColor = False
    ResizeStyle = rsUpdate
    ExplicitTop = -62
    ExplicitHeight = 592
  end
  object RzPanel2: TRzPanel
    Left = 0
    Top = 0
    Width = 274
    Height = 530
    Align = alLeft
    BorderInner = fsFlat
    BorderOuter = fsNone
    BorderSides = [sdLeft, sdRight]
    TabOrder = 0
  end
  object RzPanel1: TRzPanel
    Left = 284
    Top = 0
    Width = 784
    Height = 530
    Align = alClient
    BorderOuter = fsFlat
    BorderSides = [sdLeft, sdRight]
    TabOrder = 1
    object RzToolbar13: TRzToolbar
      Left = 1
      Top = 0
      Width = 782
      Height = 29
      AutoStyle = False
      Images = DM.cxImageList1
      BorderInner = fsNone
      BorderOuter = fsNone
      BorderSides = [sdBottom]
      BorderWidth = 0
      GradientColorStyle = gcsCustom
      TabOrder = 0
      VisualStyle = vsGradient
      ToolbarControls = (
        RzSpacer30
        RzToolButton90
        RzSpacer120
        RzToolButton85
        RzSpacer113
        btnGridSet
        RzSpacer10
        RzToolButton87
        RzSpacer1
        RzToolButton88
        RzToolButton1
        RzToolButton2)
      object RzSpacer30: TRzSpacer
        Left = 4
        Top = 2
      end
      object RzToolButton85: TRzToolButton
        Left = 82
        Top = 2
        Width = 62
        SelectionColorStop = 16119543
        SelectionFrameColor = clBtnShadow
        ImageIndex = 2
        ShowCaption = True
        UseToolbarButtonSize = False
        UseToolbarShowCaption = False
        Caption = #21024#38500
      end
      object RzSpacer113: TRzSpacer
        Left = 144
        Top = 2
      end
      object RzToolButton87: TRzToolButton
        Left = 236
        Top = 2
        Width = 62
        SelectionColorStop = 16119543
        SelectionFrameColor = clBtnShadow
        ImageIndex = 5
        ShowCaption = True
        UseToolbarButtonSize = False
        UseToolbarShowCaption = False
        ToolStyle = tsDropDown
        Caption = #25253#34920
      end
      object RzToolButton88: TRzToolButton
        Left = 306
        Top = 2
        Width = 62
        SelectionColorStop = 16119543
        SelectionFrameColor = clBtnShadow
        ImageIndex = 8
        ShowCaption = True
        UseToolbarButtonSize = False
        UseToolbarShowCaption = False
        Caption = #36864#20986
        OnClick = RzToolButton88Click
      end
      object RzToolButton90: TRzToolButton
        Left = 12
        Top = 2
        Width = 62
        SelectionColorStop = 16119543
        SelectionFrameColor = clBtnShadow
        ImageIndex = 3
        ShowCaption = True
        UseToolbarButtonSize = False
        UseToolbarShowCaption = False
        Caption = #20445#23384
        OnClick = RzToolButton90Click
      end
      object RzSpacer120: TRzSpacer
        Left = 74
        Top = 2
      end
      object RzSpacer1: TRzSpacer
        Left = 298
        Top = 2
      end
      object RzSpacer10: TRzSpacer
        Left = 228
        Top = 2
      end
      object btnGridSet: TRzToolButton
        Left = 152
        Top = 2
        Width = 76
        ImageIndex = 4
        ShowCaption = True
        UseToolbarShowCaption = False
        Caption = #34920#26684#35774#32622
      end
      object RzToolButton1: TRzToolButton
        Left = 368
        Top = 2
        Width = 36
        ShowCaption = True
        UseToolbarShowCaption = False
        Caption = #26032#22686
        OnClick = RzToolButton1Click
      end
      object RzToolButton2: TRzToolButton
        Left = 404
        Top = 2
        Width = 36
        ShowCaption = True
        UseToolbarShowCaption = False
        Caption = #21047#26032
        OnClick = RzToolButton2Click
      end
    end
    object Grid: TcxGrid
      Left = 1
      Top = 29
      Width = 782
      Height = 501
      Align = alClient
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      LookAndFeel.SkinName = 'Office2016Colorful'
      object tvView: TcxGridDBTableView
        Navigator.Buttons.CustomButtons = <>
        DataController.DataSource = ds
        DataController.Summary.DefaultGroupSummaryItems = <
          item
            Format = #21512#35745
            Position = spFooter
            Column = tvViewColumn1
          end
          item
            Format = #25968#20540':0.########;-0.########'
            Kind = skSum
          end
          item
            Format = #25968#20540':0.########;-0.########'
            Kind = skSum
            Position = spFooter
          end
          item
            Format = #25968#37327':0.###;-0.###'
            Kind = skSum
          end
          item
            Format = #25968#37327':0.###;-0.###'
            Kind = skSum
            Position = spFooter
          end
          item
            Format = #37329#39069':'#165',0.00;'#165'-,0.00'
            Kind = skSum
            Position = spFooter
          end
          item
            Format = #37329#39069':'#165',0.00;'#165'-,0.00'
            Kind = skSum
          end>
        DataController.Summary.FooterSummaryItems = <
          item
            Format = #37329#39069':'#165',0.00;'#165'-,0.00'
            Kind = skSum
          end
          item
            Format = #21512#35745#65306
            Kind = skCount
            Column = tvViewColumn1
            Sorted = True
          end
          item
            Format = #25968#20540':0.########;-0.########'
            Kind = skSum
          end
          item
            Format = #25968#37327':0.###;-0.###'
            Kind = skSum
          end
          item
            Format = #165',0.00;'#165'-,0.00'
            Kind = skSum
          end
          item
            Format = #22823#20889':'
            Kind = skCount
          end>
        DataController.Summary.SummaryGroups = <>
        FilterRow.InfoText = #21333#20987#31579#36873#26597#35810
        FilterRow.Visible = True
        OptionsBehavior.FocusCellOnTab = True
        OptionsBehavior.FocusFirstCellOnNewRecord = True
        OptionsBehavior.GoToNextCellOnEnter = True
        OptionsBehavior.FocusCellOnCycle = True
        OptionsCustomize.ColumnsQuickCustomization = True
        OptionsCustomize.DataRowSizing = True
        OptionsData.Deleting = False
        OptionsData.DeletingConfirmation = False
        OptionsSelection.InvertSelect = False
        OptionsSelection.MultiSelect = True
        OptionsSelection.CellMultiSelect = True
        OptionsView.NoDataToDisplayInfoText = '<'#27809#26377#20219#20309#25968#25454#35760#24405'>'
        OptionsView.DataRowHeight = 26
        OptionsView.Footer = True
        OptionsView.GroupByBox = False
        OptionsView.GroupRowHeight = 25
        OptionsView.HeaderFilterButtonShowMode = fbmButton
        OptionsView.HeaderHeight = 25
        OptionsView.Indicator = True
        OptionsView.IndicatorWidth = 20
        object tvViewColumn24: TcxGridDBColumn
          Caption = #24207#21495
          PropertiesClassName = 'TcxTextEditProperties'
          Properties.Alignment.Horz = taCenter
          HeaderAlignmentHorz = taCenter
          MinWidth = 40
          Options.Filtering = False
          Options.Sorting = False
          Width = 40
        end
        object tvViewColumn28: TcxGridDBColumn
          Caption = #29366#24577
          DataBinding.FieldName = 'status'
          PropertiesClassName = 'TcxCheckBoxProperties'
          Properties.Alignment = taCenter
          HeaderAlignmentHorz = taCenter
          Width = 40
        end
        object tvViewColumn1: TcxGridDBColumn
          Caption = #32534#21495
          DataBinding.FieldName = 'Numbers'
          HeaderAlignmentHorz = taCenter
          Width = 131
        end
        object tvViewColumn3: TcxGridDBColumn
          Caption = #24037#31243#21517#31216
          DataBinding.FieldName = 'ProjectName'
          Visible = False
          Width = 109
        end
      end
      object Lv: TcxGridLevel
        GridView = tvView
      end
    end
  end
  object Storage: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'DataSetProvider1'
    AfterInsert = StorageAfterInsert
    BeforePost = StorageBeforePost
    AfterPost = StorageAfterPost
    OnReconcileError = StorageReconcileError
    Left = 320
    Top = 320
  end
  object ds: TDataSource
    DataSet = Storage
    Left = 320
    Top = 384
  end
end
