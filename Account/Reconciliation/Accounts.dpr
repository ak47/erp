program Accounts;

uses
  Vcl.Forms,
  ufrmMain in 'forms\ufrmMain.pas' {frmMain},
  uDataModule in 'forms\uDataModule.pas' {DM: TDataModule},
  TreeFillThrd in 'Public\TreeFillThrd.pas',
  TreeUtils in 'Public\TreeUtils.pas',
  uFrame1 in 'Frames\uFrame1.pas' {Frame1: TFrame},
  ufrmSetTable in 'forms\ufrmSetTable.pas' {frmSetTable},
  ufrmLogin in 'forms\ufrmLogin.pas' {frmLogin};

{$R *.res}

begin
  ReportMemoryLeaksOnShutdown := DebugHook<>0;
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TfrmMain, frmMain);
  Application.CreateForm(TDM, DM);
  Application.CreateForm(TfrmSetTable, frmSetTable);
  Application.CreateForm(TfrmLogin, frmLogin);
  Application.Run;
end.
