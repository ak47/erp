unit ufrmBaseRunningAccount;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore,
  dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray, dxSkinOffice2013White,
  dxSkinOffice2016Colorful, dxSkinOffice2016Dark, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator, Data.DB,
  cxDBData, cxTextEdit, cxCheckBox, cxCalendar, cxDBLookupComboBox,
  cxCurrencyEdit, cxMemo, Datasnap.DBClient, cxGridLevel, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxClasses, cxGridCustomView, cxGrid,ufrmBaseController,
  cxDropDownEdit, Vcl.ExtCtrls, RzPanel, RzButton, Vcl.Menus,
  cxGridBandedTableView, dxmdaset, cxContainer, cxMaskEdit, cxLookupEdit,
  cxDBLookupEdit, cxLabel,UnitADO, cxSpinEdit, Vcl.ComCtrls, RzTabs,UtilsTree,FillThrdTree,
  Data.Win.ADODB, cxButtonEdit;

type
  TfrmBaseRunningAccount = class(TfrmBaseController)
    Master: TClientDataSet;
    MasterGrid: TDataSource;
    Grid: TcxGrid;
    tvMaster: TcxGridDBTableView;
    tvCol1: TcxGridDBColumn;
    tvCol2: TcxGridDBColumn;
    tvCol3: TcxGridDBColumn;
    tvCol4: TcxGridDBColumn;
    tvCol5: TcxGridDBColumn;
    tvCol7: TcxGridDBColumn;
    tvCol8: TcxGridDBColumn;
    tvCol9: TcxGridDBColumn;
    tvCol10: TcxGridDBColumn;
    tvCol11: TcxGridDBColumn;
    tvCol12: TcxGridDBColumn;
    tvCol13: TcxGridDBColumn;
    tvCol14: TcxGridDBColumn;
    tvCol15: TcxGridDBColumn;
    Lv1: TcxGridLevel;
    Repot: TPopupMenu;
    Execl1: TMenuItem;
    N6: TMenuItem;
    DetailedGrid: TDataSource;
    pmDetailed: TPopupMenu;
    N5: TMenuItem;
    Detailed: TClientDataSet;
    Client: TRzPanel;
    tvDetailed: TcxGridDBTableView;
    lv2: TcxGridLevel;
    cxGrid1: TcxGrid;
    RzPanel2: TRzPanel;
    RzPanel3: TRzPanel;
    Splitter1: TSplitter;
    Left: TRzPanel;
    Splitter2: TSplitter;
    tvDetailedColumn1: TcxGridDBColumn;
    tvDetailedColumn2: TcxGridDBColumn;
    tvDetailedColumn3: TcxGridDBColumn;
    tvDetailedColumn4: TcxGridDBColumn;
    tvDetailedColumn5: TcxGridDBColumn;
    tvDetailedColumn6: TcxGridDBColumn;
    RzToolbar1: TRzToolbar;
    rzspcrA: TRzSpacer;
    btnNew: TRzToolButton;
    rzspcrB: TRzSpacer;
    btnSave: TRzToolButton;
    rzspcrC: TRzSpacer;
    btnEdit: TRzToolButton;
    rzspcrD: TRzSpacer;
    btnDelete: TRzToolButton;
    rzspcrE: TRzSpacer;
    btnReport: TRzToolButton;
    RzToolbar3: TRzToolbar;
    RzSpacer15: TRzSpacer;
    RzToolButton11: TRzToolButton;
    RzSpacer16: TRzSpacer;
    RzToolButton12: TRzToolButton;
    RzSpacer17: TRzSpacer;
    RzToolButton13: TRzToolButton;
    RzSpacer18: TRzSpacer;
    RzToolButton15: TRzToolButton;
    RzSpacer19: TRzSpacer;
    RzToolButton16: TRzToolButton;
    RzSpacer1: TRzSpacer;
    RzToolButton1: TRzToolButton;
    RzSpacer2: TRzSpacer;
    RzToolButton2: TRzToolButton;
    tvDetailedColumn7: TcxGridDBColumn;
    tvDetailedColumn8: TcxGridDBColumn;
    tvDetailedColumn9: TcxGridDBColumn;
    pmMaster: TPopupMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    N7: TMenuItem;
    N4: TMenuItem;
    N8: TMenuItem;
    tvMasterColumn1: TcxGridDBColumn;
    rzspcrr: TRzSpacer;
    btnSetGrid: TRzToolButton;
    TopSplitter: TSplitter;
    Tv: TTreeView;
    KeyTool: TRzToolbar;
    RzSpacer7: TRzSpacer;
    RzSpacer3: TRzSpacer;
    RzToolButton4: TRzToolButton;
    cxLabel3: TcxLabel;
    cxLookupComboBox2: TcxLookupComboBox;
    RzPageControl1: TRzPageControl;
    TabSheet1: TRzTabSheet;
    TabSheet2: TRzTabSheet;
    RzToolbar5: TRzToolbar;
    RzSpacer26: TRzSpacer;
    RzSpacer27: TRzSpacer;
    RzToolButton9: TRzToolButton;
    cxLabel15: TcxLabel;
    Search: TcxLookupComboBox;
    cxGrid2: TcxGrid;
    tvSignName: TcxGridDBTableView;
    tvSignNameCol1: TcxGridDBColumn;
    tvSignNameCol2: TcxGridDBColumn;
    cxGridLevel1: TcxGridLevel;
    RzToolbar2: TRzToolbar;
    RzSpacer12: TRzSpacer;
    RzSpacer13: TRzSpacer;
    RzToolButton6: TRzToolButton;
    cxLabel2: TcxLabel;
    cxLookupComboBox1: TcxLookupComboBox;
    cxGrid3: TcxGrid;
    tvCompany: TcxGridDBTableView;
    tvCompanyColumn1: TcxGridDBColumn;
    tvCompanyColumn2: TcxGridDBColumn;
    lv: TcxGridLevel;
    ADOCompany: TADOQuery;
    DataCompany: TDataSource;
    RzSpacer51: TRzSpacer;
    Refresh: TRzToolButton;
    N9: TMenuItem;
    N10: TMenuItem;
    N11: TMenuItem;
    tvCol6: TcxGridDBColumn;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure tvCol1GetDisplayText(Sender: TcxCustomGridTableItem;
      ARecord: TcxCustomGridRecord; var AText: string);
    procedure MasterAfterInsert(DataSet: TDataSet);
    procedure tvCol13PropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
    procedure tvMasterTcxGridDBDataControllerTcxDataSummaryFooterSummaryItems0GetText(
      Sender: TcxDataSummaryItem; const AValue: Variant; AIsFooter: Boolean;
      var AText: string);
    procedure tvMasterEditing(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; var AAllow: Boolean);
    procedure tvMasterDblClick(Sender: TObject);
    procedure tvMasterStylesGetContentStyle(Sender: TcxCustomGridTableView;
      ARecord: TcxCustomGridRecord; AItem: TcxCustomGridTableItem;
      var AStyle: TcxStyle);
    procedure N6Click(Sender: TObject);
    procedure Execl1Click(Sender: TObject);
    procedure tvMasterEditKeyDown(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
      Shift: TShiftState);
    procedure tvCol2PropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
    procedure tvDetailedColumn1GetDisplayText(Sender: TcxCustomGridTableItem;
      ARecord: TcxCustomGridRecord; var AText: string);
    procedure tvDetailedEditing(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; var AAllow: Boolean);
    procedure tvDetailedEditKeyDown(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
      Shift: TShiftState);
    procedure DetailedAAfterInsert(DataSet: TDataSet);
    procedure DetailedAfterInsert(DataSet: TDataSet);
    procedure N4Click(Sender: TObject);
    procedure tvSignNameCol1GetDisplayText(Sender: TcxCustomGridTableItem;
      ARecord: TcxCustomGridRecord; var AText: string);
    procedure btnNewClick(Sender: TObject);
    procedure RzToolButton11Click(Sender: TObject);
    procedure RzToolButton12Click(Sender: TObject);
    procedure RzToolButton2Click(Sender: TObject);
    procedure tvMasterColumnSizeChanged(Sender: TcxGridTableView;
      AColumn: TcxGridColumn);
    procedure RzToolButton1Click(Sender: TObject);
    procedure RzToolButton13Click(Sender: TObject);
    procedure btnEditClick(Sender: TObject);
    procedure cxGrid1DBTableView1Editing(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; var AAllow: Boolean);
    procedure DetailedAfterOpen(DataSet: TDataSet);
    procedure DetailedAfterClose(DataSet: TDataSet);
    procedure SearchKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure N1Click(Sender: TObject);
    procedure N2Click(Sender: TObject);
    procedure N3Click(Sender: TObject);
    procedure N7Click(Sender: TObject);
    procedure tvDetailedColumn5PropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
    procedure tvDetailedColumn6PropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
    procedure tvDetailedColumn7PropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure btnSetGridClick(Sender: TObject);
    procedure tvDetailedColumnSizeChanged(Sender: TcxGridTableView;
      AColumn: TcxGridColumn);
    procedure FormActivate(Sender: TObject);
    procedure TvClick(Sender: TObject);
    procedure RefreshClick(Sender: TObject);
    procedure N9Click(Sender: TObject);
    procedure N10Click(Sender: TObject);
  private
    { Private declarations }
    g_TreeView : TNewUtilsTree;
    Prefix : string;
    Suffix : string;
    procedure SetcxGrid(lpGridView : TcxGridDBTableView ; index : Byte ;lpSuffix:string);
  public
    { Public declarations }
    dwMasterSQLText   : string;
    dwDetailedSQLText : string;
    dwADO : TADO;
    dwIsSelect : Boolean;
    dwTabTypes : string;
    function GetCodeList(lpView : TcxGridDBTableView ; lpIndex : Integer):string;
    function GetDesign(DD1,DD2 : Variant):string;
  end;

var
  frmBaseRunningAccount: TfrmBaseRunningAccount;
  dwMasterTmpFile : string = '\Temp\RunningMaster.cds';

implementation

uses
   global,uDataModule,ufrmPayDetailedPost,ufrmIsViewGrid;

{$R *.dfm}

function TfrmBaseRunningAccount.GetCodeList(lpView : TcxGridDBTableView ; lpIndex : Integer):string;
var
  s : string;
  i : Integer;
  szCodeList : string;

begin
  szCodeList := '';
  s := '';
  with lpView do
  begin
    for I := 0 to ViewData.RecordCount-1 do
    begin
      s := VarToStr( ViewData.Rows[i].Values[ lpIndex ] );

      if Length(s) = 0 then
        szCodeList := s
      else
        szCodeList := szCodeList + '","' + s ;
    end;
  end;
  Result := szCodeList;
end;

procedure TfrmBaseRunningAccount.SearchKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if Key = 13 then Self.RzToolButton9.Click;
end;

procedure TfrmBaseRunningAccount.SetcxGrid(lpGridView : TcxGridDBTableView ; index : Byte ;
lpSuffix:string);
var
  i : Integer;
begin
  for I := 0 to lpGridView.ColumnCount-1 do
  begin
    Application.ProcessMessages;
    SetGrid(lpGridView.Columns[i],index, Self.Name + lpSuffix);
  end;
end;

procedure TfrmBaseRunningAccount.TvClick(Sender: TObject);
var
  Node : TTreeNode;
  I: Integer;
  szData : PNodeData;
  szCodeId : Integer;

begin
  inherited;
  Node := Self.Tv.Selected;
  if Assigned(Node) then
  begin
    if Node.Level > 0 then
    begin
      szData := PNodeData(Node.Data);
      if Assigned(szData) then
      begin
        szCodeId := szData^.Index;

        with Self.ADOCompany do
        begin
          Close;
          SQL.Clear;
          SQL.Text := 'Select * from ' + g_Table_CompanyManage + ' WHERE ' + 'Sign_Id' + '=' + IntToStr(szCodeId) ;
          Open;
        end;

      end;

    end;

  end;

end;

procedure TfrmBaseRunningAccount.btnEditClick(Sender: TObject);
begin
  inherited;
  dwADO.ADOIsEdit(Self.Detailed);
end;

procedure TfrmBaseRunningAccount.btnNewClick(Sender: TObject);
var
  szIndex : Integer;
  szName : string;

begin
  inherited;
  with Self.Master do
  begin
    if State in [DsEdit, DSInsert] then Post;
  end;
  
  szIndex := Self.tvMaster.Controller.FocusedRowIndex;
  if szIndex >= 0 then
  begin
    szName := VarToStr( Self.tvCol6.EditValue );
    if Length(szName) = 0 then
    begin
      ShowMessage('流水帐收款单位不可以为空');
    end else
    begin
      dwADO.ADOAppend(Self.Detailed);
      Self.cxGrid1.SetFocus;
      Self.tvDetailedColumn3.FocusWithSelection;
    end;
  end;
end;

procedure TfrmBaseRunningAccount.btnSetGridClick(Sender: TObject);
var
  Child : TfrmIsViewGrid;
begin
  inherited;
//表格设置
  Child := TfrmIsViewGrid.Create(Application);
  try
    Child.g_fromName := Self.Name + g_dir_DetailTable;
    Child.ShowModal;
    SetcxGrid(Self.tvDetailed,0, g_dir_DetailTable );
  finally
    Child.Free;
  end;
end;

procedure TfrmBaseRunningAccount.cxGrid1DBTableView1Editing(
  Sender: TcxCustomGridTableView; AItem: TcxCustomGridTableItem;
  var AAllow: Boolean);
begin
  inherited;
  dwADO.ADOIsEditing(Self.Detailed,AAllow);

end;

procedure TfrmBaseRunningAccount.DetailedAAfterInsert(DataSet: TDataSet);
var
  szColName : string;
  szCode : string;

begin
  with Self.tvMaster.DataController do
     szCode := VarToStr(Values[FocusedRecordIndex,Self.tvCol3.Index]);

  Self.tvDetailedColumn2.EditValue := szCode;
end;

procedure TfrmBaseRunningAccount.DetailedAfterClose(DataSet: TDataSet);
begin
  inherited;
  Self.btnSave.Enabled := False;
  Self.btnEdit.Enabled := False;
  Self.btnDelete.Enabled := False;
  Self.btnReport.Enabled := False;
  Self.btnSetGrid.Enabled := False;
end;

procedure TfrmBaseRunningAccount.DetailedAfterInsert(DataSet: TDataSet);
begin
  Self.tvDetailedColumn2.EditValue := Self.tvCol3.EditValue;
end;

procedure TfrmBaseRunningAccount.DetailedAfterOpen(DataSet: TDataSet);
begin
  inherited;
  Self.btnSave.Enabled := True;
  Self.btnEdit.Enabled := True;
  Self.btnDelete.Enabled := True;
  Self.btnReport.Enabled := True;
  Self.btnSetGrid.Enabled := True;
end;

procedure TfrmBaseRunningAccount.Execl1Click(Sender: TObject);
begin
  inherited;
  CxGridToExcel(Self.Grid,'流水帐明细表');
end;

procedure TfrmBaseRunningAccount.FormActivate(Sender: TObject);
begin
  inherited;
  g_TreeView := TNewUtilsTree.Create(Self.tv,DM.ADOconn,g_Table_CompanyTree,'往来单位');
  g_TreeView.GetTreeTable(0);
end;

procedure TfrmBaseRunningAccount.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  Action := caFree;
end;

procedure TfrmBaseRunningAccount.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
  inherited;
  if (Self.Detailed.ChangeCount <> 0) or (Self.Master.ChangeCount <> 0) then
  begin
    if Application.MessageBox( '有新增或编辑的数据未保存，现在是否退出?', '提示:', MB_OKCANCEL + MB_ICONWARNING) = IDCANCEL then
    begin
      CanClose := False;
    end;  
  end;  
end;

procedure TfrmBaseRunningAccount.FormCreate(Sender: TObject);
begin
  inherited;
  Prefix := 'LS-';
  Suffix := '10010';
  dwMasterSQLText :=  'Select * from ' + g_Table_RuningAccount + ' Where 2=1';
  dwDetailedSQLText :=  'Select * from ' + g_Table_DetailTable + ' Where 2=1';  //明细表
  dwIsSelect := False;

  SetcxGrid(Self.tvMaster,0,g_Dir_RuningAccount);
  SetcxGrid(Self.tvDetailed,0, g_dir_DetailTable );

  case g_RunningIndex of
    0:
    begin
      dwTabTypes := '帐目收款';
      Self.tvCol6.RepositoryItem := DM.CompanyList;
    end;
    1:
    begin
      dwTabTypes := '帐目支款';
      Self.tvCol8.RepositoryItem := DM.CompanyList;
    end;
    2:
    begin
      dwTabTypes := '现金收款';
      Self.tvCol6.RepositoryItem := DM.CompanyList;
    end;
    3:
    begin
      dwTabTypes := '现金支款';
      Self.tvCol8.RepositoryItem := DM.CompanyList;
    end;
  end;
  
  Caption := Caption + ' (' + dwTabTypes + ')';
//账目收款和现金收款的收款单位固定为公司信息的名称
//账目支款和现金支款的付款单位固定为公司信息的名称
end;

procedure TfrmBaseRunningAccount.MasterAfterInsert(DataSet: TDataSet);
var
  szColName : string;
  szCode : string;

begin
  inherited;
  with DataSet do
  begin
    if Active then
    begin
      if (g_RunningIndex = 0) or (g_RunningIndex = 2) then
      begin
        Self.tvCol6.EditValue := Self.tvCompanyColumn2.EditValue;
        Self.tvCol8.EditValue := Self.tvSignNameCol2.EditValue;
      end else
      begin
        Self.tvCol6.EditValue := Self.tvSignNameCol2.EditValue;
        Self.tvCol8.EditValue := Self.tvCompanyColumn2.EditValue;
      end;

      FieldByName('fromType').Value := g_RunningIndex;
      FieldByName(Self.tvCol4.DataBinding.FieldName).Value := Date;

      FieldByName(Self.tvCol2.DataBinding.FieldName).Value := True;

      szColName := Self.tvCol3.DataBinding.FieldName;
      szCode := Prefix + GetRowCode( g_Table_RuningAccount ,
                                     szColName,
                                     Suffix,
                                     100000 + Self.tvMaster.DataController.RecordCount);
      FieldByName(szColName).Value := szCode ;
      FieldByName(Self.tvCol7.DataBinding.FieldName).Value := '现金';
      Self.tvCol5.EditValue := dwTabTypes;
    end;

  end;

end;

procedure TfrmBaseRunningAccount.N10Click(Sender: TObject);
var
  szDir : string;

begin
  inherited;
  szDir :=  Concat(g_Resources,'\' + g_Dir_RuningAccount);
  CreateEnclosure(Self.tvMaster.DataController.DataSource.DataSet,Handle, szDir );
end;

procedure TfrmBaseRunningAccount.N1Click(Sender: TObject);
begin
  inherited;
  Self.RzToolButton11.Click;
end;

procedure TfrmBaseRunningAccount.N2Click(Sender: TObject);
begin
  inherited;
  Self.RzToolButton13.Click;
end;

procedure TfrmBaseRunningAccount.N3Click(Sender: TObject);
begin
  inherited;
  Self.RzToolButton12.Click;
end;

procedure TfrmBaseRunningAccount.N4Click(Sender: TObject);
var
  Child : TfrmPayDetailedPost;
begin
  inherited;

  Child := TfrmPayDetailedPost.Create(Self);
  try
    {
    Child.ds.DataSet := Self.Detailed;
    Child.cxDBLookupComboBox1.DataBinding.DataField  := Self.tvDetailedColumn3.DataBinding.FieldName;
    Child.cxDBCurrencyEdit1.DataBinding.DataField := Self.tvDetailedColumn5.DataBinding.FieldName;
    Child.cxDBTextEdit1.DataBinding.DataField := Self.tvDetailedColumn4.DataBinding.FieldName;
    Child.cxDBMemo1.DataBinding.DataField := Self.tvDetailedColumn6.DataBinding.FieldName;
    if Sender = N4 then
    begin
      with Self.Detailed do
      begin
        First;
        Insert;
      end;

    end else
    if Sender = N5 then
    begin
      with Self.Detailed do
      begin
        Edit;
      end;
    end;

    Child.ShowModal;
    Self.tvDetailed.LayoutChanged();
    Self.tvDetailed.Painter.Invalidate;
    }

{
    TcxGridDBBandedTableView.LayoutChanged();
//tvCars.LayoutChanged(False);
TcxGridDBBandedTableView.Painter.Invalidate;
}
  finally

  end;
end;

procedure TfrmBaseRunningAccount.N6Click(Sender: TObject);
begin
  inherited;
  DM.BasePrinterLink1.Component := Self.Grid;
  DM.BasePrinter.Preview(True,nil);
end;

procedure TfrmBaseRunningAccount.N7Click(Sender: TObject);
begin
  inherited;
  Self.RzToolButton15.Click;
end;

procedure TfrmBaseRunningAccount.N9Click(Sender: TObject);
begin
  inherited;
  Self.Refresh.Click;
end;

procedure TfrmBaseRunningAccount.RefreshClick(Sender: TObject);
begin
  inherited;
  GetMaintainInfo;
  ShowMessage('信息刷新完成!');
end;

procedure TfrmBaseRunningAccount.RzToolButton11Click(Sender: TObject);
begin
  inherited;
  dwADO.ADOAppend(Self.Master);
  Self.Grid.SetFocus;
  Self.tvCol4.FocusWithSelection;
end;

procedure TfrmBaseRunningAccount.RzToolButton12Click(Sender: TObject);
var
  ErrCount : Integer;
  err : string;
  path: string;
  FileName : string;
  szCodeList , szSqlText : string;
  i : Integer;
  
begin
  inherited;
  //数据保存
  Path := ExtractFilePath(Application.Exename);
  FileName := path + dwMasterTmpFile;
  DM.ADOconn.BeginTrans; //开始事务
  try
    if Self.tvDetailed.ViewData.RecordCount <> 0  then
    begin
        if not dwIsSelect then  //是否为查询
        begin
          szCodeList := GetCodeList(Self.tvDetailed,Self.tvDetailedColumn2.Index);  //取出列表代码
          {
          for I := 0 to Self.tvMaster.ViewData.RecordCount-1 do
          begin

          end;
          }
          szSqlText := 'Select * from ' + g_Table_DetailTable +
             ' Where ' + Self.tvDetailedColumn2.DataBinding.FieldName +
             ' in ("'+ szCodeList +'")';

        end else
        begin
          szSqlText := dwMasterSQLText;
        end;

        DM.ADOQuery1.Close;
        DM.ADOQuery1.SQL.Text := szSqlText;

        with Self.Detailed do
        begin
          if State in [DsEdit, DSInsert] then Post;
          
          
          if ChangeCount > 0 then
          begin
            DM.DataSetProvider1.ApplyUpdates(Delta,0,ErrCount);
            case ErrCount of
              0:
              begin
                if fileexists(FileName) then DeleteFile(FileName);
                MergeChangeLog;
                Close;
                DM.ADOQuery1.Open;
                Data := DM.DataSetProvider1.Data;
                Open;

              end;
              1:
              begin
                ShowMessage('('+ IntToStr(ErrCount) +')保存失败!' + dwMasterSQLText);
              end;
            end;

          end;

        end;
    end;

    if Self.tvMaster.ViewData.RecordCount <> 0 then
    begin

      if not dwIsSelect then  //是否为查询
      begin
        szCodeList := GetCodeList(Self.tvMaster,Self.tvCol3.Index);  //取出列表代码

        szSqlText := 'Select * from ' + g_Table_RuningAccount +
           ' Where ' + Self.tvCol3.DataBinding.FieldName +
           ' in ("'+ szCodeList +'")';

      end else
      begin
        szSqlText := dwMasterSQLText;
      end;
      //**************************************************************************
      DM.ADOQuery1.Close;
      DM.ADOQuery1.SQL.Text := szSqlText;

      with Self.Master do
      begin
        if State in [DsEdit, DSInsert] then Post;

        if ChangeCount > 0 then
        begin
          DM.DataSetProvider1.ApplyUpdates(Delta,0,ErrCount);
          case ErrCount of
            0:
            begin
              if fileexists(FileName) then DeleteFile(FileName);
              MergeChangeLog;
              Close;
              DM.ADOQuery1.Open;
              Data := DM.DataSetProvider1.Data;
              Open;
            //  ShowMessage('保存成功!');
            end;
            1:
            begin
              ShowMessage('('+ IntToStr(ErrCount) +')保存失败!' + dwMasterSQLText);
            end;
          end;

        end;

      end;
      //**************************************************************************
      DM.ADOconn.Committrans; //提交事务
      ShowMessage('保存成功!');
    end;
  except
    on E:Exception do
    begin
      DM.ADOconn.RollbackTrans;           // 事务回滚
      err := E.Message;
      ShowMessage('保存失败(' + err + ')');
    end;
  end;

end;

procedure TfrmBaseRunningAccount.RzToolButton13Click(Sender: TObject);
begin
  inherited;
  dwADO.ADOIsEdit(Self.Master);
end;

procedure TfrmBaseRunningAccount.RzToolButton1Click(Sender: TObject);
var
  Child : TfrmIsViewGrid;
begin
  inherited;
//表格设置
  Child := TfrmIsViewGrid.Create(Application);
  try
    Child.g_fromName := Self.Name + g_Dir_RuningAccount;
    Child.ShowModal;
    SetcxGrid(Self.tvMaster,0,g_Dir_RuningAccount);
  finally
    Child.Free;
  end;
end;

procedure TfrmBaseRunningAccount.RzToolButton2Click(Sender: TObject);
begin
  inherited;
  Close;
end;

procedure TfrmBaseRunningAccount.tvCol13PropertiesValidate(Sender: TObject;
  var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
var
  szColName : string;

begin
  inherited;

  with Self.Master do
  begin
    if State <> dsInactive then
    begin
      szColName := Self.tvCol13.DataBinding.FieldName;
      FieldByName(szColName).Value := DisplayValue;
      szColName := Self.tvCol14.DataBinding.FieldName;
      FieldByName(szColName).Value := MoneyConvert(DisplayValue);
    end;
  end;

end;

procedure TfrmBaseRunningAccount.tvCol1GetDisplayText(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AText: string);
begin
  inherited;
  AText := IntToStr(ARecord.Index + 1);
end;

procedure TfrmBaseRunningAccount.tvCol2PropertiesValidate(Sender: TObject;
  var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
begin
  Self.tvMaster.DataController.Post();
end;

procedure TfrmBaseRunningAccount.tvDetailedColumn1GetDisplayText(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AText: string);
begin
  AText := IntToStr(ARecord.Index + 1 );
end;

function TfrmBaseRunningAccount.GetDesign(DD1,DD2 : Variant):string;
begin
  Result := VarToStr(DD1) + '*' + VarToStr(DD2);
  if (DD1 <> null) AND ( DD2 <> null ) then
  begin
    Self.tvDetailedColumn7.EditValue := Result;
    Self.tvDetailedColumn8.EditValue := FunExpCalc(Result,-2);
  end;
end;

procedure TfrmBaseRunningAccount.tvDetailedColumn5PropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
var
   s : string;
begin
  inherited;
  Self.tvDetailedColumn5.EditValue := DisplayValue;
  s := GetDesign(DisplayValue,Self.tvDetailedColumn6.EditValue);
end;

procedure TfrmBaseRunningAccount.tvDetailedColumn6PropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
var
  s : string;
begin
  inherited;
  Self.tvDetailedColumn6.EditValue := DisplayValue;
  s := GetDesign(Self.tvDetailedColumn5.EditValue,DisplayValue);
end;

procedure TfrmBaseRunningAccount.tvDetailedColumn7PropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
var
  szValue : string;

begin
  inherited;
  szValue := VarToStr( DisplayValue );
  if szValue <> '' then
  begin
    Self.tvDetailedColumn7.EditValue := DisplayValue;
    Self.tvDetailedColumn8.EditValue := FunExpCalc(szValue,-2);
  end;
end;

procedure TfrmBaseRunningAccount.tvDetailedColumnSizeChanged(
  Sender: TcxGridTableView; AColumn: TcxGridColumn);
begin
  inherited;
  SetcxGrid(Self.tvDetailed,0, g_dir_DetailTable );
end;

procedure TfrmBaseRunningAccount.tvDetailedEditing(
  Sender: TcxCustomGridTableView; AItem: TcxCustomGridTableItem;
  var AAllow: Boolean);
begin
  dwADO.ADOIsEditing(Self.Master,AAllow);
end;

procedure TfrmBaseRunningAccount.tvDetailedEditKeyDown(
  Sender: TcxCustomGridTableView; AItem: TcxCustomGridTableItem;
  AEdit: TcxCustomEdit; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if (Key = 13) AND 
     (AItem.Index = tvDetailedColumn8.Index) and 
     (IsNewRow) and 
     (Self.tvDetailed.Controller.FocusedRow.IsLast) then
  begin
    dwADO.ADOAppend(Self.Detailed);
    Self.cxGrid1.SetFocus;
    Self.tvDetailedColumn2.FocusWithSelection;
  end;
end;

procedure TfrmBaseRunningAccount.tvMasterColumnSizeChanged(
  Sender: TcxGridTableView; AColumn: TcxGridColumn);
begin
  inherited;
  SetcxGrid(Self.tvMaster,1,g_Dir_RuningAccount);
end;

procedure TfrmBaseRunningAccount.tvMasterDblClick(Sender: TObject);
begin
  Self.RzToolButton13.Click;
end;

procedure TfrmBaseRunningAccount.tvMasterEditing(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; var AAllow: Boolean);
begin
  inherited;
  dwADO.ADOIsEditing(Self.Master,AAllow);
  if AItem.Index = Self.tvCol2.Index then  AAllow := True;
end;

procedure TfrmBaseRunningAccount.tvMasterEditKeyDown(
  Sender: TcxCustomGridTableView; AItem: TcxCustomGridTableItem;
  AEdit: TcxCustomEdit; var Key: Word; Shift: TShiftState);
begin
  if (Key = 13) and (AItem.Index = Self.tvCol13.Index) then
  begin
    if Self.tvMaster.Controller.FocusedRow.IsLast and IsNewRow then
    begin
      dwADO.ADOAppend(Self.Master);
      Self.tvCol4.FocusWithSelection;
    end;
  end;
end;

procedure TfrmBaseRunningAccount.tvMasterStylesGetContentStyle(
  Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
  AItem: TcxCustomGridTableItem; var AStyle: TcxStyle);
var
  Value : Variant;
begin
  inherited;
  Value := ARecord.Values[Self.tvCol2.Index];
  if Value = False then
  begin
    AStyle := DM.cxStyle222;
  end;
  {
  Value := ARecord.Values[Self.tvCol5.Index];
  if VarToStr(Value ) = '成本支款' then
  begin
    AStyle := DM.cxStyle101;
  end;

  if VarToStr(Value) = '营收收款' then
  begin
    AStyle := DM.cxStyle68;
  end;

  }
  {
  成本支款
  营收收款
  其他支款
  其他收款
  借款
  贷款
  发票
  }
end;

procedure TfrmBaseRunningAccount.tvMasterTcxGridDBDataControllerTcxDataSummaryFooterSummaryItems0GetText(
  Sender: TcxDataSummaryItem; const AValue: Variant; AIsFooter: Boolean;
  var AText: string);
var
  t : Currency;
  szCapital : string;

begin
  inherited;
  try
    szCapital := '';
    if AValue <> null then
    begin
      t := StrToCurr( VarToStr(AValue) );
      szCapital := MoneyConvert(t);
    end else
    begin
      szCapital := '';
    end;
    Self.tvCol14.Summary.FooterFormat := szCapital;
  except
  end;

end;

procedure TfrmBaseRunningAccount.tvSignNameCol1GetDisplayText(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AText: string);
begin
  inherited;
  AText := IntToStr(ARecord.Index + 1);
end;

end.
