unit FillThrdTree;

interface
//新改进
uses
  Classes, ComCtrls, ADODB, SysUtils,SyncObjs,Windows,global;

type
  TNodeData = record
    Index     : integer;
    SignName  : string;
    Code      : string;
    InputDate : TDateTime;
    parent    : Integer;
    PId       : Integer;
    ProjectName:string;
    ProjectCode:string;
    remarks   : string;
  end;

  PNodeData = ^TNodeData;

  TNewFillTree = class(TThread)
    TV: TTreeView;
    TableName    : string;
    FieldIndex   : string;
    FieldPID     : string;
    FieldSignName: string;
    FieldCode    : string;
    FieldInputDate    : string;
    FieldProjectName  : string;
    FieldProjectCode  : string;
    FieldRemarks : string;
    FieldParent  : Integer;
    RootText: string;
    Conn: TADOConnection;
    Ptr: PNodeData;
    constructor Create(ATreeView: TTreeView; AConn: TADOConnection;ATable: string;
                       ARootText     : string = '节点管理';
                       AFieldIndex   : string = 'parent';
                       AFieldPID     : string = 'PID';
                       AFieldSignName: string = 'SignName';
                       AFieldCode    : string = 'Code';
                       AFieldInputDate: string ='InputDate';
                       AProjectName  : string = 'ProjectName';
                       AProjectCode  : string = 'ProjectCode';
                       AFieldRemarks : string = 'Remarks';
                       AParent       : Integer = 0
                       );
  private
    function AddParentTree(Node : TTreeNode; PN: PNodeData):TTreeNode;stdcall;
    procedure AddTree(Node: TTreeNode; PN: PNodeData;IsPatType : Boolean = False);
    procedure FillTree(treeview: TTreeView);
  protected
    procedure Execute; override;
  public
 //   Critical1 : TCriticalSection;
  end;

implementation

//------------------------------------------------------------------------------
constructor TNewFillTree.Create(ATreeView: TTreeView; AConn: TADOConnection;
                             ATable        : string;
                             ARootText     : string = '节点管理';
                             AFieldIndex   : string = 'parent';
                             AFieldPID     : string = 'PID';
                             AFieldSignName: string = 'SignName';
                             AFieldCode    : string = 'Code';
                             AFieldInputDate: string ='InputDate';
                             AProjectName  : string = 'ProjectName';
                             AProjectCode  : string = 'ProjectCode';
                             AFieldRemarks : string = 'Remarks';
                             AParent       : Integer = 0
                             );
begin

  FieldIndex    := AFieldIndex;
  FieldPID      := AFieldPID;
  FieldSignName := AFieldSignName;
  FieldCode     := AFieldCode;
  FieldInputDate:= AFieldInputDate;
  FieldProjectName  := AProjectName;
  FieldProjectCode  := AProjectCode;
  FieldRemarks  := AFieldRemarks;
  FieldParent   := AParent;
  TV := ATreeView;
  TableName := ATable;
  RootText  := ARootText;
  Conn      := AConn;

  New(Ptr);
  //DeleteCriticalSection(Critical1);
  inherited Create(False);

end;
//------------------------------------------------------------------------------
{
procedure TNewFillTree.Execute;
var
  Node: TTreeNode;

begin
  FreeOnTerminate := True;
  TV.Items.Clear;

  Ptr^.SignName:= RootText;
  ptr^.Index   := 0;

  Node := TV.Items.AddObject(nil, RootText, Ptr);
  Node.ImageIndex := 0;
  Node.SelectedIndex := 0;
  AddTree(Node,Ptr,False);

  TV.Items[0].Expanded := True;
//  TV.FullExpand;  全部展开
end;
}
procedure TNewFillTree.Execute;
var
  Node: TTreeNode;

begin
  FreeOnTerminate := True;

  TV.Items.Clear;

  Ptr^.Index    := FieldParent;
  Ptr^.SignName := RootText;

  Node := TV.Items.AddObject(nil, RootText,Ptr);
  if Assigned(Node) then
  begin
    Node.ImageIndex := 0;
    Node.SelectedIndex := 0;
  end;

  EnterCriticalSection(CS); //进入临界区
  try

    if FieldParent = 0 then
    begin
      AddTree(Node,Ptr,False);
    end
    else
    begin
      AddParentTree(Node,Ptr);
    end;
    with TV do
    begin
      if Items.Count >=1 then
      begin
        Items[0].Expanded := True;
        if Items.Count >= 2 then
        begin
          Items[1].Selected := True;
        end;
      end;
    end;

  finally
    LeaveCriticalSection(CS); //离开临界区
  end;
end;
//------------------------------------------------------------------------------
function TNewFillTree.AddParentTree(Node : TTreeNode; PN: PNodeData):TTreeNode;stdcall;
var
  Query: TADOQuery;
  nNode: TTreeNode;
  PNode: PNodeData;
begin

  Query := TADOQuery.Create(nil);
  try
      Query.Connection := Conn;
      Query.SQL.Text := 'Select * from ' + TableName + ' where ' + 'parent' + ' =' + IntToStr(PN^.Index);
      OutputLog(Query.SQL.Text);

      if Query.Active then
         Query.Close;

      Query.Open;
      if not Query.Eof then
      begin
        New(PNode);
        PNode^.SignName   := Query.FieldByName(FieldSignName).AsString;
        PNode^.Index      := Query.FieldByName(FieldIndex).AsInteger;
        PNode^.Code       := Query.FieldByName(FieldCode).AsString;
        PNode^.remarks    := Query.FieldByName(FieldRemarks).AsString;
        PNode^.pid        := Query.FieldByName(FieldPID).AsInteger;
        PNode^.InputDate  := Query.FieldByName(FieldInputDate).AsDateTime;
        nNode := TV.Items.AddChildObject(Node, PNode^.SignName, PNode);
        if Assigned(nNode) then
        begin
          nNode.ImageIndex    := 1;
          nNode.SelectedIndex := 2;

        end;
        AddTree(nNode, PNode,True);
      end;

  finally
    Query.Free;
  end;

end;
//------------------------------------------------------------------------------
procedure TNewFillTree.AddTree(Node: TTreeNode; PN: PNodeData;IsPatType : Boolean = False);
var
  Query: TADOQuery;
  nNode: TTreeNode;
  PNode: PNodeData;

begin
  //带排序树目录
  Query := TADOQuery.Create(nil);
  try
      Query.Connection := Conn;
      Query.SQL.Text := 'Select * from [' + TableName + '] ' +
                        ' where ' + FieldPID +
                        ' =' + IntToStr(PN^.Index) +
                        ' ORDER BY TreeIndex';

  //    OutputDebugString( PWideChar( query.SQL.Text ));
      OutputLog(Query.SQL.Text);
      if Query.Active then
         Query.Close;

      Query.Open;

      while not Query.Eof do
      begin

        New(PNode);

        PNode^.SignName   := Query.FieldByName(FieldSignName).AsString;
        PNode^.Index      := Query.FieldByName(FieldIndex).AsInteger;
        PNode^.Code       := Query.FieldByName(FieldCode).AsString;
        PNode^.remarks    := Query.FieldByName(FieldRemarks).AsString;
        PNode^.pid        := Query.FieldByName(FieldPID).AsInteger;
        PNode^.InputDate  := Query.FieldByName(FieldInputDate).AsDateTime;

        nNode := TV.Items.AddChildObject(Node, PNode^.SignName, PNode);
        if Assigned(nNode) then
        begin
          nNode.ImageIndex    := 1;
          nNode.SelectedIndex := 2;
        end;
        AddTree(nNode, PNode,IsPatType);
        Query.Next;
      end;

  finally
    Query.Free;
  end;

end;
//------------------------------------------------------------------------------

procedure AddTree(Node: TTreeNode; PN: PNodeData;IsPatType : Boolean = False);
var
  Query: TADOQuery;
  nNode: TTreeNode;
  PNode: PNodeData;

begin
  Query := TADOQuery.Create(nil);
  try
  //    Query.Connection := Conn;
  //    Query.SQL.Text := 'Select * from ' + TableName + ' where ' + FieldPID + ' =' + IntToStr(PN^.Index);

      if Query.Active then
         Query.Close;

      Query.Open;
      while not Query.Eof do
      begin
        New(PNode);
      //  PNode^.SignName   := Query.FieldByName(FieldSignName).AsString;
      //  PNode^.Index      := Query.FieldByName(FieldIndex).AsInteger;
      //  PNode^.Code       := Query.FieldByName(FieldCode).AsString;
      //  PNode^.remarks    := Query.FieldByName(FieldRemarks).AsString;
      //  PNode^.pid        := Query.FieldByName(FieldPID).AsInteger;

        if IsPatType then
        begin

        //  nNode := TV.Items.AddChildObject(Node, PNode^.SignName, PNode);
          if Assigned(nNode) then
          begin
            nNode.ImageIndex    := 1;
            nNode.SelectedIndex := 2;
          end;

        end
        else
        begin

        //  nNode := TV.Items.AddChildObject(Node, PNode^.SignName, PNode);
          if Assigned(nNode) then
          begin
            nNode.ImageIndex    := 1;
            nNode.SelectedIndex := 2;
          end;

        end;

        AddTree(nNode, PNode,IsPatType);
        Query.Next;
      end;

  finally
    Query.Free;
  end;

end;

//------------------------------------------------------------------------------
procedure TNewFillTree.FillTree(treeview: TTreeView);
var
  findq: TADOQuery;
  node: TTreeNode;
  s : string;

  //这个方法是根据记录的id字段值，查找TreeView上的父节点
  function FindParentNode(id:Integer):TTreeNode;
  var
    i:Integer;
  begin
    Result := nil;
    for i := 0 to treeview.Items.Count - 1 do
    begin
      //比较Node的Data值和记录的id值
      if Integer(treeview.Items[i].Data) = id then
      begin
        Result := treeview.Items[i];
        Break;
      end;
    end;
  end;

begin
  findq := TADOQuery.Create(nil);
  findq.Connection := Conn; //这里的Connection指向你的数据连接
  try
    treeview.Items.BeginUpdate;
    treeview.Items.Clear;
    //选出所有记录，并按parentid排序，这样可以保证先添加顶级节点，在下级记录添加时，总能找到父节点
    findq.SQL.Text := 'SELECT * FROM ['+ TableName +'] order by parent,id';
    findq.Open;

    while not findq.Eof do
    begin
      s := findq.FieldByName('SignName').AsString;
      //如果是最顶级，直接添加到Treeview
      if findq.FieldByName('parent').AsInteger = 0 then
      begin
        //将id值，保存在Node的Data中，以便查找用
        treeview.Items.AddObject(nil,s,nil);
      end
      else
      begin
        {
        //查找上级节点
        node := FindParentNode(findq.FieldByName('parent').AsInteger);
        if node <> nil then
        begin
          treeview.Items.AddChildObject(node,findq.FieldByName('SignName').AsString,Pointer(findq.FieldByName('id').AsInteger));
        end;
        }
      end;

      findq.Next;

    end;

  finally
    findq.Free;
    treeview.Items.EndUpdate;
  end;
end;
























end.

