unit ufrmDetailed;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, RzButton, RzPanel, Vcl.ExtCtrls,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxStyles,
  dxSkinsCore, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray,
  dxSkinOffice2013White, dxSkinOffice2016Colorful, dxSkinOffice2016Dark,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit,
  cxNavigator, Data.DB, cxDBData, cxTextEdit, cxDropDownEdit, cxCurrencyEdit,
  cxSpinEdit, cxMemo, cxGridLevel, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxClasses, cxGridCustomView, cxGrid, Vcl.Menus,
  Data.Win.ADODB,ufrmBaseController, Vcl.ComCtrls, frxClass, frxDBSet,
  Vcl.StdCtrls;

type
  TfrmPayDetailed = class(TfrmBaseController)
    RzToolbar2: TRzToolbar;
    RzSpacer5: TRzSpacer;
    RzToolButton7: TRzToolButton;
    RzSpacer8: TRzSpacer;
    RzToolButton8: TRzToolButton;
    RzSpacer9: TRzSpacer;
    RzToolButton9: TRzToolButton;
    RzSpacer10: TRzSpacer;
    RzToolButton12: TRzToolButton;
    Grid: TcxGrid;
    tvGrid4: TcxGridDBTableView;
    tvGrid4Column1: TcxGridDBColumn;
    tvGrid4Column2: TcxGridDBColumn;
    tvGrid4Column3: TcxGridDBColumn;
    tvGrid4Column4: TcxGridDBColumn;
    tvGrid4Column5: TcxGridDBColumn;
    tvGrid4Column6: TcxGridDBColumn;
    tvGrid4Column7: TcxGridDBColumn;
    tvGrid4Column8: TcxGridDBColumn;
    tvGrid4Column9: TcxGridDBColumn;
    tvGrid4Column10: TcxGridDBColumn;
    tvGrid4Column11: TcxGridDBColumn;
    tvGrid4Column12: TcxGridDBColumn;
    tvGrid4Column13: TcxGridDBColumn;
    tvGrid4Column14: TcxGridDBColumn;
    tvGrid4Column15: TcxGridDBColumn;
    Lv: TcxGridLevel;
    tvView: TcxGridDBTableView;
    tvViewColumn1: TcxGridDBColumn;
    tvViewColumn2: TcxGridDBColumn;
    tvViewColumn3: TcxGridDBColumn;
    tvViewColumn4: TcxGridDBColumn;
    tvViewColumn5: TcxGridDBColumn;
    tvViewColumn6: TcxGridDBColumn;
    tvViewColumn7: TcxGridDBColumn;
    tvViewColumn8: TcxGridDBColumn;
    tvViewColumn9: TcxGridDBColumn;
    tvViewColumn10: TcxGridDBColumn;
    tvViewColumn11: TcxGridDBColumn;
    tvViewColumn12: TcxGridDBColumn;
    tvViewColumn13: TcxGridDBColumn;
    tvViewColumn14: TcxGridDBColumn;
    tvViewColumn15: TcxGridDBColumn;
    DataSource1: TDataSource;
    ADOQuery1: TADOQuery;
    StatusBar1: TStatusBar;
    RzToolButton1: TRzToolButton;
    Print: TPopupMenu;
    N3: TMenuItem;
    N5: TMenuItem;
    Execl1: TMenuItem;
    tvViewColumn16: TcxGridDBColumn;
    RzSpacer1: TRzSpacer;
    RzToolButton2: TRzToolButton;
    RzSpacer2: TRzSpacer;
    RzSpacer3: TRzSpacer;
    RzToolButton3: TRzToolButton;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    N4: TMenuItem;
    procedure RzToolButton7Click(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure tvViewColumn1GetDisplayText(Sender: TcxCustomGridTableItem;
      ARecord: TcxCustomGridRecord; var AText: string);
    procedure RzToolButton12Click(Sender: TObject);
    procedure RzToolButton8Click(Sender: TObject);
    procedure RzToolButton9Click(Sender: TObject);
    procedure tvViewColumnSizeChanged(Sender: TcxGridTableView;
      AColumn: TcxGridColumn);
    procedure tvViewTcxGridDBDataControllerTcxDataSummaryFooterSummaryItems0GetText(
      Sender: TcxDataSummaryItem; const AValue: Variant; AIsFooter: Boolean;
      var AText: string);
    procedure tvViewEditKeyDown(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
      Shift: TShiftState);
    procedure N3Click(Sender: TObject);
    procedure Execl1Click(Sender: TObject);
    procedure tvViewColumn16GetPropertiesForEdit(Sender: TcxCustomGridTableItem;
      ARecord: TcxCustomGridRecord; var AProperties: TcxCustomEditProperties);
    procedure RzToolButton2Click(Sender: TObject);
    procedure tvViewEditing(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; var AAllow: Boolean);
    procedure RzToolButton3Click(Sender: TObject);
    procedure tvViewColumn4PropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
    procedure tvViewColumn5PropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
    procedure N1Click(Sender: TObject);
    procedure N2Click(Sender: TObject);
    procedure N4Click(Sender: TObject);
  private
    { Private declarations }
    procedure SetcxGrid(lpGridView : TcxGridDBTableView ; index : Byte ;lpSuffix:string);
  public
    { Public declarations }
    g_PactCode   : string;
    g_formName   : string;
    g_parentCode : string;
    g_TableName  : string;
    g_Detailed   : string;
    g_Money      : Currency;
    function GetDesign(DD1,DD2 : Variant):string;
  end;

var
  frmPayDetailed: TfrmPayDetailed;


implementation

uses
   uDataModule,ufrmIsViewGrid,global;

{$R *.dfm}

procedure TfrmPayDetailed.Execl1Click(Sender: TObject);
begin
  inherited;
  CxGridToExcel(Self.Grid,'明细表');
end;

procedure TfrmPayDetailed.FormActivate(Sender: TObject);
var
  I: Integer;

begin

  with Self.ADOQuery1 do
  begin
    Close;
    SQL.Clear;
    SQL.Text := 'Select * from '+  g_TableName  +' where parent="' + g_parentCode + '"';
    Open;

  end;

  SetcxGrid(Self.tvView,0,g_Detailed);
  Self.StatusBar1.Panels[0].Text := '汇总金额:' + CurrToStr(g_Money);
  with (Self.tvViewColumn16.Properties as TcxComboBoxProperties) do
  begin

    Items.Clear;
    for I := 0 to 10 do
    begin
      Items.Add(g_DetailArray[i]);
    end;

  end;

end;

procedure TfrmPayDetailed.SetcxGrid(lpGridView : TcxGridDBTableView ; index : Byte ;lpSuffix:string);
var
  i : Integer;
  szFieldName : string;
  szCaption   : string;
  szName : string;
begin
  inherited;
  for I := 0 to lpGridView.ColumnCount-1 do
  begin
    Application.ProcessMessages;
    SetGrid(lpGridView.Columns[i],index, g_formName + lpSuffix);
  end;
  
end;

procedure TfrmPayDetailed.N1Click(Sender: TObject);
begin
  inherited;
  Self.RzToolButton7.Click;
end;

procedure TfrmPayDetailed.N2Click(Sender: TObject);
begin
  inherited;
  Self.RzToolButton3.Click;
end;

procedure TfrmPayDetailed.N3Click(Sender: TObject);
begin
  inherited;
  DM.BasePrinterLink1.Component := Self.Grid;
  DM.BasePrinterLink1.ReportTitleText := '';
  DM.BasePrinter.Preview(True, nil);
end;

procedure TfrmPayDetailed.N4Click(Sender: TObject);
begin
  inherited;
  Self.RzToolButton9.Click;
end;

procedure TfrmPayDetailed.RzToolButton12Click(Sender: TObject);
begin
  Close;
end;

procedure TfrmPayDetailed.RzToolButton2Click(Sender: TObject);
var
  Child : TfrmIsViewGrid;
begin
  inherited;
   Child := TfrmIsViewGrid.Create(Application);
   try
      Child.g_fromName := g_formName + g_Detailed;
      Child.ShowModal;
      SetcxGrid(Self.tvView,0,g_Detailed);
   finally
      Child.Free;
   end;
end;

procedure TfrmPayDetailed.RzToolButton3Click(Sender: TObject);
begin
  inherited;
  ADOIsEdit(True,Self.ADOQuery1);
end;

procedure TfrmPayDetailed.RzToolButton7Click(Sender: TObject);
var
  szColName : string;
begin
  with Self.ADOQuery1 do
  begin
    if State = dsInactive then
    begin
      Application.MessageBox( '当前状态无效，无法正常获取拨款信息,重新选择一条拨款信息在尝试操作', '提示:',MB_OKCANCEL + MB_ICONWARNING);
      Close;
    end else
    begin
      if (State = dsEdit) or (State = dsInsert) then
      begin
        Post;
        Refresh;
      end ;
      Append;

      szColName := Self.tvViewColumn2.DataBinding.FieldName;
      FieldByName('Code').Value   := g_PactCode;
      FieldByName('parent').Value := g_parentCode;
      FieldByName(szColName).Value:= DM.getDataMaxDate(g_TableName);
      szColName := Self.tvViewColumn16.DataBinding.FieldName;
      //FieldByName(szColName).Value:= '税金';
      Self.Grid.SetFocus;
      SetGridFocus(Self.tvView);

    end;

  end;

end;

procedure TfrmPayDetailed.RzToolButton8Click(Sender: TObject);
begin
  SaveGrid(Self.ADOQuery1);
end;

procedure TfrmPayDetailed.RzToolButton9Click(Sender: TObject);
begin
  cxGridDeleteData(Self.tvView,'',g_TableName);
  Self.FormActivate(Sender);
end;

procedure TfrmPayDetailed.tvViewColumn16GetPropertiesForEdit(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AProperties: TcxCustomEditProperties);
begin
  inherited;
  GetTableMaintain(AProperties,g_Table_Maintain_Cost);
end;

procedure TfrmPayDetailed.tvViewColumn1GetDisplayText(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AText: string);
begin
  AText := IntToStr(ARecord.Index + 1);
end;

function TfrmPayDetailed.GetDesign(DD1,DD2 : Variant):string;
begin
  Result := VarToStr(DD1) + '*' + VarToStr(DD2);
  if (DD1 <> null) AND ( DD2 <> null ) then
  begin
    Self.tvViewColumn13.EditValue := Result;
    Self.tvViewColumn14.EditValue := FunExpCalc(Result,-2);
  end;  
end;  

procedure TfrmPayDetailed.tvViewColumn4PropertiesValidate(Sender: TObject;
  var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
var  
   s : string;
   
begin
  inherited;
  Self.tvViewColumn4.EditValue := DisplayValue;
  s := GetDesign(DisplayValue,Self.tvViewColumn5.EditValue);
end;

procedure TfrmPayDetailed.tvViewColumn5PropertiesValidate(Sender: TObject;
  var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
var
  s : string;
begin
  inherited;
  Self.tvViewColumn5.EditValue := DisplayValue;
  s := GetDesign(Self.tvViewColumn4.EditValue,DisplayValue);
end;

procedure TfrmPayDetailed.tvViewColumnSizeChanged(Sender: TcxGridTableView;
  AColumn: TcxGridColumn);
begin
  SetcxGrid(Self.tvView,1,g_Detailed);
end;

procedure TfrmPayDetailed.tvViewEditing(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; var AAllow: Boolean);
begin
  inherited;
  AAllow := ADOBanEditing(Self.ADOQuery1);
  if AItem.Index = Self.tvViewColumn1.Index then  AAllow := False;
end;

procedure TfrmPayDetailed.tvViewEditKeyDown(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
  Shift: TShiftState);
var
  s : string;
  szName : string;
  szValue: string;

begin
  inherited;
  if (Key = 13) then
  begin

    if AItem.Editing then
    begin

      if Self.tvViewColumn13.Index = AItem.Index then
      begin
        Self.tvView.DataController.UpdateData;
        with Self.ADOQuery1 do
        begin

          if State <> dsInactive then
          begin
            szName := Self.tvViewColumn13.DataBinding.FieldName;
            s := FieldByName(szName).Value;
            szValue:= FunExpCalc(s,-2);
          //  Edit;
            szName := Self.tvViewColumn14.DataBinding.FieldName;
            FieldByName(szName).Value := szValue;
          //  Post;
          //  Refresh;
          end;

        end;

      end else
      if Self.tvViewColumn14.Index = AItem.Index then
      begin
        Self.tvView.DataController.UpdateData;
        if IsNewRow then
        begin
          Self.RzToolButton7.Click;
        end;
      end;  

    end;

  end;

end;

procedure TfrmPayDetailed.tvViewTcxGridDBDataControllerTcxDataSummaryFooterSummaryItems0GetText(
  Sender: TcxDataSummaryItem; const AValue: Variant; AIsFooter: Boolean;
  var AText: string);
var
  szMoney : Currency;
  s : string;

begin
  inherited;
  try
    s := VarToStrDef(AValue,'');
    if Length(s)<>0 then
    begin
      szMoney := StrToCurrDef(s,0);
      Self.StatusBar1.Panels[1].Text := '交易金额:' + s;
      Self.StatusBar1.Panels[2].Text := '结余款额:' + CurrToStr(g_Money - szMoney);
    end;
  except
  end;

end;

end.
