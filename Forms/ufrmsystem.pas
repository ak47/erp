unit ufrmsystem;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, RzTabs, ExtCtrls, RzPanel, StdCtrls, ComCtrls, RzButton, Grids,
  DBGrids, ImgList, Buttons, Menus, ActnList, Spin, Mask, RzEdit,
  RzSpnEdt, RzDBEdit,IniFiles, DB, ADODB, ExtDlgs, System.Actions,
  System.ImageList, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, dxSkinsCore,
  dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray, dxSkinOffice2013White,
  dxSkinOffice2016Colorful, dxSkinOffice2016Dark, cxGroupBox, cxStyles,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxNavigator, cxDBData, cxTextEdit, cxGridLevel, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxClasses, cxGridCustomView, cxGrid;

type
  Tfrmsystem = class(TForm)
    pcSys: TRzPageControl;
    TabSheet1: TRzTabSheet;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    GroupBox2: TGroupBox;
    Label2: TLabel;
    Edit1: TEdit;
    Edit2: TEdit;
    ComboBox1: TComboBox;
    Label3: TLabel;
    RzBitBtn1: TRzBitBtn;
    RzBitBtn2: TRzBitBtn;
    Label4: TLabel;
    ListView1: TListView;
    ImageList1: TImageList;
    RzBitBtn3: TRzBitBtn;
    StatusBar1: TStatusBar;
    ImageList2: TImageList;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    N4: TMenuItem;
    N5: TMenuItem;
    N6: TMenuItem;
    ActionList1: TActionList;
    acAdd: TAction;
    acAddChild: TAction;
    acDelete: TAction;
    acEdit: TAction;
    TabSheet2: TRzTabSheet;
    GroupBox3: TGroupBox;
    Label27: TLabel;
    Label28: TLabel;
    Edit15: TEdit;
    Edit16: TEdit;
    RzBitBtn10: TRzBitBtn;
    RzBitBtn11: TRzBitBtn;
    GroupBox4: TGroupBox;
    ListView2: TListView;
    TabSheet5: TRzTabSheet;
    RzBitBtn4: TRzBitBtn;
    Edit3: TEdit;
    TabSheet3: TRzTabSheet;
    GroupBox5: TGroupBox;
    RzBitBtn5: TRzBitBtn;
    RzBitBtn6: TRzBitBtn;
    Edit4: TEdit;
    Label6: TLabel;
    RzBitBtn7: TRzBitBtn;
    GroupBox6: TGroupBox;
    ListView3: TListView;
    RzBitBtn8: TRzBitBtn;
    ADOconn: TADOConnection;
    Qry: TADOQuery;
    GroupBox7: TGroupBox;
    RzBitBtn9: TRzBitBtn;
    Label7: TLabel;
    GroupBox8: TGroupBox;
    RzEdit1: TRzEdit;
    Label5: TLabel;
    RzBitBtn12: TRzBitBtn;
    RzBitBtn13: TRzBitBtn;
    Edit5: TEdit;
    TabSheet4: TRzTabSheet;
    RzPageControl1: TRzPageControl;
    TabSheet6: TRzTabSheet;
    TabSheet7: TRzTabSheet;
    GroupBox10: TGroupBox;
    Label8: TLabel;
    RzBitBtn14: TRzBitBtn;
    RzBitBtn15: TRzBitBtn;
    Edit6: TEdit;
    RzBitBtn16: TRzBitBtn;
    RzBitBtn17: TRzBitBtn;
    GroupBox9: TGroupBox;
    ListView4: TListView;
    GroupBox12: TGroupBox;
    ListView5: TListView;
    GroupBox11: TGroupBox;
    Label9: TLabel;
    RzBitBtn18: TRzBitBtn;
    RzBitBtn19: TRzBitBtn;
    Edit7: TEdit;
    RzBitBtn20: TRzBitBtn;
    RzBitBtn21: TRzBitBtn;
    TabSheet8: TRzTabSheet;
    GroupBox13: TGroupBox;
    ListView6: TListView;
    GroupBox14: TGroupBox;
    Label10: TLabel;
    RzBitBtn22: TRzBitBtn;
    RzBitBtn23: TRzBitBtn;
    Edit8: TEdit;
    RzBitBtn24: TRzBitBtn;
    RzBitBtn25: TRzBitBtn;
    GroupBox15: TGroupBox;
    Label11: TLabel;
    RzBitBtn26: TRzBitBtn;
    RzBitBtn27: TRzBitBtn;
    Edit9: TEdit;
    RzBitBtn28: TRzBitBtn;
    RzBitBtn29: TRzBitBtn;
    GroupBox16: TGroupBox;
    ListView7: TListView;
    GroupBox17: TGroupBox;
    Label12: TLabel;
    RzBitBtn30: TRzBitBtn;
    RzBitBtn31: TRzBitBtn;
    Edit10: TEdit;
    RzBitBtn32: TRzBitBtn;
    RzBitBtn33: TRzBitBtn;
    GroupBox18: TGroupBox;
    ListView8: TListView;
    RzBitBtn34: TRzBitBtn;
    OpenPictureDialog1: TOpenPictureDialog;
    Label13: TLabel;
    RzEdit2: TRzEdit;
    TabSheet9: TRzTabSheet;
    GroupBox19: TGroupBox;
    Label14: TLabel;
    RzBitBtn35: TRzBitBtn;
    RzBitBtn36: TRzBitBtn;
    Edit11: TEdit;
    RzBitBtn37: TRzBitBtn;
    RzBitBtn38: TRzBitBtn;
    GroupBox20: TGroupBox;
    ListView9: TListView;
    TabSheet10: TRzTabSheet;
    GroupBox21: TGroupBox;
    ListView10: TListView;
    GroupBox22: TGroupBox;
    Label15: TLabel;
    RzBitBtn39: TRzBitBtn;
    RzBitBtn40: TRzBitBtn;
    Edit12: TEdit;
    RzBitBtn41: TRzBitBtn;
    RzBitBtn42: TRzBitBtn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure RzBitBtn1Click(Sender: TObject);
    procedure ListView1Click(Sender: TObject);
    procedure RzBitBtn2Click(Sender: TObject);
    procedure RzBitBtn3Click(Sender: TObject);
    procedure Edit8KeyPress(Sender: TObject; var Key: Char);
    procedure RzBitBtn10Click(Sender: TObject);
    procedure RzEdit1Change(Sender: TObject);
    procedure RzBitBtn4Click(Sender: TObject);
    procedure ListView2Click(Sender: TObject);
    procedure RzBitBtn11Click(Sender: TObject);
    procedure RzBitBtn5Click(Sender: TObject);
    procedure RzBitBtn6Click(Sender: TObject);
    procedure RzBitBtn7Click(Sender: TObject);
    procedure RzBitBtn8Click(Sender: TObject);
    procedure ListView3Click(Sender: TObject);
    procedure RzBitBtn9Click(Sender: TObject);
    procedure RzBitBtn12Click(Sender: TObject);
    procedure RzBitBtn13Click(Sender: TObject);
    procedure RzBitBtn17Click(Sender: TObject);
    procedure RzBitBtn21Click(Sender: TObject);
    procedure RzBitBtn14Click(Sender: TObject);
    procedure RzBitBtn18Click(Sender: TObject);
    procedure ListView4Click(Sender: TObject);
    procedure ListView5Click(Sender: TObject);
    procedure RzBitBtn16Click(Sender: TObject);
    procedure RzBitBtn20Click(Sender: TObject);
    procedure RzBitBtn22Click(Sender: TObject);
    procedure RzBitBtn25Click(Sender: TObject);
    procedure RzBitBtn29Click(Sender: TObject);
    procedure RzBitBtn24Click(Sender: TObject);
    procedure RzBitBtn28Click(Sender: TObject);
    procedure RzBitBtn26Click(Sender: TObject);
    procedure ListView6Click(Sender: TObject);
    procedure ListView7Click(Sender: TObject);
    procedure RzBitBtn27Click(Sender: TObject);
    procedure RzBitBtn23Click(Sender: TObject);
    procedure RzBitBtn15Click(Sender: TObject);
    procedure RzBitBtn19Click(Sender: TObject);
    procedure ListView8Click(Sender: TObject);
    procedure RzBitBtn30Click(Sender: TObject);
    procedure RzBitBtn33Click(Sender: TObject);
    procedure RzBitBtn32Click(Sender: TObject);
    procedure RzBitBtn31Click(Sender: TObject);
    procedure RzBitBtn34Click(Sender: TObject);
    procedure Edit15KeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure Edit4KeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure Edit6KeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure Edit7KeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure Edit8KeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure Edit9KeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure Edit10KeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure RzBitBtn35Click(Sender: TObject);
    procedure RzBitBtn37Click(Sender: TObject);
    procedure RzBitBtn36Click(Sender: TObject);
    procedure Edit11KeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure ListView9KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ListView6KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ListView7KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ListView8KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ListView4KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ListView5KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ListView3KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ListView9Click(Sender: TObject);
    procedure RzBitBtn39Click(Sender: TObject);
    procedure RzBitBtn42Click(Sender: TObject);
    procedure RzBitBtn41Click(Sender: TObject);
    procedure RzBitBtn40Click(Sender: TObject);
    procedure ListView10Click(Sender: TObject);
    procedure RzBitBtn38Click(Sender: TObject);
    procedure Edit12KeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
    function RestoreFile(StrRestorePathFile:TFileName;infoLabel:TLabel):Boolean;
    function GetUnitList(List : TListView ; chType : Integer):Boolean;stdcall;
    function AddUnit(lpName : string; chType:Integer):Boolean;stdcall;
    function ModifyUnit(lpName : string; chType : Integer) : Boolean;stdcall;
    function DeleteUnit(lpNumber : string; lpName : string):Boolean;stdcall;
  end;

var
  frmsystem: Tfrmsystem;

implementation

{$R *.dfm}

uses
  global,uDataModule,MD5,ComObj;


function Tfrmsystem.ModifyUnit(lpName : string; chType : Integer) : Boolean;stdcall;
var
  sqlstr : string;
  s0: string;
   
begin
//添加交易类型
  Result := False;
  if m_user.dwType = 0 then
  begin

     s0 := Self.Edit5.Text;
     if Length(s0) <> 0 then
     begin
        if Length(lpName) = 0 then
        begin
          Application.MessageBox('名称不能为空!',m_title,MB_OK + MB_ICONQUESTION )
        end
        else
        begin
          sqlstr := Format('update unit set name=''%s'' where 编号=%s',[lpName,s0]);
          DM.Qry.Close;
          DM.Qry.SQL.Clear;
          DM.Qry.SQL.Text := sqlstr;
          if 0 <> DM.Qry.ExecSQL  then
          begin
            m_IsModify := True;
            Result := True;
          end;
        end;
     end;
  end;

end;

procedure Tfrmsystem.Edit10KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = 13 then 
  begin
    Self.RzBitBtn30.Click;
  end;
end;

procedure Tfrmsystem.Edit11KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = 13 then
  begin
    Self.RzBitBtn35.Click;
  end;
end;

procedure Tfrmsystem.Edit12KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = 13 then
  begin
    Self.RzBitBtn39.Click;
  end;
end;

procedure Tfrmsystem.Edit15KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = 13 then 
  begin
    Self.RzBitBtn10.Click;
  end;  
end;

procedure Tfrmsystem.Edit4KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = 13 then 
  begin
    Self.RzBitBtn5.Click;
  end;  
end;

procedure Tfrmsystem.Edit6KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = 13 then
  begin
    Self.RzBitBtn14.Click;
  end;
end;

procedure Tfrmsystem.Edit7KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = 13 then 
  begin
    Self.RzBitBtn18.Click;
  end;
end;

procedure Tfrmsystem.Edit8KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = 13 then 
  begin
    Self.RzBitBtn22.Click;
  end;
end;

procedure Tfrmsystem.Edit8KeyPress(Sender: TObject; var Key: Char);
begin
  if (key<>#46) and ((key < #48) or (key > #57)) and (key <> #8) then//如果输入不是数字或小数点（#46代表小数点）
  begin
    key:=#0; //取消输入的内容（#0代表空值）
  end;
end;

procedure Tfrmsystem.Edit9KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = 13 then 
  begin
    Self.RzBitBtn26.Click;
  end;
end;

procedure Tfrmsystem.FormCreate(Sender: TObject);
begin
  Self.ComboBox1.ItemIndex := 0;
  Self.pcSys.TabIndex := 0;
end;

procedure Tfrmsystem.FormShow(Sender: TObject);
begin

  Self.RzBitBtn3.Click;
  Self.RzBitBtn4.Click;
  Self.RzBitBtn8.Click;
  Self.RzBitBtn17.Click;
  Self.RzBitBtn21.Click;
  Self.RzBitBtn25.Click;
  Self.RzBitBtn29.Click;
  Self.RzBitBtn33.Click;
  Self.RzBitBtn38.Click;
  Self.RzBitBtn42.Click;
  Self.RzEdit1.Text := m_AppName;

  Self.RzEdit2.Text :=GetFilePath + 'Main.jpg';
end;  

procedure Tfrmsystem.ListView10Click(Sender: TObject);
begin
  if Self.ListView10.Selected <> nil then
  begin
    Self.Edit12.Text := Self.ListView10.Selected.SubItems.Strings[0];
    Self.Edit5.Text := Self.ListView10.Selected.Caption;
  end;

end;

procedure Tfrmsystem.ListView1Click(Sender: TObject);
var
  szuser : string;
  szpass : string;
  szauth : string;
  i : Integer;

begin
  //修改账号密码
  if Self.ListView1.Selected <> nil then
  begin
    Self.Edit1.Text := Self.ListView1.Selected.SubItems.Strings[0];
    //Self.Edit2.Text := Self.ListView1.Selected.SubItems.Strings[1];

    szauth := Self.ListView1.Selected.SubItems.Strings[1];
    i := Self.ComboBox1.Items.IndexOf(szauth);
    if i >= 0 then Self.ComboBox1.ItemIndex := i;

    if m_user.dwType <> 0 then
    begin
      Self.Edit1.Enabled := False;
      Self.ComboBox1.Enabled := False;
    end
    else
    begin
      Self.Edit1.Enabled := true;
      Self.ComboBox1.Enabled := True;
    end;

  end;

end;

procedure Tfrmsystem.ListView2Click(Sender: TObject);
var
  szuser : string;
  szpass : string;
  szauth : string;
  i : Integer;

begin
  if m_user.dwType = 0 then
  begin
    //修改账号密码
    if Self.ListView2.Selected <> nil then
    begin
      Self.Edit15.Text := Self.ListView2.Selected.SubItems.Strings[0];
      Self.Edit16.Text := Self.ListView2.Selected.SubItems.Strings[1];
      Self.Edit3.Text := Self.ListView2.Selected.Caption;

    end;
  end;
end;

procedure Tfrmsystem.ListView3Click(Sender: TObject);
begin
  if Self.ListView3.Selected <> nil then
  begin
    Self.Edit4.Text := Self.ListView3.Selected.SubItems.Strings[0];
    Self.Edit5.Text := Self.ListView3.Selected.Caption;

  end;

end;

procedure Tfrmsystem.ListView3KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = 46 then Self.RzBitBtn6.Click;

end;

procedure Tfrmsystem.ListView4Click(Sender: TObject);
begin
  if Self.ListView4.Selected <> nil then
  begin
    Self.Edit6.Text := Self.ListView4.Selected.SubItems.Strings[0];
    Self.Edit5.Text := Self.ListView4.Selected.Caption;

  end;

end;

procedure Tfrmsystem.ListView4KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = 46 then Self.RzBitBtn15.Click;

end;

procedure Tfrmsystem.ListView5Click(Sender: TObject);
begin
  if Self.ListView5.Selected <> nil then
  begin
    Self.Edit7.Text := Self.ListView5.Selected.SubItems.Strings[0];
    Self.Edit5.Text := Self.ListView5.Selected.Caption;

  end;

end;

procedure Tfrmsystem.ListView5KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = 46 then Self.RzBitBtn19.Click;

end;

procedure Tfrmsystem.ListView6Click(Sender: TObject);
begin
  if Self.ListView6.Selected <> nil then
  begin
    Self.Edit8.Text := Self.ListView6.Selected.SubItems.Strings[0];
    Self.Edit5.Text := Self.ListView6.Selected.Caption;

  end;
end;

procedure Tfrmsystem.ListView6KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = 46 then Self.RzBitBtn23.Click;
  
end;

procedure Tfrmsystem.ListView7Click(Sender: TObject);
begin
  if Self.ListView7.Selected <> nil then
  begin
    Self.Edit9.Text := Self.ListView7.Selected.SubItems.Strings[0];
    Self.Edit5.Text := Self.ListView7.Selected.Caption;

  end;
end;

procedure Tfrmsystem.ListView7KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = 46 then Self.RzBitBtn27.Click;

end;

procedure Tfrmsystem.ListView8Click(Sender: TObject);
begin
  if Self.ListView8.Selected <> nil then
  begin
    Self.Edit10.Text := Self.ListView8.Selected.SubItems.Strings[0];
    Self.Edit5.Text := Self.ListView8.Selected.Caption;

  end;
end;

procedure Tfrmsystem.ListView8KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = 46 then Self.RzBitBtn31.Click;
  
end;

procedure Tfrmsystem.ListView9Click(Sender: TObject);
begin
  if Self.ListView9.Selected <> nil then
  begin
    Self.Edit11.Text := Self.ListView9.Selected.SubItems.Strings[0];
    Self.Edit5.Text := Self.ListView9.Selected.Caption;

  end;

end;

procedure Tfrmsystem.ListView9KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = 46 then Self.RzBitBtn36.Click;
end;

procedure Tfrmsystem.RzBitBtn10Click(Sender: TObject);
var
  s : string;
  sqlstr : string;
  s1: string;
   
begin
//添加交易类型
  if m_user.dwType = 0 then
  begin
    s := Self.Edit15.Text;
    s1:= Self.Edit16.Text;
  
    if Length(s) = 0 then
    begin
      Self.Edit15.SetFocus;
      Application.MessageBox('交易名称不得为空!',m_title,MB_OK + MB_ICONHAND );
    end
    else
    begin

      with DM.Qry do
      begin
        Close;
        SQL.Clear;
        SQL.Text := 'Select *from pay_type where pay_name="' + s + '"';
        Open;
        if RecordCount <> 0 then
        begin
          Application.MessageBox('交易名称不已存在!',m_title,MB_OK + MB_ICONHAND );
          Exit;
        end;  
      end;



      sqlstr := Format('insert into pay_type (pay_name,remarks) VALUES (''%s'',''%s'')',
                      [s,s1]);

      DM.Qry.Close;
      DM.Qry.SQL.Clear;
      DM.Qry.SQL.Text := sqlstr;
      if DM.Qry.ExecSQL <> 0 then
      begin
        Application.MessageBox('添加成功!',m_title,MB_OK + MB_ICONQUESTION );
        m_IsModify := True;
      end
      else
        Application.MessageBox('添加失败!',m_title,MB_OK + MB_ICONHAND );
    end;

    Self.RzBitBtn4.Click;
  end;

end;

procedure Tfrmsystem.RzBitBtn11Click(Sender: TObject);
var
  sqlstr : string;
  s1 , s2 , s3 : string;

begin
  if m_user.dwType = 0 then
  begin
      s1 := Self.Edit15.Text;
      s2 := Self.Edit16.Text;
      s3 := Self.Edit3.Text;

      if Length(s1) = 0 then
      begin
        Self.Edit15.SetFocus;

        Application.MessageBox('请填写修改交易名称!',m_title,MB_OK + MB_ICONQUESTION )
      end
      else
      begin

        sqlstr := Format('update pay_type set pay_name=''%s'',remarks=''%s'' where 编号=%s',
                                              [s1,s2,s3 ]);
        DM.Qry.Close;
        DM.Qry.SQL.Clear;
        DM.Qry.SQL.Text := sqlstr;

        if 0 <> DM.Qry.ExecSQL  then
        begin
           m_IsModify := True;
           Application.MessageBox('修改成功!',m_title,MB_OK + MB_ICONQUESTION );
        end
        else
           Application.MessageBox('修改失败!',m_title,MB_OK + MB_ICONQUESTION )
      end;

      Self.RzBitBtn4.Click;
  end;
end;

procedure Tfrmsystem.RzBitBtn12Click(Sender: TObject);
var
  s , Name : string;

begin
  if m_user.dwType = 0 then
  begin
     if Assigned( Self.ListView1.Selected )  then
     begin
        s := Self.ListView1.Selected.Caption;
        Name := Self.ListView1.Selected.SubItems.Strings[0];
        if StrToInt(s ) = 11 then
        begin
          Application.MessageBox('顶级账号禁止删除!',m_title,MB_OK + MB_ICONHAND );
        end
        else
        begin
          if MessageBox(handle, PChar('你确认要删除"' + Name + '"吗?'),
            m_title, MB_ICONQUESTION + MB_YESNO + MB_DEFBUTTON2) = IDYES then
          begin
            if Length(Name) <> 0 then
            begin
              s := 'delete from users where 编号=' + s ;
              OutputLog(s);
              DM.Qry.Close;
              DM.Qry.SQL.Clear;
              DM.Qry.SQL.Text := s;
              if 0 <> DM.Qry.ExecSQL then
              begin
              //   Application.MessageBox('删除成功!',m_title,MB_OK + MB_ICONQUESTION );
                 m_IsModify := True;
                 Self.RzBitBtn3.Click;
              end
              else
                 Application.MessageBox('删除失败!',m_title,MB_OK + MB_ICONQUESTION );
            end;

          end;

        end;

     end;

  end;
end;

procedure Tfrmsystem.RzBitBtn13Click(Sender: TObject);
var
  s , Name : string;

begin
  if m_user.dwType = 0 then
  begin
    if Assigned( Self.ListView2.Selected )  then
    begin
      s := Self.ListView2.Selected.Caption;
      Name := Self.ListView2.Selected.SubItems.Strings[0];
      if MessageBox(handle, PChar('你确认要删除"' + Name + '"吗?'),
        m_title, MB_ICONQUESTION + MB_YESNO + MB_DEFBUTTON2) = IDYES then
      begin
        if Length(Name) <> 0 then
        begin
          s := 'delete from pay_type where 编号=' + s ;

          DM.Qry.Close;
          DM.Qry.SQL.Clear;
          DM.Qry.SQL.Text := s;
          if 0 <> DM.Qry.ExecSQL then
          begin
             m_IsModify := True;
          //   Application.MessageBox('删除成功!',m_title,MB_OK + MB_ICONQUESTION );
          end
          else
             Application.MessageBox('删除失败!',m_title,MB_OK + MB_ICONQUESTION );


          Self.RzBitBtn4.Click;
        end;
      end;
    end;
  end;
end;

procedure Tfrmsystem.RzBitBtn14Click(Sender: TObject);
var
  szName : string;

begin
  szName := Self.Edit6.Text;
  if AddUnit(szName,1) then Self.RzBitBtn17.Click;
end;

procedure Tfrmsystem.RzBitBtn15Click(Sender: TObject);
var
  s , Name : string;
begin
  if Self.ListView4.Selected <> nil then
  begin
     Name := Self.ListView4.Selected.SubItems.Strings[0];
     s := Self.ListView4.Selected.Caption;
     if DeleteUnit(s,Name) then
     begin
       Self.RzBitBtn17.Click;
     end;
  end else begin
    Application.MessageBox('请选择要删除的名称!',m_title,MB_OK + MB_ICONQUESTION );
  end;
end;

procedure Tfrmsystem.RzBitBtn16Click(Sender: TObject);
begin
  if ModifyUnit(Self.Edit6.Text,0) then Self.RzBitBtn17.Click;
end;

function Tfrmsystem.GetUnitList(List : TListView ; chType : Integer):Boolean;stdcall;
var
  name , Remarks , id : string;
  ListItem : TListItem;
  i : Integer;
  s : string;

begin
  if m_user.dwType = 0  then
  begin
      DM.Qry.Close;
      DM.Qry.SQL.Clear;
      DM.Qry.SQL.Text := 'Select * from unit where chType=' + IntToStr(chType);
      DM.Qry.Open;
      i:=0;

      if not DM.Qry.IsEmpty then
      begin
        List.Clear;
        while not DM.Qry.Eof do
        begin
          id   := DM.Qry.FieldByName('编号').AsString;
          name := DM.Qry.FieldByName('name').AsString;
          Inc(i);
          //有所有权限
          ListItem := List.Items.Add ;
          ListItem.Caption := id;
          ListItem.SubItems.Add( name );
          DM.Qry.Next;
        end;
      end;

  end;

end;

procedure Tfrmsystem.RzBitBtn17Click(Sender: TObject);
begin
  GetUnitList(Self.ListView4,1);
end;  

procedure Tfrmsystem.RzBitBtn18Click(Sender: TObject);
var szName : string;
begin
  szName := Self.Edit7.Text;
  if AddUnit(szName,2) then Self.RzBitBtn21.Click;
  
end;


procedure Tfrmsystem.RzBitBtn19Click(Sender: TObject);
var
  s , Name : string;
begin
  if Self.ListView5.Selected <> nil then
  begin
     Name := Self.ListView5.Selected.SubItems.Strings[0];
     s := Self.ListView5.Selected.Caption;
     if DeleteUnit(s,Name) then
     begin
       Self.RzBitBtn21.Click;
     end;
  end else begin
    Application.MessageBox('请选择要删除的名称!',m_title,MB_OK + MB_ICONQUESTION );
  end;
end;

procedure Tfrmsystem.RzBitBtn1Click(Sender: TObject);
var
  szUser : string;
  szPass : string;
  szAuth : Integer;
  sqlstr : string;
  
begin
  if m_user.dwType = 0  then
  begin
    szUser := Self.Edit1.Text;
    if Length(szUser) = 0 then
    begin                              
      Application.MessageBox('添加账号不得为空!',m_title,MB_OK + MB_ICONHAND );
    end
    else
    begin
      with DM.Qry do
      begin
        Close;
        SQL.Clear;
        SQL.Text := 'Select *from users where ch_name="' + szUser + '"';
        Open;
        if RecordCount <> 0 then
        begin
          Application.MessageBox('账号已存在!',m_title,MB_OK + MB_ICONHAND );
          Exit;
        end;  
      end;

      
      szPass := Self.Edit2.Text;
      if Length(szPass) = 0 then
      begin
        Application.MessageBox('添加密码不得为空!',m_title,MB_OK + MB_ICONHAND );
      end
      else
      begin
        szAuth := Self.ComboBox1.ItemIndex;
        sqlstr := Format('Insert into users (ch_name,ch_pass,ch_type) VALUES ("%s","%s",''%d'')',
                        [szUser,md5s(szPass),szAuth]);

        OutputLog(sqlstr);
        with DM.Qry do
        begin
          Close;
          SQL.Clear;
          SQL.Text := sqlstr;
          if ExecSQL <> 0 then
          begin
            m_IsModify := True;
            Application.MessageBox('账号添加成功!',m_title,MB_OK + MB_ICONQUESTION );
            Self.RzBitBtn3.Click;
          end
          else
            Application.MessageBox('账号添加失败!',m_title,MB_OK + MB_ICONHAND );
        end;  

      end;

    end;

  end
  else
  begin

    Application.MessageBox('您无权限添加账号','警告',MB_OK + MB_ICONHAND);
    
  end;  
end;

procedure Tfrmsystem.RzBitBtn20Click(Sender: TObject);
begin
//添加交易类型
  if ModifyUnit(Self.Edit7.Text,0) then Self.RzBitBtn21.Click;
end;

procedure Tfrmsystem.RzBitBtn21Click(Sender: TObject);
begin
    GetUnitList(Self.ListView5,2);
end;

procedure Tfrmsystem.RzBitBtn22Click(Sender: TObject);
var szName : string;
begin
  szName := Self.Edit8.Text;
  if AddUnit(szName,3) then Self.RzBitBtn25.Click;
end;

procedure Tfrmsystem.RzBitBtn23Click(Sender: TObject);
var
  s , Name : string;
begin
  if Self.ListView6.Selected <> nil then
  begin
     Name := Self.ListView6.Selected.SubItems.Strings[0];
     s := Self.ListView6.Selected.Caption;
     if DeleteUnit(s,Name) then
     begin
       Self.RzBitBtn25.Click;
     end;
  end else begin
    Application.MessageBox('请选择要删除的名称!',m_title,MB_OK + MB_ICONQUESTION );
  end;
end;

procedure Tfrmsystem.RzBitBtn24Click(Sender: TObject);
begin
  if ModifyUnit(Self.Edit8.Text,0) then Self.RzBitBtn25.Click;
end;

procedure Tfrmsystem.RzBitBtn25Click(Sender: TObject);
begin
  GetUnitList(Self.ListView6,3);
end;

procedure Tfrmsystem.RzBitBtn26Click(Sender: TObject);
var szName : string;
begin
  szName := Self.Edit9.Text;
  if AddUnit(szName,4) then Self.RzBitBtn29.Click;
end;

procedure Tfrmsystem.RzBitBtn27Click(Sender: TObject);
var
  s , Name : string;
begin
  if Self.ListView7.Selected <> nil then
  begin
     Name := Self.ListView7.Selected.SubItems.Strings[0];
     s := Self.ListView7.Selected.Caption;
     if DeleteUnit(s,Name) then
     begin
       Self.RzBitBtn29.Click;
     end;  
  end else begin
    Application.MessageBox('请选择要删除的名称!',m_title,MB_OK + MB_ICONQUESTION );
  end;
end;

procedure Tfrmsystem.RzBitBtn28Click(Sender: TObject);
begin
  if ModifyUnit(Self.Edit9.Text,4) then Self.RzBitBtn29.Click;
end;

procedure Tfrmsystem.RzBitBtn29Click(Sender: TObject);
begin
  GetUnitList(Self.ListView7,4);
end;

procedure Tfrmsystem.RzBitBtn2Click(Sender: TObject);
var
  szpass,szuser : string;
  szAuth : Integer;
  sqlstr : string;
  i : Integer;
  IsNode : Integer;
  
begin
  szpass := Self.Edit2.Text;
  szAuth := Self.ComboBox1.ItemIndex;
  szuser := Self.Edit1.Text;
  if Length(szuser) = 0 then
  begin
     Application.MessageBox('账号不得为空!',m_title,MB_OK + MB_ICONHAND );
  end
  else
  begin
    with DM.Qry do
    begin
        sqlstr := Format('Select * from users where ch_name=''%s''',[szuser]);
        Close;
        SQL.Clear;
        SQL.Add( sqlstr );
        Open;
        if RecordCount = 0 then
        begin
          Application.MessageBox('账号不存在!',m_title,MB_OK + MB_ICONHAND );
        end
        else
        begin
          IsNode := FieldByName(m_Numer).AsInteger;

          Edit;
          if Length(szpass) = 0 then
          begin
            case m_user.dwType of
                0:
                begin
                  FieldByName('ch_name').Value := szuser;
                  if IsNode <> 11 then FieldByName('ch_type').Value := szAuth;
                  m_IsModify := True;
                end;
                1:
                begin
                  m_IsModify := True;
                  FieldByName('ch_pass').Value := MD5S( szpass );
                end;
             end;
          end
          else
          begin
             case m_user.dwType of
                0:
                begin
                  FieldByName('ch_name').Value := szuser;
                  FieldByName('ch_pass').Value := MD5S( szpass );
                  if IsNode <> 11 then FieldByName('ch_type').Value := szAuth;
                  m_IsModify := True;
                end;
                1:
                begin
                  m_IsModify := True;
                  FieldByName('ch_pass').Value := MD5S( szpass );
                end;
             end;
          end;

          Post;
          Self.RzBitBtn3.Click;
          Application.MessageBox('修改成功!',m_title,MB_OK + MB_ICONQUESTION );
        end;

    end;
  end;

end;

procedure Tfrmsystem.RzBitBtn30Click(Sender: TObject);
var szName : string;
begin
  szName := Self.Edit10.Text;
  if AddUnit(szName,5) then Self.RzBitBtn33.Click;
end;

procedure Tfrmsystem.RzBitBtn31Click(Sender: TObject);
var
  s , Name : string;
begin
  if Self.ListView8.Selected <> nil then
  begin
     Name := Self.ListView8.Selected.SubItems.Strings[0];
     s := Self.ListView8.Selected.Caption;
     if DeleteUnit(s,Name) then
     begin
       Self.RzBitBtn33.Click;
     end;
  end else begin
    Application.MessageBox('请选择要删除的名称!',m_title,MB_OK + MB_ICONQUESTION );
  end;
end;

procedure Tfrmsystem.RzBitBtn32Click(Sender: TObject);
begin
  if ModifyUnit(Self.Edit10.Text,0) then Self.RzBitBtn33.Click;
end;

procedure Tfrmsystem.RzBitBtn33Click(Sender: TObject);
begin
  GetUnitList(Self.ListView8,5);
end;

procedure Tfrmsystem.RzBitBtn34Click(Sender: TObject);
var
  s : string;
  fileName : string;
  szIni : TIniFile;
  
begin
  if Self.OpenPictureDialog1.Execute(Handle) then
  begin
    s := Self.OpenPictureDialog1.FileName;
    fileName := GetFilePath + 'Main.jpg';
    Self.RzEdit2.Text := s;
    if CopyFile(PChar(s),PChar(fileName) , False) then
    begin
      Application.MessageBox('保存成功,重启动软件后生效!',m_title,MB_OK + MB_ICONQUESTION )
    end;

  end;

end;

procedure Tfrmsystem.RzBitBtn35Click(Sender: TObject);
var szName : string;
begin
  szName := Self.Edit11.Text;
  if AddUnit(szName,6) then Self.RzBitBtn38.Click;
end;

procedure Tfrmsystem.RzBitBtn36Click(Sender: TObject);
var
  s , Name : string;
begin
  if Self.ListView9.Selected <> nil then
  begin
     Name := Self.ListView9.Selected.SubItems.Strings[0];
     s := Self.ListView9.Selected.Caption;
     if DeleteUnit(s,Name) then
     begin
       Self.RzBitBtn38.Click;
     end;
  end else begin
    Application.MessageBox('请选择要删除的名称!',m_title,MB_OK + MB_ICONQUESTION );
  end;
end;

procedure Tfrmsystem.RzBitBtn37Click(Sender: TObject);
begin
  if ModifyUnit(Self.Edit11.Text,6) then Self.RzBitBtn38.Click;
end;

procedure Tfrmsystem.RzBitBtn38Click(Sender: TObject);
begin
  GetUnitList(Self.ListView9,6);
end;

procedure Tfrmsystem.RzBitBtn39Click(Sender: TObject);
var szName : string;
begin
  szName := Self.Edit12.Text;
  if AddUnit(szName,7) then Self.RzBitBtn42.Click;
end;

procedure Tfrmsystem.RzBitBtn3Click(Sender: TObject);
var
  username , password : string;
  auth : Integer;
  ListItem : TListItem;
  i : Integer;
  s : string;
  
begin
  DM.Qry.Close;
  DM.Qry.SQL.Clear;
  DM.Qry.SQL.Text := 'Select * from users';
  DM.Qry.Open;
  i:=0;

  if not DM.Qry.IsEmpty then
  begin
    Self.ListView1.Clear;
    
    while not DM.Qry.Eof do
    begin
      i := DM.Qry.FieldByName(m_Numer).AsInteger;
      
      auth     := DM.Qry.FieldByName('ch_type').AsInteger;
      username := DM.Qry.FieldByName('ch_name').AsString;
      password := DM.Qry.FieldByName('ch_pass').AsString;

      if (auth >= 0) and (auth <= Self.ComboBox1.Items.Count) then
      begin
        s := Self.ComboBox1.Items[auth];
      end;

      case m_user.dwType of
        0:
        begin
          //有所有权限
          ListItem := Self.ListView1.Items.Add ;
          ListItem.Caption := IntToStr(i);
          ListItem.SubItems.Add( username );
        //  ListItem.SubItems.Add( password );
          ListItem.SubItems.Add( s ); //权限

        end;
        1:
        begin
          if SameText( m_user.dwUserName ,username ) then
          begin

            ListItem := Self.ListView1.Items.Add ;
            ListItem.Caption := IntToStr(i);
            ListItem.SubItems.Add( username );
          //  ListItem.SubItems.Add( password );
            ListItem.SubItems.Add( s ); //权限

          end;

        end;    
      end;

      DM.Qry.Next;
    end;
  end;
end;

procedure Tfrmsystem.RzBitBtn40Click(Sender: TObject);
var
  s , Name : string;
begin
  if Self.ListView10.Selected <> nil then
  begin
     Name := Self.ListView10.Selected.SubItems.Strings[0];
     s := Self.ListView10.Selected.Caption;
     if DeleteUnit(s,Name) then
     begin
       Self.RzBitBtn42.Click;
     end;
  end else begin
    Application.MessageBox('请选择要删除的名称!',m_title,MB_OK + MB_ICONQUESTION );
  end;
end;

procedure Tfrmsystem.RzBitBtn41Click(Sender: TObject);
begin
  if ModifyUnit(Self.Edit12.Text,7) then Self.RzBitBtn42.Click;
end;

procedure Tfrmsystem.RzBitBtn42Click(Sender: TObject);
begin
  GetUnitList(Self.ListView10,7);
end;

procedure Tfrmsystem.RzBitBtn4Click(Sender: TObject);
var
  name , Remarks , id : string;
  ListItem : TListItem;
  i : Integer;
  s : string;
  
begin
  if m_user.dwType = 0  then
  begin
      DM.Qry.Close;
      DM.Qry.SQL.Clear;
      DM.Qry.SQL.Text := 'Select * from pay_type';
      DM.Qry.Open;
      i:=0;

      if not DM.Qry.IsEmpty then
      begin
        Self.ListView2.Clear;

        while not DM.Qry.Eof do
        begin
          id   := DM.Qry.FieldByName(m_Numer).AsString;
          name := DM.Qry.FieldByName('pay_name').AsString;
          Remarks := DM.Qry.FieldByName('Remarks').AsString;
          Inc(i);
          //有所有权限
          ListItem := Self.ListView2.Items.Add ;
          ListItem.Caption := id;
          ListItem.SubItems.Add( name );
          ListItem.SubItems.Add( Remarks );

          DM.Qry.Next;
        end;
      end;

  end;

end;

function Tfrmsystem.AddUnit(lpName : string; chType:Integer):Boolean;stdcall;
var
  s : string;

begin
  Result := False;
  //单位添加
  if Length(lpName) = 0 then 
  begin
    Application.MessageBox('请填写名称!',m_title,MB_OK + MB_ICONQUESTION )
  end
  else
  begin
      with DM.Qry do
      begin
        Close;
        SQL.Clear;
        SQL.Text := 'Select *from unit where chType='+ IntToStr(chType) +' and name="'+ lpName +'"';
        Open;
        if 0 <> RecordCount then
        begin
          Application.MessageBox('此名称已存在!',m_title,MB_OK + MB_ICONQUESTION )
        end
        else
        begin

          s := Format('insert into unit (name,chType) VALUES (''%s'',''%d'')',
                        [lpName,chType]);
          SQL.Text := s;
          if 0 <> ExecSQL then
          begin
            m_IsModify := True;
            Result := True;
          end
          else
            Application.MessageBox('添加失败!',m_title,MB_OK + MB_ICONHAND)
        end;

      end;
  end;    


end;

procedure Tfrmsystem.RzBitBtn5Click(Sender: TObject);
begin
  if AddUnit(Self.Edit4.Text,0) then Self.RzBitBtn8.Click;
end;

function Tfrmsystem.DeleteUnit(lpNumber : string; lpName : string):Boolean;stdcall;
var
  s : string;
  
begin
  if m_user.dwType = 0 then
  begin
      if MessageBox(handle, PChar('你确认要删除"' + lpName + '"吗?'),
        m_title, MB_ICONQUESTION + MB_YESNO + MB_DEFBUTTON2) = IDYES then
      begin
        if Length(Name) <> 0 then
        begin
          s := 'delete from unit where 编号=' + lpNumber ;
          DM.Qry.Close;
          DM.Qry.SQL.Clear;
          DM.Qry.SQL.Text := s;
          if 0 <> DM.Qry.ExecSQL then
          begin
            m_IsModify := True;
          end
          else
             Application.MessageBox('删除失败!',m_title,MB_OK + MB_ICONQUESTION );

        end;

      end;
  end;
end;

procedure Tfrmsystem.RzBitBtn6Click(Sender: TObject);
var
  s , Name : string;
begin
  if Self.ListView3.Selected <> nil then
  begin
     Name := Self.ListView3.Selected.SubItems.Strings[0];
     s := Self.ListView3.Selected.Caption;
     if DeleteUnit(s,Name) then
     begin
       Self.RzBitBtn8.Click;
     end;
  end else begin
    Application.MessageBox('请选择要删除的名称!',m_title,MB_OK + MB_ICONQUESTION );
  end;
end;

procedure Tfrmsystem.RzBitBtn7Click(Sender: TObject);
var
  s : string;
  sqlstr : string;
  s1 , s0: string;
   
begin
//添加交易类型
  if m_user.dwType = 0 then
  begin
     try
       s0 := Self.Edit5.Text;
       if Length(s0) <> 0 then
       begin
         s1 := Self.Edit4.Text;
          if Length(s1) = 0 then
          begin
            Self.Edit4.SetFocus;
            Application.MessageBox('请填写名称!',m_title,MB_OK + MB_ICONQUESTION )
          end
          else
          begin

            sqlstr := Format('update unit set name=''%s'' where 编号=%s',
                                                  [s1,s0]);
            m_IsModify := True;
            DM.Qry.Close;
            DM.Qry.SQL.Clear;
            DM.Qry.SQL.Text := sqlstr;
            if 0 <> DM.Qry.ExecSQL  then
            //   Application.MessageBox('修改成功!',m_title,MB_OK + MB_ICONQUESTION )
            else
            //   Application.MessageBox('修改失败!',m_title,MB_OK + MB_ICONQUESTION )
          end;
          Self.RzBitBtn8.Click;
       end;
     except
     end;
      
  end;

end;
procedure Tfrmsystem.RzBitBtn8Click(Sender: TObject);
begin
  GetUnitList(Self.ListView3,0);
end;


function IsMdb(AFileName : string  ):Integer;
var
  ADOConn : TADOConnection;
  s : string;
  TableNameList : TStringList;
  i : Integer;
  SuccessFlag , blInListFlag : Boolean;
  
begin
  ADOConn := TADOConnection.Create(nil);
  try
    OutputLog(AFileName);
    
    ADOConn.Connected := False;
    SuccessFlag := False;
    ADOConn.LoginPrompt := False;
    s := 'Provider=Microsoft.Jet.OLEDB.4.0;Data Source=' + AFileName + ';Persist Security Info=False;';
    try

      ADOConn.ConnectionString := s;
      ADOConn.Connected := True;
      SuccessFlag := True;

    except
      SuccessFlag := False;
    end;

    if SuccessFlag and ADOConn.Connected then
    begin

        TableNameList := TStringList.Create();
        try
          ADOConn.GetTableNames(TableNameList, False);

          for I := 0 to TableNameList.Count - 1 do
          begin
             blInListFlag := False;

             if TableNameList[i] = 'users' then  blInListFlag := True;
             if TableNameList[i] = 'contract_tree' then  blInListFlag := True;
             if TableNameList[i] = 'expenditure' then  blInListFlag := True;
             if TableNameList[i] = 'income' then  blInListFlag := True;
             if TableNameList[i] = 'sk_BorrowAccount' then  blInListFlag := True;
             if TableNameList[i] = 'out_record' then  blInListFlag := True;
             if TableNameList[i] = 'out_tree' then  blInListFlag := True;
             if TableNameList[i] = 'pactmoney' then  blInListFlag := True;
             if TableNameList[i] = 'pay_record' then  blInListFlag := True;
             if TableNameList[i] = 'pay_tree' then  blInListFlag := True;
             if TableNameList[i] = 'unit' then  blInListFlag := True;

          end;

          if blInListFlag then
              Result := 0
          else
              Result := 2;

        //  OutputLog(IntToStr( Result ));
          
        finally
          TableNameList.Free;
        end;

    end
    else
      Result := 1;

  finally
    ADOConn.Free;
  end;
end;


procedure Tfrmsystem.RzBitBtn9Click(Sender: TObject);
var
  SourceFile : string;
  FileName : string;
  s : string;

begin

  FileName := GetFilePath + '\Backup.mdb';
  if FileExists(FileName) then
  begin
    s := 'Provider=Microsoft.Jet.OLEDB.4.0;Data Source=' + FileName + ';Persist Security Info=False;';
    try
      Self.ADOconn.Connected := False;
      Self.ADOconn.ConnectionString := s;
      Self.ADOconn.Connected := True;
      RestoreFile(FileName,Self.Label7);
    except
      Application.MessageBox(PChar('打开备份数据库失败'),PChar('操作失败'), MB_ICONINFORMATION);
    end;
  end;

end;

function Tfrmsystem.RestoreFile(StrRestorePathFile:TFileName;infoLabel:TLabel):Boolean;
var
  SourceFile,strTheTempFileName,DespFile: string;
  SuccessFlag: Boolean;
  m:TMemoryStream;
  Compact: OleVariant;
  dwType : Integer;
  dwQry  : TADOQuery;
  i : Integer;
  s : string;
  
begin
  if m_user.dwType = 0 then
  begin
      SourceFile :=  GetFilePath+ '\Data.mdb';
      if FileExists(StrRestorePathFile) then
      begin
        case IsMdb(StrRestorePathFile) of
          0:
          begin
              SuccessFlag := True;
              infoLabel.Caption := '正在恢复，请等待......';
              infoLabel.Update;
              try

                DM.Qry.Close;
                DM.Qry.SQL.Clear;
                DM.Qry.SQL.Text := 'select * from [users] where ch_name=''' + 'Administrator' +''' and ch_pass='''+ MD5S('123456') +'''';
                DM.Qry.Open;

                if DM.Qry.RecordCount = 0 then
                begin
                  infoLabel.Caption := '无法验证用户信息！';
                  Application.MessageBox(PChar(infoLabel.Caption),PChar('操作失败'), MB_ICONINFORMATION);
                end
                else
                begin

                  dwType := DM.Qry.FieldByName('ch_type').AsInteger;
                  if dwType = 0 then
                  begin
                    s := 'select *from contract_tree';
                    DM.Qry.Close;
                    DM.Qry.SQL.Clear;
                    DM.Qry.SQL.Text := s;

                    Qry.Close;
                    Qry.SQL.Clear;
                    Qry.SQL.Text := s;

                    try
                      DM.Qry.Open;
                      Qry.Open;

                      while not Qry.Eof do
                      begin

                        if not DM.Qry.Locate(m_Numer,Qry.FieldByName(m_Numer).AsInteger,[]) then  DM.Qry.Append;

                          DM.Qry.Edit;
                          DM.Qry.FieldByName('parent').AsInteger    := Qry.FieldByName('parent').AsInteger;
                          DM.Qry.FieldByName('PID').AsInteger       := Qry.FieldByName('PID').AsInteger;
                          DM.Qry.FieldByName('name').AsString       := Qry.FieldByName('name').AsString;
                          DM.Qry.FieldByName('code').AsString       := Qry.FieldByName('code').AsString;
                          DM.Qry.FieldByName('sum_money').AsCurrency:= Qry.FieldByName('sum_money').AsCurrency;
                          DM.Qry.FieldByName('photo').AsString      := Qry.FieldByName('photo').AsString;
                          DM.Qry.FieldByName('remarks').AsString    := Qry.FieldByName('remarks').AsString;
                          DM.Qry.Post;
                        Qry.Next;
                      end;

                    except
                      infoLabel.Caption := '无法打开信息数据！';
                      Application.MessageBox(PChar(infoLabel.Caption),PChar('操作失败'), MB_ICONINFORMATION);
                      exit;
                    end;

                    //**********************************************************
                    {
                      支款合同
                    }
                    //**********************************************************
                    s := 'select *from sk_BorrowAccount';
                    DM.Qry.Close;
                    DM.Qry.SQL.Clear;
                    DM.Qry.SQL.Text := s;

                    Qry.Close;
                    Qry.SQL.Clear;
                    Qry.SQL.Text := s;

                    try
                      DM.Qry.Open;
                      Qry.Open;

                      while not Qry.Eof do
                      begin

                        if not DM.Qry.Locate(m_Numer,Qry.FieldByName(m_Numer).AsInteger,[]) then  DM.Qry.Append;

                          DM.Qry.Edit;
                          DM.Qry.FieldByName('parent').AsInteger    := Qry.FieldByName('parent').AsInteger;
                          DM.Qry.FieldByName('PID').AsInteger       := Qry.FieldByName('PID').AsInteger;
                          DM.Qry.FieldByName('name').AsString       := Qry.FieldByName('name').AsString;
                          DM.Qry.FieldByName('code').AsString       := Qry.FieldByName('code').AsString;
                          DM.Qry.FieldByName('sum_money').AsCurrency:= Qry.FieldByName('sum_money').AsCurrency;
                          DM.Qry.FieldByName('photo').AsString      := Qry.FieldByName('photo').AsString;
                          DM.Qry.FieldByName('remarks').AsString    := Qry.FieldByName('remarks').AsString;
                          DM.Qry.Post;
                        Qry.Next;
                      end;

                    except
                      infoLabel.Caption := '无法打开支款合同信息数据！';
                      Application.MessageBox(PChar(infoLabel.Caption),PChar('操作失败'), MB_ICONINFORMATION);
                      exit;
                    end;
                    //**********************************************************
                    {
                      支款联系人
                    }
                    //**********************************************************
                    s := 'select *from out_record';
                    DM.Qry.Close;
                    DM.Qry.SQL.Clear;
                    DM.Qry.SQL.Text := s;

                    Qry.Close;
                    Qry.SQL.Clear;
                    Qry.SQL.Text := s;

                    try
                      DM.Qry.Open;
                      Qry.Open;

                      while not Qry.Eof do
                      begin

                        if not DM.Qry.Locate(m_Numer,Qry.FieldByName(m_Numer).AsInteger,[]) then  DM.Qry.Append;

                          DM.Qry.Edit;
                          DM.Qry.FieldByName('code').AsString       := Qry.FieldByName('code').AsString;
                          DM.Qry.FieldByName('Caption').AsString    := Qry.FieldByName('Caption').AsString;
                          DM.Qry.FieldByName('fixed').AsString      := Qry.FieldByName('fixed').AsString;
                          DM.Qry.FieldByName('phone').AsString      := Qry.FieldByName('phone').AsString;
                          DM.Qry.FieldByName('address').AsString    := Qry.FieldByName('address').AsString;
                          DM.Qry.FieldByName('remarks').AsString    := Qry.FieldByName('remarks').AsString;
                          DM.Qry.Post;

                        Qry.Next;
                      end;

                    except
                      infoLabel.Caption := '无法打开支款联系人信息数据！';
                      Application.MessageBox(PChar(infoLabel.Caption),PChar('操作失败'), MB_ICONINFORMATION);
                      exit;
                    end;

                    //**********************************************************
                    {
                      付款联系人
                    }
                    //**********************************************************
                    s := 'select *from pay_record';
                    DM.Qry.Close;
                    DM.Qry.SQL.Clear;
                    DM.Qry.SQL.Text := s;

                    Qry.Close;
                    Qry.SQL.Clear;
                    Qry.SQL.Text := s;

                    try
                      DM.Qry.Open;
                      Qry.Open;

                      while not Qry.Eof do
                      begin

                        if not DM.Qry.Locate(m_Numer,Qry.FieldByName(m_Numer).AsInteger,[]) then  DM.Qry.Append;

                          DM.Qry.Edit;
                          DM.Qry.FieldByName('code').AsString       := Qry.FieldByName('code').AsString;
                          DM.Qry.FieldByName('Caption').AsString    := Qry.FieldByName('Caption').AsString;
                          DM.Qry.FieldByName('fixed').AsString      := Qry.FieldByName('fixed').AsString;
                          DM.Qry.FieldByName('phone').AsString      := Qry.FieldByName('phone').AsString;
                          DM.Qry.FieldByName('address').AsString    := Qry.FieldByName('address').AsString;
                          DM.Qry.FieldByName('remarks').AsString    := Qry.FieldByName('remarks').AsString;
                          DM.Qry.Post;

                        Qry.Next;
                      end;

                    except
                      infoLabel.Caption := '无法打开付款联系人信息数据！';
                      Application.MessageBox(PChar(infoLabel.Caption),PChar('操作失败'), MB_ICONINFORMATION);
                      exit;
                    end;

                    //**********************************************************
                    {
                      支款out_tree分类
                    }
                    //**********************************************************
                    s := 'select *from out_tree';

                    DM.Qry.Close;
                    DM.Qry.SQL.Clear;
                    DM.Qry.SQL.Text := s;

                    Qry.Close;
                    Qry.SQL.Clear;
                    Qry.SQL.Text := s;

                    try
                      DM.Qry.Open;
                      Qry.Open;

                      while not Qry.Eof do
                      begin

                        if not DM.Qry.Locate(m_Numer,Qry.FieldByName(m_Numer).AsInteger,[]) then  DM.Qry.Append;

                          DM.Qry.Edit;
                          DM.Qry.FieldByName('parent').AsInteger    := Qry.FieldByName('parent').AsInteger;
                          DM.Qry.FieldByName('PID').AsInteger       := Qry.FieldByName('PID').AsInteger;
                          DM.Qry.FieldByName('name').AsString       := Qry.FieldByName('name').AsString;
                          DM.Qry.FieldByName('code').AsString       := Qry.FieldByName('code').AsString;
                          DM.Qry.FieldByName('sum_money').AsCurrency:= Qry.FieldByName('sum_money').AsCurrency;
                          DM.Qry.FieldByName('photo').AsString      := Qry.FieldByName('photo').AsString;
                          DM.Qry.FieldByName('remarks').AsString    := Qry.FieldByName('remarks').AsString;
                          DM.Qry.Post;
                        Qry.Next;
                      end;

                    except
                      infoLabel.Caption := '无法打开支款合同信息数据！';
                      Application.MessageBox(PChar(infoLabel.Caption),PChar('操作失败'), MB_ICONINFORMATION);
                      exit;
                    end;

                    //**********************************************************
                    {
                      付款pay_tree
                    }
                    //**********************************************************
                    s := 'select *from pay_tree';
                    DM.Qry.Close;
                    DM.Qry.SQL.Clear;
                    DM.Qry.SQL.Text := s;

                    Qry.Close;
                    Qry.SQL.Clear;
                    Qry.SQL.Text := s;

                    try
                      DM.Qry.Open;
                      Qry.Open;

                      while not Qry.Eof do
                      begin

                        if not DM.Qry.Locate(m_Numer,Qry.FieldByName(m_Numer).AsInteger,[]) then  DM.Qry.Append;

                          DM.Qry.Edit;
                          DM.Qry.FieldByName('parent').AsInteger    := Qry.FieldByName('parent').AsInteger;
                          DM.Qry.FieldByName('PID').AsInteger       := Qry.FieldByName('PID').AsInteger;
                          DM.Qry.FieldByName('name').AsString       := Qry.FieldByName('name').AsString;
                          DM.Qry.FieldByName('code').AsString       := Qry.FieldByName('code').AsString;
                          DM.Qry.FieldByName('sum_money').AsCurrency:= Qry.FieldByName('sum_money').AsCurrency;
                          DM.Qry.FieldByName('photo').AsString      := Qry.FieldByName('photo').AsString;
                          DM.Qry.FieldByName('remarks').AsString    := Qry.FieldByName('remarks').AsString;
                          DM.Qry.Post;
                        Qry.Next;
                      end;

                    except
                      infoLabel.Caption := '无法打开支款合同信息数据！';
                      Application.MessageBox(PChar(infoLabel.Caption),PChar('操作失败'), MB_ICONINFORMATION);
                      exit;
                    end;

                     //**********************************************************
                    {
                       单位unit
                    }
                    //**********************************************************
                    s := 'select *from unit';
                    DM.Qry.Close;
                    DM.Qry.SQL.Clear;
                    DM.Qry.SQL.Text := s;

                    Qry.Close;
                    Qry.SQL.Clear;
                    Qry.SQL.Text := s;

                    try
                      DM.Qry.Open;
                      Qry.Open;

                      while not Qry.Eof do
                      begin

                        if not DM.Qry.Locate(m_Numer,Qry.FieldByName(m_Numer).AsInteger,[]) then  DM.Qry.Append;

                          DM.Qry.Edit;
                          DM.Qry.FieldByName('name').AsString       := Qry.FieldByName('name').AsString;
                          DM.Qry.Post;
                        Qry.Next;
                      end;

                    except
                      infoLabel.Caption := '无法打开单位信息数据！';
                      Application.MessageBox(PChar(infoLabel.Caption),PChar('操作失败'), MB_ICONINFORMATION);
                      exit;
                    end;

                    //**********************************************************
                    {
                       入库单 expenditure
                    }
                    //**********************************************************
                    s := 'select *from expenditure';
                    DM.Qry.Close;
                    DM.Qry.SQL.Clear;
                    DM.Qry.SQL.Text := s;

                    Qry.Close;
                    Qry.SQL.Clear;
                    Qry.SQL.Text := s;

                    try
                      DM.Qry.Open;
                      Qry.Open;

                      while not Qry.Eof do
                      begin

                        if not DM.Qry.Locate(m_Numer,Qry.FieldByName(m_Numer).AsInteger,[]) then  DM.Qry.Append;

                          DM.Qry.Edit;
                          DM.Qry.FieldByName('code').AsString       := Qry.FieldByName('code').AsString;
                          DM.Qry.FieldByName('numbers').AsString   := Qry.FieldByName('numbers').AsString;
                          DM.Qry.FieldByName('caption').AsString    := Qry.FieldByName('caption').AsString;
                          DM.Qry.FieldByName('content').AsString    := Qry.FieldByName('content').AsString;
                          DM.Qry.FieldByName('pay_time').AsDateTime := Qry.FieldByName('pay_time').AsDateTime;
                          DM.Qry.FieldByName('truck').AsInteger     := Qry.FieldByName('truck').AsInteger;
                          DM.Qry.FieldByName('Price').AsCurrency    := Qry.FieldByName('Price').AsCurrency;
                          DM.Qry.FieldByName('num').AsInteger       := Qry.FieldByName('num').AsInteger;
                          DM.Qry.FieldByName('Units').AsString      := Qry.FieldByName('Units').AsString;
                          DM.Qry.FieldByName('remarks').AsString    := Qry.FieldByName('remarks').AsString;

                          DM.Qry.Post;
                        Qry.Next;
                      end;

                    except
                      infoLabel.Caption := '无法打开入库单信息数据！';
                      Application.MessageBox(PChar(infoLabel.Caption),PChar('操作失败'), MB_ICONINFORMATION);
                      exit;
                    end;

                    //**********************************************************
                    {
                       付款列表 income
                    }
                    //**********************************************************
                    s := 'select *from income';
                    DM.Qry.Close;
                    DM.Qry.SQL.Clear;
                    DM.Qry.SQL.Text := s;

                    Qry.Close;
                    Qry.SQL.Clear;
                    Qry.SQL.Text := s;

                    try
                      DM.Qry.Open;
                      Qry.Open;

                      while not Qry.Eof do
                      begin

                        if not DM.Qry.Locate(m_Numer,Qry.FieldByName(m_Numer).AsInteger,[]) then  DM.Qry.Append;

                          DM.Qry.Edit;
                          DM.Qry.FieldByName('code').AsString       := Qry.FieldByName('code').AsString;
                          DM.Qry.FieldByName('ContractCode').AsString:= Qry.FieldByName('ContractCode').AsString;
                          DM.Qry.FieldByName('pay_type').AsInteger   := Qry.FieldByName('pay_type').AsInteger;
                          DM.Qry.FieldByName('small_money').AsCurrency:= Qry.FieldByName('small_money').AsCurrency;
                          DM.Qry.FieldByName('large_money').AsString := Qry.FieldByName('large_money').AsString;
                          DM.Qry.FieldByName('pay_time').AsDateTime     := Qry.FieldByName('pay_time').AsDateTime;
                          DM.Qry.FieldByName('payee').AsString       := Qry.FieldByName('payee').AsString;
                          DM.Qry.FieldByName('remarks').AsString      := Qry.FieldByName('remarks').AsString;
                          DM.Qry.FieldByName('status').AsInteger    := Qry.FieldByName('status').AsInteger;
                          DM.Qry.Post;
                        Qry.Next;
                      end;

                    except
                      infoLabel.Caption := '无法打开收款信息数据！';
                      Application.MessageBox(PChar(infoLabel.Caption),PChar('操作失败'), MB_ICONINFORMATION);
                      exit;
                    end;

                    //**********************************************************
                    {
                       付款总金额表 pactmoney
                    }
                    //**********************************************************
                    s := 'select *from pactmoney';
                    DM.Qry.Close;
                    DM.Qry.SQL.Clear;
                    DM.Qry.SQL.Text := s;

                    Qry.Close;
                    Qry.SQL.Clear;
                    Qry.SQL.Text := s;

                    try
                      DM.Qry.Open;
                      Qry.Open;

                      while not Qry.Eof do
                      begin

                        if not DM.Qry.Locate(m_Numer,Qry.FieldByName(m_Numer).AsInteger,[]) then  DM.Qry.Append;

                          DM.Qry.Edit;
                          DM.Qry.FieldByName('code').AsString       := Qry.FieldByName('code').AsString;
                          DM.Qry.FieldByName('sum_money').AsCurrency:= Qry.FieldByName('sum_money').AsCurrency;
                          DM.Qry.FieldByName('accountmoney').AsCurrency:= Qry.FieldByName('accountmoney').AsCurrency;
                          DM.Qry.FieldByName('paymoney').AsCurrency:= Qry.FieldByName('paymoney').AsCurrency;
                          DM.Qry.FieldByName('accountmoney_time').AsDateTime     := Qry.FieldByName('accountmoney_time').AsDateTime;
                          DM.Qry.FieldByName('remarks').AsString      := Qry.FieldByName('remarks').AsString;
                          DM.Qry.Post;
                        Qry.Next;
                      end;

                    except
                      infoLabel.Caption := '无法打开总金额信息数据！';
                      Application.MessageBox(PChar(infoLabel.Caption),PChar('操作失败'), MB_ICONINFORMATION);
                      exit;
                    end;

                     //**********************************************************
                    {
                       支款付款总金额表 t_pactmoney
                    }
                    //**********************************************************
                    s := 'select *from t_paymoney';
                    DM.Qry.Close;
                    DM.Qry.SQL.Clear;
                    DM.Qry.SQL.Text := s;

                    Qry.Close;
                    Qry.SQL.Clear;
                    Qry.SQL.Text := s;

                    try
                      DM.Qry.Open;
                      Qry.Open;

                      while not Qry.Eof do
                      begin

                        if not DM.Qry.Locate(m_Numer,Qry.FieldByName(m_Numer).AsInteger,[]) then  DM.Qry.Append;

                          DM.Qry.Edit;
                          DM.Qry.FieldByName('code').AsString       := Qry.FieldByName('code').AsString;
                          DM.Qry.FieldByName('sum_money').AsCurrency:= Qry.FieldByName('sum_money').AsCurrency;
                          DM.Qry.FieldByName('accountmoney').AsCurrency:= Qry.FieldByName('accountmoney').AsCurrency;
                          DM.Qry.FieldByName('paymoney').AsCurrency:= Qry.FieldByName('paymoney').AsCurrency;
                          DM.Qry.FieldByName('accountmoney_time').AsDateTime     := Qry.FieldByName('accountmoney_time').AsDateTime;
                          DM.Qry.FieldByName('remarks').AsString      := Qry.FieldByName('remarks').AsString;
                          DM.Qry.Post;
                        Qry.Next;
                      end;

                    except
                      infoLabel.Caption := '无法打开总金额信息数据！';
                      Application.MessageBox(PChar(infoLabel.Caption),PChar('操作失败'), MB_ICONINFORMATION);
                      exit;
                    end;


                  end
                  else
                  begin
                    infoLabel.Caption := '无权限恢复数据库';
                    Application.MessageBox(PChar(infoLabel.Caption),PChar('操作失败'), MB_ICONINFORMATION);
                  end;
                end;

              finally

              end;

          end;
          1:
          begin
            infoLabel.Caption := '选择的数据备份文件不合法，不能恢复！';
            infoLabel.Update;
            Application.MessageBox(PChar(infoLabel.Caption),PChar('操作失败'), MB_ICONINFORMATION);
          end;
          2:
          begin
            infoLabel.Caption := '选择的数据备份文件已经不完整或版本错误，不能恢复！';
            infoLabel.Update;
            Application.MessageBox(PChar(infoLabel.Caption),PChar('操作失败'), MB_ICONINFORMATION);
          end;
        end;
      end
      else
      begin
        infoLabel.Caption := '原备份数据文件不存在，请重新选择文件！';
        infoLabel.Update;
        Application.MessageBox(PChar(infoLabel.Caption),PChar('操作失败'),MB_ICONHAND);
      end;


      try

      //  Compact := CreateOleObject('DAO.DBEngine.36');
      //  strTheTempFileName := GetTmpFile;
      //  if FileExists(strTheTempFileName) then DeleteFile(strTheTempFileName);

      //  Compact.CompactDatabase(SourceFile, strTheTempFileName);

      //  if  DeleteFile(SourceFile) then Compact.CompactDatabase(strTheTempFileName, SourceFile);

      //  DeleteFile(strTheTempFileName);

      //  Compact := unassigned;

      except
      //  infoLabel.Caption := '压缩数据时遇到错误！';
      //  Application.MessageBox(PChar(infoLabel.Caption),PChar('操作失败'), MB_ICONINFORMATION);
      //  exit
      end;

      infoLabel.Caption := '数据恢复成功！';
      Application.MessageBox(PChar(infoLabel.Caption),PChar('操作成功'), MB_ICONINFORMATION);

  end
  else
  begin
    infoLabel.Caption := '无权限恢复数据库';
    Application.MessageBox(PChar(infoLabel.Caption),PChar('操作失败'), MB_ICONINFORMATION);
  end;

end;



procedure Tfrmsystem.RzEdit1Change(Sender: TObject);
var
  szIni : TIniFile;
  Path : string;

begin
  szIni := TIniFile.Create(GetFilePath + m_Config);
  try
    szIni.WriteString(m_Node, 'title' ,Self.RzEdit1.Text);
  finally
    szIni.Free;
  end;
end;

end.
