unit ufrmContrctGoodsList;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2013White, dxSkinOffice2016Colorful,
  dxSkinOffice2016Dark, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, Data.DB, cxDBData, cxTextEdit, cxCheckBox,
  cxDropDownEdit, cxMemo, cxCurrencyEdit, cxGridLevel, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxClasses, cxGridCustomView, cxGrid,
  RzButton, RzPanel, Vcl.ExtCtrls, cxContainer, cxLabel, cxMaskEdit,
  Datasnap.DBClient,UnitADO, Vcl.StdCtrls, cxLookupEdit, cxDBLookupEdit,
  cxDBLookupComboBox,ufrmBaseController, cxButtonEdit;

type
  TfrmContrctGoodsList = class(TfrmBaseController)
    RzToolbar4: TRzToolbar;
    RzSpacer26: TRzSpacer;
    cxLabel1: TcxLabel;
    RzSpacer1: TRzSpacer;
    RzToolButton1: TRzToolButton;
    RzSpacer2: TRzSpacer;
    cxLabel2: TcxLabel;
    Master: TClientDataSet;
    MasterSource: TDataSource;
    RzSpacer3: TRzSpacer;
    RzToolButton2: TRzToolButton;
    tmr: TTimer;
    cxLookupComboBox1: TcxLookupComboBox;
    cxLookupComboBox2: TcxLookupComboBox;
    dsGoodsList: TClientDataSet;
    GoodsListource: TDataSource;
    dsContrctDate: TClientDataSet;
    dsContrctDateSource: TDataSource;
    Grid: TcxGrid;
    tContractView: TcxGridDBTableView;
    tContractViewColumn1: TcxGridDBColumn;
    tContractViewColumn2: TcxGridDBColumn;
    tContractViewColumn3: TcxGridDBColumn;
    tContractViewColumn4: TcxGridDBColumn;
    tContractViewColumn5: TcxGridDBColumn;
    tContractViewColumn6: TcxGridDBColumn;
    tContractViewColumn7: TcxGridDBColumn;
    tContractViewColumn8: TcxGridDBColumn;
    tContractViewColumn9: TcxGridDBColumn;
    tContractViewColumn10: TcxGridDBColumn;
    tContractViewColumn11: TcxGridDBColumn;
    tContractViewColumn12: TcxGridDBColumn;
    tContractViewColumn13: TcxGridDBColumn;
    tContractViewColumn14: TcxGridDBColumn;
    tContractViewColumn15: TcxGridDBColumn;
    tContractViewColumn16: TcxGridDBColumn;
    Lv: TcxGridLevel;
    procedure RzToolButton2Click(Sender: TObject);
    procedure tmrTimer(Sender: TObject);
    procedure tContractViewColumn1GetDisplayText(Sender: TcxCustomGridTableItem;
      ARecord: TcxCustomGridRecord; var AText: string);
    procedure cxLookupComboBox2PropertiesCloseUp(Sender: TObject);
    procedure cxLookupComboBox1PropertiesCloseUp(Sender: TObject);
    procedure tContractViewDblClick(Sender: TObject);
    procedure MasterAfterScroll(DataSet: TDataSet);
  private
    { Private declarations }
    dwADO : TADO;
    dwContract : string;
    function Search():Boolean;
    procedure SetcxGrid(lpGridView : TcxGridDBTableView ; index : Byte ;
lpSuffix:string);
  public
    { Public declarations }
    dwModuleIndex : Integer;
    dwSignName : Variant;
    dwParentHandle : HWND;
    dwCategory : string;
  end;

var
  frmContrctGoodsList: TfrmContrctGoodsList;

implementation

uses
    uDataModule,global;
{$R *.dfm}

procedure TfrmContrctGoodsList.cxLookupComboBox1PropertiesCloseUp(
  Sender: TObject);
begin
  Search;
end;

procedure TfrmContrctGoodsList.cxLookupComboBox2PropertiesCloseUp(
  Sender: TObject);
begin
  Search;
end;

procedure TfrmContrctGoodsList.tContractViewColumn1GetDisplayText(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AText: string);
begin
  AText := IntToStr(ARecord.Index +1 );
end;

procedure TfrmContrctGoodsList.MasterAfterScroll(DataSet: TDataSet);
var
  pMsg : TMakings;

begin
  inherited;
  with DataSet do
  begin
    if (Self.tContractView.ViewData.RowCount <> 0) then
    begin
      pMsg.dwGoodsNo       := '';
      pMsg.dwMakingCaption := tContractViewColumn4.EditValue;;
      pMsg.dwModelIndex    := tContractViewColumn5.EditValue;
      pMsg.dwSpec          := tContractViewColumn6.EditValue;
      pMsg.dwBrand         := tContractViewColumn7.EditValue;

      pMsg.dwManufactor    := tContractViewColumn8.EditValue;
      pMsg.dwNoColour      := tContractViewColumn9.EditValue;
      pMsg.dwTexture       := tContractViewColumn10.EditValue;
      pMsg.dwNoType        := tContractViewColumn11.EditValue;
      pMsg.dwNoNumber      := tContractViewColumn12.EditValue;
      pMsg.dwUnitPrice     := tContractViewColumn15.EditValue;

      SendMessage(dwParentHandle, WM_LisView, 0, LPARAM(@pMsg));  // 发消息
    end;
  end;
end;

procedure TfrmContrctGoodsList.RzToolButton2Click(Sender: TObject);
begin
  Close;
end;

function TfrmContrctGoodsList.Search():Boolean;
var
  s : string;
  szInputDate : string;
  szGoodsName : string;
begin
  if Self.cxLookupComboBox1.EditingValue <> null then szGoodsName := ' AND B.GoodsName="'+ Self.cxLookupComboBox1.Text + '"';

  if Self.cxLookupComboBox2.EditingValue <> null then szInputDate := ' AND A.InputDate=#'+ Self.cxLookupComboBox2.Text + '#';

  // 0  = 入库  1 = 出库
  s := 'SELECT A.NoId,A.SignName,A.ModuleIndex,A.status,A.InputDate,' +
       ' B.GoodsName,B.Model,B.Spec,B.brand,B.UnitPrice,B.Remarks,B.Manufactor,B.NoColour,B.Texture,B.NoType,B.NoNumber,B.Company' +
       ' FROM ' +
       g_Table_Company_Storage_ContractList + ' as A '+
       ' LEFT JOIN ' + g_Table_Company_Storage_ContractDetailed +' AS B ON (A.NoId = B.NoId) ' +
       ' Where A.SignName="' + dwSignName +
       '" AND A.ModuleIndex=' + IntToStr(dwModuleIndex) +
       ' AND A.' + Self.tContractViewColumn2.DataBinding.FieldName + '=yes' + szInputDate + szGoodsName + ' and B.Category="' + dwCategory + '"';
  dwADO.OpenSQL(Self.Master,s);
end;

procedure TfrmContrctGoodsList.SetcxGrid(lpGridView : TcxGridDBTableView ; index : Byte ;
lpSuffix:string);
var
  i : Integer;
begin
  for I := 0 to lpGridView.ColumnCount - 1 do
  begin
    Application.ProcessMessages;
    SetGrid(lpGridView.Columns[i],index, lpSuffix);
  end;
end;

procedure TfrmContrctGoodsList.tmrTimer(Sender: TObject);
var
  s : string;
  szWhereName : string;
begin
  tmr.Enabled := False;
  dwContract := 'Contract';
  dwCategory := '商品主材';
  SetcxGrid(Self.tContractView,0, dwContract);

  s := 'Select * from ' + g_Table_Company_Storage_ContractList +
       ' Where ' + Self.tContractViewColumn2.DataBinding.FieldName +
       '=yes AND ModuleIndex=' + IntToStr(dwModuleIndex) + ' AND SignName="' + dwSignName + '"';
  dwADO.OpenSQL(Self.dsContrctDate,s);

  s := 'SELECT B.GoodsName FROM ' + g_Table_Company_Storage_ContractList + ' as A '+
       ' LEFT JOIN ' + g_Table_Company_Storage_ContractDetailed +' AS B ON (A.NoId = B.NoId) ' +
       ' Where A.SignName="' + dwSignName + '" AND A.ModuleIndex=' + IntToStr(dwModuleIndex) +
       ' AND A.' + Self.tContractViewColumn2.DataBinding.FieldName + '=yes and B.Category="' + dwCategory + '"';;

  dwADO.OpenSQL(Self.dsGoodsList,s);
  Search();
end;

procedure TfrmContrctGoodsList.tContractViewDblClick(Sender: TObject);
begin
  Close;
end;

end.
