object frmContrctGoodsList: TfrmContrctGoodsList
  Left = 0
  Top = 0
  Caption = #21512#21516#21830#21697#21015#34920
  ClientHeight = 480
  ClientWidth = 968
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object RzToolbar4: TRzToolbar
    Left = 0
    Top = 0
    Width = 968
    Height = 29
    AutoStyle = False
    Images = DM.cxImageList1
    BorderInner = fsNone
    BorderOuter = fsNone
    BorderSides = [sdBottom]
    BorderWidth = 0
    GradientColorStyle = gcsCustom
    TabOrder = 0
    VisualStyle = vsGradient
    ToolbarControls = (
      RzSpacer26
      cxLabel1
      cxLookupComboBox1
      RzSpacer1
      RzToolButton1
      RzSpacer2
      cxLabel2
      cxLookupComboBox2
      RzSpacer3
      RzToolButton2)
    object RzSpacer26: TRzSpacer
      Left = 4
      Top = 2
    end
    object RzSpacer1: TRzSpacer
      Left = 236
      Top = 2
    end
    object RzToolButton1: TRzToolButton
      Left = 244
      Top = 2
      ImageIndex = 59
    end
    object RzSpacer2: TRzSpacer
      Left = 269
      Top = 2
    end
    object RzSpacer3: TRzSpacer
      Left = 521
      Top = 2
    end
    object RzToolButton2: TRzToolButton
      Left = 529
      Top = 2
      Width = 65
      ImageIndex = 8
      ShowCaption = True
      UseToolbarButtonSize = False
      UseToolbarShowCaption = False
      Caption = #36864#20986
      OnClick = RzToolButton2Click
    end
    object cxLabel1: TcxLabel
      Left = 12
      Top = 6
      Caption = #20851#38190#23383#65306
      Transparent = True
    end
    object cxLabel2: TcxLabel
      Left = 277
      Top = 6
      Caption = #21512#21516#26085#26399#65306
      Transparent = True
    end
    object cxLookupComboBox1: TcxLookupComboBox
      Left = 64
      Top = 4
      Hint = #21830#21697#21517#31216
      ParentShowHint = False
      Properties.DropDownListStyle = lsEditList
      Properties.KeyFieldNames = 'GoodsName'
      Properties.ListColumns = <
        item
          Caption = #21830#21697#21517#31216
          FieldName = 'GoodsName'
        end>
      Properties.ListSource = GoodsListource
      Properties.OnCloseUp = cxLookupComboBox1PropertiesCloseUp
      ShowHint = True
      TabOrder = 2
      Width = 172
    end
    object cxLookupComboBox2: TcxLookupComboBox
      Left = 341
      Top = 4
      Properties.DropDownListStyle = lsEditList
      Properties.DropDownWidth = 350
      Properties.ImmediateDropDownWhenActivated = True
      Properties.KeyFieldNames = 'InputDate'
      Properties.ListColumns = <
        item
          Caption = #21512#21516#21517#31216
          Width = 250
          FieldName = 'ContractName'
        end
        item
          Caption = #26085#26399
          Width = 100
          FieldName = 'InputDate'
        end>
      Properties.ListFieldIndex = 1
      Properties.ListSource = dsContrctDateSource
      Properties.OnCloseUp = cxLookupComboBox2PropertiesCloseUp
      TabOrder = 3
      Width = 180
    end
  end
  object Grid: TcxGrid
    Left = 0
    Top = 29
    Width = 968
    Height = 451
    Align = alClient
    BevelEdges = [beLeft, beTop, beRight]
    BevelInner = bvSpace
    BevelKind = bkFlat
    BorderStyle = cxcbsNone
    TabOrder = 1
    object tContractView: TcxGridDBTableView
      OnDblClick = tContractViewDblClick
      Navigator.Buttons.CustomButtons = <>
      Navigator.InfoPanel.Visible = True
      Navigator.Visible = True
      DataController.DataSource = MasterSource
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <
        item
          Kind = skSum
        end
        item
          Kind = skSum
        end
        item
          Kind = skCount
        end>
      DataController.Summary.SummaryGroups = <>
      FilterRow.Visible = True
      OptionsBehavior.FocusCellOnTab = True
      OptionsBehavior.FocusFirstCellOnNewRecord = True
      OptionsBehavior.GoToNextCellOnEnter = True
      OptionsBehavior.FocusCellOnCycle = True
      OptionsData.CancelOnExit = False
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsSelection.InvertSelect = False
      OptionsSelection.MultiSelect = True
      OptionsSelection.CellMultiSelect = True
      OptionsView.DataRowHeight = 23
      OptionsView.Footer = True
      OptionsView.GroupByBox = False
      OptionsView.HeaderHeight = 23
      OptionsView.Indicator = True
      OptionsView.IndicatorWidth = 14
      object tContractViewColumn1: TcxGridDBColumn
        Caption = #24207#21495
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taCenter
        Properties.ReadOnly = True
        OnGetDisplayText = tContractViewColumn1GetDisplayText
        Options.Editing = False
        Width = 40
      end
      object tContractViewColumn2: TcxGridDBColumn
        Caption = #29366#24577
        DataBinding.FieldName = 'status'
        PropertiesClassName = 'TcxCheckBoxProperties'
        Visible = False
        Width = 40
      end
      object tContractViewColumn3: TcxGridDBColumn
        Caption = #21333#21495
        DataBinding.FieldName = 'NoId'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.ReadOnly = True
        Visible = False
        Width = 160
      end
      object tContractViewColumn4: TcxGridDBColumn
        Caption = #21830#21697#21517#31216
        DataBinding.FieldName = 'GoodsName'
        PropertiesClassName = 'TcxButtonEditProperties'
        Properties.Buttons = <
          item
            Default = True
            Kind = bkEllipsis
          end>
        Width = 133
      end
      object tContractViewColumn5: TcxGridDBColumn
        Caption = #22411#21495
        DataBinding.FieldName = 'Model'
        Width = 92
      end
      object tContractViewColumn6: TcxGridDBColumn
        Caption = #35268#26684
        DataBinding.FieldName = 'Spec'
        Width = 98
      end
      object tContractViewColumn7: TcxGridDBColumn
        Caption = #21697#29260
        DataBinding.FieldName = 'brand'
        Width = 60
      end
      object tContractViewColumn8: TcxGridDBColumn
        Caption = #21378#23478
        DataBinding.FieldName = 'Manufactor'
        PropertiesClassName = 'TcxTextEditProperties'
        Width = 80
      end
      object tContractViewColumn9: TcxGridDBColumn
        Caption = #39068#33394
        DataBinding.FieldName = 'NoColour'
        PropertiesClassName = 'TcxTextEditProperties'
        Width = 75
      end
      object tContractViewColumn10: TcxGridDBColumn
        Caption = #26448#36136
        DataBinding.FieldName = 'Texture'
        Width = 60
      end
      object tContractViewColumn11: TcxGridDBColumn
        Caption = #24037#33402#20570#27861
        DataBinding.FieldName = 'Process'
        PropertiesClassName = 'TcxMemoProperties'
        Width = 116
      end
      object tContractViewColumn12: TcxGridDBColumn
        Caption = #36135#21495#20998#31867
        DataBinding.FieldName = 'NoType'
        Width = 80
      end
      object tContractViewColumn13: TcxGridDBColumn
        Caption = #36135#21495#32534#21495
        DataBinding.FieldName = 'NoNumber'
        Width = 80
      end
      object tContractViewColumn14: TcxGridDBColumn
        Caption = #21333#20301
        DataBinding.FieldName = 'Company'
        PropertiesClassName = 'TcxComboBoxProperties'
        Properties.ImmediateDropDownWhenActivated = True
        Width = 60
      end
      object tContractViewColumn15: TcxGridDBColumn
        Caption = #21333#20215
        DataBinding.FieldName = 'UnitPrice'
        PropertiesClassName = 'TcxCurrencyEditProperties'
        Width = 70
      end
      object tContractViewColumn16: TcxGridDBColumn
        Caption = #22791#27880
        DataBinding.FieldName = 'Remarks'
        Width = 118
      end
    end
    object Lv: TcxGridLevel
      GridView = tContractView
    end
  end
  object Master: TClientDataSet
    Aggregates = <>
    Params = <>
    AfterScroll = MasterAfterScroll
    Left = 80
    Top = 208
  end
  object MasterSource: TDataSource
    DataSet = Master
    Left = 80
    Top = 264
  end
  object tmr: TTimer
    Interval = 100
    OnTimer = tmrTimer
    Left = 192
    Top = 208
  end
  object dsGoodsList: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 88
    Top = 112
  end
  object GoodsListource: TDataSource
    DataSet = dsGoodsList
    Left = 168
    Top = 112
  end
  object dsContrctDate: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 448
    Top = 104
  end
  object dsContrctDateSource: TDataSource
    DataSet = dsContrctDate
    Left = 448
    Top = 160
  end
end
