unit ufrmMaterialStoragePost;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, ufrmBaseController, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit,
  dxSkinsCore, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray,
  dxSkinOffice2013White, dxSkinOffice2016Colorful, dxSkinOffice2016Dark,
  Vcl.Menus, Vcl.StdCtrls, cxButtons, cxTextEdit, cxCurrencyEdit, RzCmboBx,
  cxGroupBox;

type
  TfrmMaterialStoragePost = class(TfrmBaseController)
    cxGroupBox1: TcxGroupBox;
    RzComboBox1: TRzComboBox;
    Label2: TLabel;
    Label5: TLabel;
    cxCurrencyEdit1: TcxCurrencyEdit;
    Label4: TLabel;
    cxCurrencyEdit2: TcxCurrencyEdit;
    Label14: TLabel;
    cxCurrencyEdit7: TcxCurrencyEdit;
    cxCurrencyEdit3: TcxCurrencyEdit;
    Label3: TLabel;
    cxButton1: TcxButton;
    cxCurrencyEdit6: TcxCurrencyEdit;
    Label13: TLabel;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmMaterialStoragePost: TfrmMaterialStoragePost;

implementation

{$R *.dfm}

end.
