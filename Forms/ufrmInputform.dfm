object frmInputformBase: TfrmInputformBase
  Left = 0
  Top = 0
  ActiveControl = cxTextEdit1
  BorderIcons = [biSystemMenu, biMinimize, biMaximize, biHelp]
  Caption = #24555#25463#21333'-'#22522#31867
  ClientHeight = 619
  ClientWidth = 1050
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 13
  object Bevel3: TBevel
    Left = 0
    Top = 114
    Width = 1050
    Height = 8
    Align = alTop
    Shape = bsSpacer
    Style = bsRaised
    ExplicitTop = 129
    ExplicitWidth = 911
  end
  object RzToolbar4: TRzToolbar
    Left = 0
    Top = 0
    Width = 1050
    Height = 29
    AutoStyle = False
    Images = DM.cxImageList1
    BorderInner = fsNone
    BorderOuter = fsNone
    BorderSides = [sdBottom]
    BorderWidth = 0
    Caption = #38468#20214
    GradientColorStyle = gcsCustom
    ParentShowHint = False
    ShowHint = True
    TabOrder = 1
    VisualStyle = vsGradient
    DesignSize = (
      1050
      29)
    ToolbarControls = (
      RzSpacer26
      RzToolButton1
      RzSpacer1
      RzToolButton7
      RzSpacer6
      RzToolButton6
      RzSpacer2
      RzToolButton2
      RzSpacer4
      RzToolButton23
      RzSpacer31
      RzToolButton8
      RzSpacer7
      RzToolButton24
      RzSpacer32
      RzToolButton4
      RzSpacer33
      cxDBNavigator1
      RzSpacer5
      RzToolButton5
      RzSpacer3
      RzToolButton9
      RzSpacer8
      RzToolButton3)
    object RzSpacer26: TRzSpacer
      Left = 4
      Top = 2
    end
    object RzToolButton23: TRzToolButton
      Left = 285
      Top = 2
      Width = 60
      Hint = #21482#21024#38500#21830#21697#20449#24687
      SelectionColorStop = 16119543
      SelectionFrameColor = clBtnShadow
      ImageIndex = 51
      ShowCaption = True
      UseToolbarButtonSize = False
      UseToolbarShowCaption = False
      Caption = #21024#38500
      ParentShowHint = False
      ShowHint = True
      OnClick = RzToolButton23Click
    end
    object RzSpacer31: TRzSpacer
      Left = 345
      Top = 2
    end
    object RzToolButton24: TRzToolButton
      Left = 441
      Top = 2
      Width = 60
      SelectionColorStop = 16119543
      SelectionFrameColor = clBtnShadow
      DropDownMenu = Print
      ImageIndex = 6
      ShowCaption = True
      UseToolbarButtonSize = False
      UseToolbarShowCaption = False
      Caption = #25171#21360
      OnClick = RzToolButton24Click
    end
    object RzSpacer32: TRzSpacer
      Left = 501
      Top = 2
    end
    object RzSpacer33: TRzSpacer
      Left = 569
      Top = 2
    end
    object RzSpacer1: TRzSpacer
      Left = 72
      Top = 2
    end
    object RzToolButton1: TRzToolButton
      Left = 12
      Top = 2
      Width = 60
      SelectionColorStop = 16119543
      ImageIndex = 37
      ShowCaption = True
      UseToolbarButtonSize = False
      UseToolbarShowCaption = False
      Caption = #26032#22686'&'
      ParentShowHint = False
      ShowHint = True
      OnClick = RzToolButton1Click
    end
    object RzSpacer2: TRzSpacer
      Left = 209
      Top = 2
    end
    object RzToolButton2: TRzToolButton
      Left = 217
      Top = 2
      Width = 60
      Hint = #20445#23384#21040#25968#25454#24211
      SelectionColorStop = 16119543
      ImageIndex = 3
      ShowCaption = True
      UseToolbarButtonSize = False
      UseToolbarShowCaption = False
      Caption = #20445#23384'&'
      ParentShowHint = False
      ShowHint = True
      OnClick = RzToolButton2Click
    end
    object RzSpacer3: TRzSpacer
      Left = 729
      Top = 2
    end
    object RzToolButton3: TRzToolButton
      Left = 841
      Top = 2
      Width = 60
      SelectionColorStop = 16119543
      ImageIndex = 8
      ShowCaption = True
      UseToolbarButtonSize = False
      UseToolbarShowCaption = False
      Caption = #36864#20986'&'
      OnClick = RzToolButton3Click
    end
    object RzToolButton4: TRzToolButton
      Left = 509
      Top = 2
      Width = 60
      SelectionColorStop = 16119543
      ImageIndex = 29
      ShowCaption = True
      UseToolbarButtonSize = False
      UseToolbarShowCaption = False
      Caption = #19978#19968#39029
      OnClick = RzToolButton4Click
    end
    object RzSpacer4: TRzSpacer
      Left = 277
      Top = 2
    end
    object RzToolButton5: TRzToolButton
      Left = 669
      Top = 2
      Width = 60
      SelectionColorStop = 16119543
      ImageIndex = 31
      ShowCaption = True
      UseToolbarButtonSize = False
      UseToolbarShowCaption = False
      Caption = #19979#19968#39029
      OnClick = RzToolButton5Click
    end
    object RzToolButton6: TRzToolButton
      Left = 149
      Top = 2
      Width = 60
      Hint = #20020#26102#20445#23384
      SelectionColorStop = 16119543
      ImageIndex = 17
      ShowCaption = True
      UseToolbarButtonSize = False
      UseToolbarShowCaption = False
      Caption = #30830#23450
      ParentShowHint = False
      ShowHint = True
      OnClick = RzToolButton6Click
    end
    object RzSpacer5: TRzSpacer
      Left = 661
      Top = 2
    end
    object RzSpacer6: TRzSpacer
      Left = 140
      Top = 2
      Width = 9
    end
    object RzToolButton7: TRzToolButton
      Left = 80
      Top = 2
      Width = 60
      SelectionColorStop = 16119543
      ImageIndex = 0
      ShowCaption = True
      UseToolbarButtonSize = False
      UseToolbarShowCaption = False
      Caption = #32534#36753
      OnClick = RzToolButton7Click
    end
    object RzSpacer7: TRzSpacer
      Left = 433
      Top = 2
    end
    object RzToolButton8: TRzToolButton
      Left = 353
      Top = 2
      Width = 80
      SelectionColorStop = 16119543
      ImageIndex = 4
      ShowCaption = True
      UseToolbarButtonSize = False
      UseToolbarShowCaption = False
      Caption = #34920#26684#35774#32622
      OnClick = RzToolButton8Click
    end
    object RzSpacer8: TRzSpacer
      Left = 833
      Top = 2
    end
    object RzToolButton9: TRzToolButton
      Left = 737
      Top = 2
      Width = 96
      ImageIndex = 39
      ShowCaption = True
      UseToolbarShowCaption = False
      Caption = #25171#21360#23567#31080'(F2)'
      OnClick = RzToolButton9Click
    end
    object cxDBNavigator1: TcxDBNavigator
      Left = 577
      Top = 4
      Width = 84
      Height = 20
      Buttons.CustomButtons = <>
      Buttons.First.Visible = False
      Buttons.PriorPage.Visible = False
      Buttons.Prior.Visible = False
      Buttons.Next.Visible = False
      Buttons.NextPage.Visible = False
      Buttons.Last.Visible = False
      Buttons.Insert.Visible = False
      Buttons.Delete.Visible = False
      Buttons.Edit.Visible = False
      Buttons.Post.Visible = False
      Buttons.Cancel.Visible = False
      Buttons.Refresh.Visible = False
      Buttons.SaveBookmark.Visible = False
      Buttons.GotoBookmark.Visible = False
      Buttons.Filter.Visible = False
      DataSource = MasterSource
      InfoPanel.DisplayMask = #31532'[RecordIndex]'#39029#65292#20849'[RecordCount]'#39029
      InfoPanel.Visible = True
      Anchors = [akLeft, akTop, akRight, akBottom]
      TabOrder = 0
    end
  end
  object cxGroupBox1: TcxGroupBox
    Left = 0
    Top = 29
    Align = alTop
    Style.BorderStyle = ebsFlat
    Style.Edges = [bLeft, bTop, bRight, bBottom]
    Style.LookAndFeel.NativeStyle = False
    StyleDisabled.LookAndFeel.NativeStyle = False
    StyleFocused.LookAndFeel.NativeStyle = False
    StyleHot.LookAndFeel.NativeStyle = False
    TabOrder = 0
    Height = 85
    Width = 1050
    object cxLabel7: TcxLabel
      Left = 698
      Top = 22
      Caption = #25910#36135#20179#24211#65306
    end
    object cxLabel1: TcxLabel
      Left = 360
      Top = 22
      Caption = #21457#36135#21333#20301#65306
    end
    object cxLabel3: TcxLabel
      Left = 25
      Top = 22
      Caption = #25910#36135#21333#20301#65306
    end
    object cxLabel8: TcxLabel
      Left = 360
      Top = 49
      Caption = #24320#31080#26085#26399#65306
    end
    object cxLabel5: TcxLabel
      Left = 25
      Top = 49
      Caption = #31080#25454#31867#21035#65306
    end
    object cxLabel9: TcxLabel
      Left = 698
      Top = 49
      Caption = #21333#25454#32534#21495#65306
    end
    object cxDBComboBox1: TcxDBComboBox
      Left = 85
      Top = 49
      DataBinding.DataField = 'BillCategory'
      DataBinding.DataSource = MasterSource
      ParentColor = True
      Properties.OnCloseUp = cxDBComboBox1PropertiesCloseUp
      Properties.OnValidate = cxDBComboBox1PropertiesValidate
      Style.BorderColor = clBlack
      Style.BorderStyle = ebsSingle
      Style.Edges = [bBottom]
      Style.HotTrack = False
      Style.LookAndFeel.NativeStyle = False
      Style.ButtonStyle = btsSimple
      StyleDisabled.LookAndFeel.NativeStyle = False
      StyleFocused.LookAndFeel.NativeStyle = False
      StyleHot.LookAndFeel.NativeStyle = False
      TabOrder = 0
      OnEnter = cxDBComboBox1Enter
      Width = 240
    end
    object BillNumber: TcxDBTextEdit
      Left = 759
      Top = 45
      DataBinding.DataField = 'BillNumber'
      DataBinding.DataSource = MasterSource
      ParentColor = True
      Style.BorderColor = clBlack
      Style.BorderStyle = ebsSingle
      Style.Edges = [bBottom]
      Style.HotTrack = False
      Style.LookAndFeel.NativeStyle = False
      StyleDisabled.LookAndFeel.NativeStyle = False
      StyleFocused.LookAndFeel.NativeStyle = False
      StyleHot.LookAndFeel.NativeStyle = False
      TabOrder = 2
      Width = 240
    end
    object cxDBDateEdit1: TcxDBDateEdit
      Left = 421
      Top = 47
      DataBinding.DataField = 'BillingDate'
      DataBinding.DataSource = MasterSource
      ParentColor = True
      Style.BorderColor = clBlack
      Style.BorderStyle = ebsSingle
      Style.Edges = [bBottom]
      Style.HotTrack = False
      Style.LookAndFeel.NativeStyle = False
      Style.ButtonStyle = btsSimple
      StyleDisabled.LookAndFeel.NativeStyle = False
      StyleFocused.LookAndFeel.NativeStyle = False
      StyleHot.LookAndFeel.NativeStyle = False
      TabOrder = 1
      OnEnter = cxDBDateEdit1Enter
      Width = 240
    end
    object cxDBButtonEdit1: TcxDBButtonEdit
      Left = 759
      Top = 22
      Hint = #25910#36135#20179#24211
      DataBinding.DataSource = MasterSource
      ParentColor = True
      Properties.Buttons = <
        item
          Caption = #36873#25321
          Default = True
          Glyph.Data = {
            36040000424D3604000000000000360000002800000010000000100000000100
            2000000000000004000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000004A220966AC5116F07B380FAB0000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            00004D230A69B85617FFBB5617FFAE4F16F30000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000004D24
            0A69BC5819FFBB5718FFB95318FC401D08570000000000000000000000000000
            000000000000281406368A4315B7B3571AF0B35818F0854012B466300E89BD59
            19FFBC5919FFBA5719FC401E0857000000000000000000000000000000000000
            00005A2D0F78C05F1FFFC06021FFCD8250FFCD824FFFBE5D1FFFBE5A1BFFBD5A
            19FFBB5819FC401E095700000000000000000000000000000000000000003018
            083FC0601FFCCD804CFFF7EAE2FFFFFFFFFFFFFFFFFFF7EAE2FFCA7A46FFBC5A
            1BFF622F0D84000000000000000000000000000000000000000000000000944B
            18C3C36929FFF6E9E0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6E8DFFFC063
            23FF854012B4000000000000000000000000000000000000000000000000B75D
            20F0D18958FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCF84
            54FFAD541AEA000000000000000000000000000000000000000000000000B85E
            20F0D18A59FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD085
            55FFAE541AEA000000000000000000000000000000000000000000000000964D
            1BC3C5692BFFF7EBE2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6E8DFFFC164
            26FF854214B40000000000000000000000000000000000000000000000003019
            093FC36525FFCF844FFFF7EBE2FFFFFFFFFFFFFFFFFFF7EBE2FFCD804CFFBF5E
            1FFF361B08480000000000000000000000000000000000000000000000000000
            00005E31127BC36625FFC46726FFCF8551FFCF8350FFC46523FFC16020FF5D2E
            0F7B000000000000000000000000000000000000000000000000000000000000
            0000000000003019093F874518B1AD591EE4AD591EE4864517B13018083F0000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000}
          Kind = bkGlyph
        end>
      Properties.OnButtonClick = cxDBButtonEdit1PropertiesButtonClick
      Properties.OnValidate = cxDBButtonEdit1PropertiesValidate
      Style.BorderColor = clBlack
      Style.BorderStyle = ebsSingle
      Style.Edges = [bBottom]
      Style.HotTrack = False
      Style.LookAndFeel.NativeStyle = False
      Style.ButtonStyle = btsSimple
      StyleDisabled.LookAndFeel.NativeStyle = False
      StyleFocused.LookAndFeel.NativeStyle = False
      StyleHot.LookAndFeel.NativeStyle = False
      TabOrder = 9
      Width = 240
    end
    object cxLabel2: TcxLabel
      Left = 331
      Top = 26
      Caption = '*'
      Style.TextColor = clRed
      Style.TextStyle = [fsBold]
    end
    object cxLabel4: TcxLabel
      Left = 331
      Top = 49
      Caption = '*'
      Style.TextColor = clRed
      Style.TextStyle = [fsBold]
    end
    object cxLabel6: TcxLabel
      Left = 667
      Top = 49
      Caption = '*'
      Style.TextColor = clRed
      Style.TextStyle = [fsBold]
    end
    object cxLabel10: TcxLabel
      Left = 667
      Top = 26
      Caption = '*'
      Style.TextColor = clRed
      Style.TextStyle = [fsBold]
    end
    object cxLabel11: TcxLabel
      Left = 1005
      Top = 49
      Caption = '*'
      Style.TextColor = clRed
      Style.TextStyle = [fsBold]
    end
    object cxLabel12: TcxLabel
      Left = 1005
      Top = 26
      Caption = '*'
      Style.TextColor = clRed
      Style.TextStyle = [fsBold]
    end
    object cxDBButtonEdit2: TcxDBButtonEdit
      Left = 421
      Top = 20
      Hint = #25910#36135#21333#20301
      DataBinding.DataSource = MasterSource
      ParentColor = True
      Properties.Buttons = <
        item
          Caption = #36873#25321
          Default = True
          Glyph.Data = {
            36040000424D3604000000000000360000002800000010000000100000000100
            2000000000000004000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000004A220966AC5116F07B380FAB0000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            00004D230A69B85617FFBB5617FFAE4F16F30000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000004D24
            0A69BC5819FFBB5718FFB95318FC401D08570000000000000000000000000000
            000000000000281406368A4315B7B3571AF0B35818F0854012B466300E89BD59
            19FFBC5919FFBA5719FC401E0857000000000000000000000000000000000000
            00005A2D0F78C05F1FFFC06021FFCD8250FFCD824FFFBE5D1FFFBE5A1BFFBD5A
            19FFBB5819FC401E095700000000000000000000000000000000000000003018
            083FC0601FFCCD804CFFF7EAE2FFFFFFFFFFFFFFFFFFF7EAE2FFCA7A46FFBC5A
            1BFF622F0D84000000000000000000000000000000000000000000000000944B
            18C3C36929FFF6E9E0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6E8DFFFC063
            23FF854012B4000000000000000000000000000000000000000000000000B75D
            20F0D18958FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCF84
            54FFAD541AEA000000000000000000000000000000000000000000000000B85E
            20F0D18A59FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD085
            55FFAE541AEA000000000000000000000000000000000000000000000000964D
            1BC3C5692BFFF7EBE2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6E8DFFFC164
            26FF854214B40000000000000000000000000000000000000000000000003019
            093FC36525FFCF844FFFF7EBE2FFFFFFFFFFFFFFFFFFF7EBE2FFCD804CFFBF5E
            1FFF361B08480000000000000000000000000000000000000000000000000000
            00005E31127BC36625FFC46726FFCF8551FFCF8350FFC46523FFC16020FF5D2E
            0F7B000000000000000000000000000000000000000000000000000000000000
            0000000000003019093F874518B1AD591EE4AD591EE4864517B13018083F0000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000}
          Kind = bkEllipsis
        end>
      Properties.OnButtonClick = cxDBButtonEdit2PropertiesButtonClick
      Style.BorderColor = clBlack
      Style.BorderStyle = ebsSingle
      Style.Edges = [bBottom]
      Style.HotTrack = False
      Style.LookAndFeel.NativeStyle = False
      Style.ButtonStyle = btsSimple
      StyleDisabled.LookAndFeel.NativeStyle = False
      StyleFocused.LookAndFeel.NativeStyle = False
      StyleHot.LookAndFeel.NativeStyle = False
      TabOrder = 16
      Width = 240
    end
    object DBCompany: TcxDBButtonEdit
      Left = 85
      Top = 22
      DataBinding.DataField = 'CompanyName'
      DataBinding.DataSource = MasterSource
      Properties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.OnButtonClick = DBCompanyPropertiesButtonClick
      Style.BorderColor = clBlack
      Style.BorderStyle = ebsSingle
      Style.Color = clBtnFace
      Style.Edges = [bBottom]
      Style.HotTrack = False
      Style.LookAndFeel.NativeStyle = False
      Style.ButtonStyle = btsSimple
      Style.ButtonTransparency = ebtNone
      StyleDisabled.LookAndFeel.NativeStyle = False
      StyleFocused.LookAndFeel.NativeStyle = False
      StyleHot.LookAndFeel.NativeStyle = False
      TabOrder = 17
      Width = 240
    end
  end
  object cxGroupBox3: TcxGroupBox
    Left = 0
    Top = 488
    Align = alBottom
    Anchors = [akLeft, akTop, akRight]
    Style.BorderStyle = ebsFlat
    Style.Edges = [bLeft, bTop, bRight, bBottom]
    Style.LookAndFeel.NativeStyle = False
    StyleDisabled.LookAndFeel.NativeStyle = False
    StyleFocused.LookAndFeel.NativeStyle = False
    StyleHot.LookAndFeel.NativeStyle = False
    TabOrder = 2
    Height = 108
    Width = 1050
    object cxLabel22: TcxLabel
      Left = 25
      Top = 20
      Caption = #20132#26131#29366#24577#65306
    end
    object cxDBComboBox7: TcxDBComboBox
      Left = 85
      Top = 18
      DataBinding.DataField = 'AccountsStatus'
      DataBinding.DataSource = MasterSource
      Enabled = False
      ParentColor = True
      Properties.ImmediateDropDownWhenActivated = True
      Properties.OnValidate = cxDBComboBox7PropertiesValidate
      Style.BorderColor = clBlack
      Style.BorderStyle = ebsSingle
      Style.Edges = [bBottom]
      Style.HotTrack = False
      Style.LookAndFeel.NativeStyle = False
      Style.ButtonStyle = btsSimple
      StyleDisabled.LookAndFeel.NativeStyle = False
      StyleFocused.LookAndFeel.NativeStyle = False
      StyleHot.LookAndFeel.NativeStyle = False
      TabOrder = 1
      OnEnter = cxDBComboBox1Enter
      Width = 240
    end
    object cxLabel23: TcxLabel
      Left = 372
      Top = 20
      Caption = #32463#25163#20154#65306
    end
    object cxDBTextEdit8: TcxDBTextEdit
      Left = 421
      Top = 18
      DataBinding.DataField = 'Handman'
      DataBinding.DataSource = MasterSource
      ParentColor = True
      Properties.OnValidate = cxDBTextEdit8PropertiesValidate
      Style.BorderColor = clBlack
      Style.BorderStyle = ebsSingle
      Style.Edges = [bBottom]
      Style.HotTrack = False
      Style.LookAndFeel.NativeStyle = False
      StyleDisabled.LookAndFeel.NativeStyle = False
      StyleFocused.LookAndFeel.NativeStyle = False
      StyleHot.LookAndFeel.NativeStyle = False
      TabOrder = 3
      Width = 240
    end
    object cxLabel24: TcxLabel
      Left = 698
      Top = 20
      Caption = #23457#26680#31614#23383#65306
    end
    object cxDBTextEdit9: TcxDBTextEdit
      Left = 759
      Top = 18
      DataBinding.DataField = 'AuditSignature'
      DataBinding.DataSource = MasterSource
      ParentColor = True
      Properties.OnValidate = cxDBTextEdit9PropertiesValidate
      Style.BorderColor = clBlack
      Style.BorderStyle = ebsSingle
      Style.Edges = [bBottom]
      Style.HotTrack = False
      Style.LookAndFeel.NativeStyle = False
      StyleDisabled.LookAndFeel.NativeStyle = False
      StyleFocused.LookAndFeel.NativeStyle = False
      StyleHot.LookAndFeel.NativeStyle = False
      TabOrder = 5
      Width = 240
    end
    object cxLabel25: TcxLabel
      Left = 37
      Top = 47
      Caption = #31614#25910#20154#65306
    end
    object cxDBTextEdit10: TcxDBTextEdit
      Left = 85
      Top = 45
      DataBinding.DataField = 'Recipient'
      DataBinding.DataSource = MasterSource
      ParentColor = True
      Properties.OnValidate = cxDBTextEdit10PropertiesValidate
      Style.BorderColor = clBlack
      Style.BorderStyle = ebsSingle
      Style.Edges = [bBottom]
      Style.HotTrack = False
      Style.LookAndFeel.NativeStyle = False
      StyleDisabled.LookAndFeel.NativeStyle = False
      StyleFocused.LookAndFeel.NativeStyle = False
      StyleHot.LookAndFeel.NativeStyle = False
      TabOrder = 7
      Width = 240
    end
    object cxLabel26: TcxLabel
      Left = 372
      Top = 47
      Caption = #21457#36135#20154#65306
    end
    object cxDBTextEdit11: TcxDBTextEdit
      Left = 421
      Top = 45
      DataBinding.DataField = 'Consignor'
      DataBinding.DataSource = MasterSource
      ParentColor = True
      Properties.OnValidate = cxDBTextEdit11PropertiesValidate
      Style.BorderColor = clBlack
      Style.BorderStyle = ebsSingle
      Style.Edges = [bBottom]
      Style.HotTrack = False
      Style.LookAndFeel.NativeStyle = False
      StyleDisabled.LookAndFeel.NativeStyle = False
      StyleFocused.LookAndFeel.NativeStyle = False
      StyleHot.LookAndFeel.NativeStyle = False
      TabOrder = 9
      Width = 240
    end
    object cxLabel27: TcxLabel
      Left = 698
      Top = 47
      Caption = #31614#25910#26085#26399#65306
    end
    object cxDBDateEdit4: TcxDBDateEdit
      Left = 759
      Top = 45
      DataBinding.DataField = 'DateSigning'
      DataBinding.DataSource = MasterSource
      ParentColor = True
      Properties.OnValidate = cxDBDateEdit4PropertiesValidate
      Style.BorderColor = clBlack
      Style.BorderStyle = ebsSingle
      Style.Edges = [bBottom]
      Style.HotTrack = False
      Style.LookAndFeel.NativeStyle = False
      Style.ButtonStyle = btsSimple
      StyleDisabled.LookAndFeel.NativeStyle = False
      StyleFocused.LookAndFeel.NativeStyle = False
      StyleHot.LookAndFeel.NativeStyle = False
      TabOrder = 11
      OnEnter = cxDBDateEdit1Enter
      Width = 240
    end
    object cxDBComboBox8: TcxDBComboBox
      Left = 85
      Top = 70
      DataBinding.DataField = 'BillStatus'
      DataBinding.DataSource = MasterSource
      ParentColor = True
      Properties.ImmediateDropDownWhenActivated = True
      Properties.Items.Strings = (
        #24050#24320#25910#25454
        #24050#24320#21457#31080
        #26410#24320#21457#31080)
      Properties.OnValidate = cxDBComboBox8PropertiesValidate
      Style.BorderColor = clBlack
      Style.BorderStyle = ebsSingle
      Style.Edges = [bBottom]
      Style.HotTrack = False
      Style.LookAndFeel.NativeStyle = False
      Style.ButtonStyle = btsSimple
      StyleDisabled.LookAndFeel.NativeStyle = False
      StyleFocused.LookAndFeel.NativeStyle = False
      StyleHot.LookAndFeel.NativeStyle = False
      TabOrder = 12
      OnEnter = cxDBComboBox1Enter
      Width = 240
    end
    object cxLabel28: TcxLabel
      Left = 25
      Top = 74
      Caption = #31080#25454#29366#24577#65306
    end
    object cxLabel29: TcxLabel
      Left = 360
      Top = 74
      Caption = #20132#26131#26041#24335#65306
    end
    object cxLabel30: TcxLabel
      Left = 698
      Top = 74
      Caption = #36153#29992#31185#30446#65306
    end
    object cxDBLookupComboBox4: TcxDBLookupComboBox
      Left = 421
      Top = 72
      RepositoryItem = DM.MarketTypeBox
      DataBinding.DataField = 'PayStatus'
      DataBinding.DataSource = MasterSource
      Enabled = False
      ParentColor = True
      Properties.ImmediateDropDownWhenActivated = True
      Properties.ListColumns = <>
      Properties.OnValidate = cxDBLookupComboBox4PropertiesValidate
      Style.BorderColor = clBlack
      Style.BorderStyle = ebsSingle
      Style.Edges = [bBottom]
      Style.HotTrack = False
      Style.LookAndFeel.NativeStyle = False
      Style.ButtonStyle = btsSimple
      Style.ButtonTransparency = ebtNone
      StyleDisabled.LookAndFeel.NativeStyle = False
      StyleFocused.LookAndFeel.NativeStyle = False
      StyleHot.LookAndFeel.NativeStyle = False
      TabOrder = 16
      Width = 240
    end
    object cxDBLookupComboBox5: TcxDBLookupComboBox
      Left = 759
      Top = 72
      RepositoryItem = DM.CostComboBox
      DataBinding.DataField = 'Exacct'
      DataBinding.DataSource = MasterSource
      Enabled = False
      ParentColor = True
      Properties.ImmediateDropDownWhenActivated = True
      Properties.ListColumns = <>
      Properties.OnValidate = cxDBLookupComboBox5PropertiesValidate
      Style.BorderColor = clBlack
      Style.BorderStyle = ebsSingle
      Style.Edges = [bBottom]
      Style.HotTrack = False
      Style.LookAndFeel.NativeStyle = False
      Style.ButtonStyle = btsSimple
      Style.ButtonTransparency = ebtNone
      StyleDisabled.LookAndFeel.NativeStyle = False
      StyleFocused.LookAndFeel.NativeStyle = False
      StyleHot.LookAndFeel.NativeStyle = False
      TabOrder = 17
      Width = 240
    end
    object cxLabel16: TcxLabel
      Left = 331
      Top = 22
      Caption = '*'
      Style.TextColor = clRed
      Style.TextStyle = [fsBold]
    end
  end
  object RzPanel5: TRzPanel
    Left = 0
    Top = 596
    Width = 1050
    Height = 23
    Align = alBottom
    BorderInner = fsFlat
    BorderOuter = fsNone
    BorderSides = [sdLeft, sdRight, sdBottom]
    TabOrder = 3
    object RzPanel6: TRzPanel
      Left = 415
      Top = 0
      Width = 366
      Height = 22
      Align = alRight
      Alignment = taLeftJustify
      BorderOuter = fsNone
      BorderSides = [sdLeft, sdRight]
      ParentColor = True
      TabOrder = 0
      object cxDBTextEdit7: TcxDBTextEdit
        Left = 0
        Top = 0
        Align = alClient
        DataBinding.DataField = 'LargeMonery'
        DataBinding.DataSource = MasterSource
        ParentColor = True
        Properties.Alignment.Horz = taLeftJustify
        Style.BorderColor = clBlack
        Style.BorderStyle = ebsFlat
        Style.Edges = [bBottom]
        Style.HotTrack = False
        Style.LookAndFeel.NativeStyle = False
        StyleDisabled.LookAndFeel.NativeStyle = False
        StyleFocused.LookAndFeel.NativeStyle = False
        StyleHot.LookAndFeel.NativeStyle = False
        TabOrder = 0
        Width = 366
      end
    end
    object RzPanel7: TRzPanel
      Left = 358
      Top = 0
      Width = 57
      Height = 22
      Align = alRight
      BorderInner = fsFlat
      BorderOuter = fsFlat
      BorderSides = [sdLeft, sdRight]
      Caption = #21512#35745#65306
      Color = clWhite
      TabOrder = 1
    end
    object RzPanel8: TRzPanel
      Left = 781
      Top = 0
      Width = 268
      Height = 22
      Align = alRight
      Alignment = taLeftJustify
      BorderInner = fsFlat
      BorderOuter = fsFlat
      BorderSides = [sdLeft, sdRight]
      Caption = #12288#65509':'
      ParentColor = True
      TabOrder = 2
      object cxDBCurrencyEdit1: TcxDBCurrencyEdit
        Left = 2
        Top = 0
        Align = alClient
        DataBinding.DataField = 'SumMonery'
        DataBinding.DataSource = MasterSource
        ParentColor = True
        Properties.Alignment.Horz = taLeftJustify
        Style.BorderStyle = ebsNone
        TabOrder = 0
        Width = 264
      end
    end
    object RzPanel1: TRzPanel
      Left = 58
      Top = 0
      Width = 300
      Height = 22
      Align = alClient
      BorderInner = fsFlat
      BorderOuter = fsNone
      BorderSides = []
      Color = clWhite
      TabOrder = 3
      object cxTextEdit1: TcxTextEdit
        Left = 0
        Top = 0
        Align = alClient
        ParentFont = False
        Style.BorderStyle = ebsNone
        Style.Color = clBtnFace
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -13
        Style.Font.Name = 'Tahoma'
        Style.Font.Style = []
        Style.HotTrack = False
        Style.LookAndFeel.NativeStyle = False
        Style.TextColor = clRed
        Style.TextStyle = [fsBold]
        Style.IsFontAssigned = True
        StyleDisabled.LookAndFeel.NativeStyle = False
        StyleFocused.LookAndFeel.NativeStyle = False
        StyleHot.LookAndFeel.NativeStyle = False
        TabOrder = 0
        TextHint = #26465#30721#26538#36755#20837#25110#25163#24037#36755#20837#26465#24418#30721
        OnKeyDown = cxTextEdit1KeyDown
        Width = 300
      end
    end
    object RzPanel2: TRzPanel
      Left = 1
      Top = 0
      Width = 57
      Height = 22
      Align = alLeft
      BorderInner = fsFlat
      BorderOuter = fsFlat
      BorderSides = [sdLeft, sdRight]
      Caption = #26465#24418#30721#65306
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 4
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 122
    Width = 1050
    Height = 366
    Align = alClient
    BevelOuter = bvNone
    Caption = 'Panel2'
    TabOrder = 4
    object cxGroupBox2: TcxGroupBox
      Left = 0
      Top = 0
      Align = alClient
      Caption = #21830#21697#20449#24687
      Style.BorderStyle = ebsFlat
      Style.Edges = [bLeft, bTop, bRight, bBottom]
      Style.LookAndFeel.NativeStyle = False
      StyleDisabled.LookAndFeel.NativeStyle = False
      StyleFocused.LookAndFeel.NativeStyle = False
      StyleHot.LookAndFeel.NativeStyle = False
      TabOrder = 0
      Height = 366
      Width = 1050
      object Grid: TcxGrid
        AlignWithMargins = True
        Left = 5
        Top = 21
        Width = 1040
        Height = 340
        Align = alClient
        TabOrder = 0
        object tView: TcxGridDBTableView
          PopupMenu = pm
          OnDblClick = tViewDblClick
          Navigator.Buttons.CustomButtons = <>
          Navigator.Buttons.First.Visible = True
          Navigator.Buttons.PriorPage.Visible = True
          Navigator.Buttons.Prior.Visible = True
          Navigator.Buttons.Next.Visible = True
          Navigator.Buttons.Insert.Visible = False
          Navigator.Buttons.Delete.Visible = False
          Navigator.Buttons.Edit.Visible = False
          Navigator.Buttons.Post.Visible = False
          Navigator.Buttons.Cancel.Visible = False
          Navigator.Buttons.Refresh.Visible = False
          Navigator.Buttons.SaveBookmark.Visible = False
          Navigator.Buttons.GotoBookmark.Visible = False
          Navigator.Buttons.Filter.Visible = False
          Navigator.InfoPanel.Visible = True
          Navigator.Visible = True
          OnEditing = tViewEditing
          OnEditKeyDown = tViewEditKeyDown
          DataController.DataSource = DetailedSource
          DataController.KeyFieldNames = 'OddNumbers'
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <
            item
              Format = #165',0.00;'#165'-,0.00'
              Kind = skSum
              OnGetText = tViewTcxGridDBDataControllerTcxDataSummaryFooterSummaryItems0GetText
              Column = tViewCol10
            end>
          DataController.Summary.SummaryGroups = <>
          OptionsBehavior.FocusCellOnTab = True
          OptionsBehavior.FocusFirstCellOnNewRecord = True
          OptionsBehavior.GoToNextCellOnEnter = True
          OptionsBehavior.FocusCellOnCycle = True
          OptionsData.Deleting = False
          OptionsData.DeletingConfirmation = False
          OptionsSelection.InvertSelect = False
          OptionsSelection.MultiSelect = True
          OptionsSelection.CellMultiSelect = True
          OptionsView.DataRowHeight = 23
          OptionsView.Footer = True
          OptionsView.GroupByBox = False
          OptionsView.HeaderHeight = 23
          OptionsView.Indicator = True
          OptionsView.IndicatorWidth = 14
          OnColumnSizeChanged = tViewColumnSizeChanged
          object tViewCol1: TcxGridDBColumn
            Caption = #24207#21495
            PropertiesClassName = 'TcxTextEditProperties'
            Properties.Alignment.Horz = taCenter
            Properties.ReadOnly = True
            OnGetDisplayText = tViewCol1GetDisplayText
            Width = 40
          end
          object tViewCol2: TcxGridDBColumn
            Caption = #29366#24577
            DataBinding.FieldName = 'status'
            PropertiesClassName = 'TcxCheckBoxProperties'
            Width = 40
          end
          object tViewColumn8: TcxGridDBColumn
            Caption = #32534#21495
            DataBinding.FieldName = 'OddNumbers'
            Width = 95
          end
          object tViewCol3: TcxGridDBColumn
            Caption = #21333#21495
            DataBinding.FieldName = 'BillNumber'
            PropertiesClassName = 'TcxTextEditProperties'
            Properties.ReadOnly = True
            Width = 160
          end
          object tViewCol4: TcxGridDBColumn
            Caption = #21830#21697#21517#31216
            DataBinding.FieldName = 'GoodsName'
            PropertiesClassName = 'TcxButtonEditProperties'
            Properties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.OnButtonClick = tViewCol4PropertiesButtonClick
            Width = 155
          end
          object tViewCol5: TcxGridDBColumn
            Caption = #22411#21495
            DataBinding.FieldName = 'Model'
            Width = 60
          end
          object tViewCol6: TcxGridDBColumn
            Caption = #35268#26684
            DataBinding.FieldName = 'Spec'
            PropertiesClassName = 'TcxButtonEditProperties'
            Properties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.OnButtonClick = tViewCol6PropertiesButtonClick
            Width = 60
          end
          object tViewColumn3: TcxGridDBColumn
            Caption = #21378#23478
            DataBinding.FieldName = 'Manufactor'
            Visible = False
            Width = 60
          end
          object tViewColumn4: TcxGridDBColumn
            Caption = #21697#29260
            DataBinding.FieldName = 'brand'
            Visible = False
            Width = 60
          end
          object tViewColumn6: TcxGridDBColumn
            Caption = #39068#33394
            DataBinding.FieldName = 'NoColour'
            Visible = False
            Width = 60
          end
          object tViewColumn7: TcxGridDBColumn
            Caption = #26448#36136
            DataBinding.FieldName = 'Texture'
            Visible = False
            Width = 60
          end
          object tViewColumn1: TcxGridDBColumn
            Caption = #36135#21495#20998#31867
            DataBinding.FieldName = 'NoType'
            Visible = False
            Width = 60
          end
          object tViewColumn2: TcxGridDBColumn
            Caption = #36135#21495#32534#21495
            DataBinding.FieldName = 'NoNumber'
            Visible = False
            Width = 60
          end
          object tViewColumn5: TcxGridDBColumn
            Caption = #20998#39033#33539#22260
            DataBinding.FieldName = 'ItemRange'
            Visible = False
            Width = 60
          end
          object tViewColumn12: TcxGridDBColumn
            Caption = #35745#31639#20844#24335
            DataBinding.FieldName = 'CalculatingFormula'
            Width = 100
          end
          object tViewColumn13: TcxGridDBColumn
            Caption = #21464#37327#25968
            DataBinding.FieldName = 'VariableCount'
            PropertiesClassName = 'TcxSpinEditProperties'
            Properties.DisplayFormat = '0.#######;-0.#######'
            Properties.EditFormat = '0.#######;-0.#######'
            Properties.ValueType = vtFloat
          end
          object tViewCol7: TcxGridDBColumn
            Caption = #25968#37327
            DataBinding.FieldName = 'CountTotal'
            PropertiesClassName = 'TcxButtonEditProperties'
            Properties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.OnButtonClick = tViewCol7PropertiesButtonClick
            Properties.OnValidate = tViewCol7PropertiesValidate
            Width = 70
          end
          object tViewColumn10: TcxGridDBColumn
            Caption = #37325#37327
            DataBinding.FieldName = 'weight'
            PropertiesClassName = 'TcxSpinEditProperties'
            Properties.DisplayFormat = '0.#######;-0.#######'
            Properties.EditFormat = '0.#######;-0.#######'
            Properties.ValueType = vtFloat
            Properties.OnValidate = tViewColumn10PropertiesValidate
          end
          object tViewCol9: TcxGridDBColumn
            Caption = #21333#20301
            DataBinding.FieldName = 'Company'
            PropertiesClassName = 'TcxComboBoxProperties'
            Properties.ImmediateDropDownWhenActivated = True
            RepositoryItem = DM.UnitList
            Width = 60
          end
          object tViewColumn11: TcxGridDBColumn
            Caption = #21830#21697#21333#20215
            DataBinding.FieldName = 'GoodsPrice'
            PropertiesClassName = 'TcxCurrencyEditProperties'
            Width = 70
          end
          object tViewColumn9: TcxGridDBColumn
            Caption = #32452#21512#21333#20215
            DataBinding.FieldName = 'GroupPrice'
            PropertiesClassName = 'TcxCurrencyEditProperties'
            Width = 70
          end
          object tViewCol8: TcxGridDBColumn
            Caption = #21333#20215
            DataBinding.FieldName = 'UnitPrice'
            PropertiesClassName = 'TcxSpinEditProperties'
            Properties.AssignedValues.EditFormat = True
            Properties.DisplayFormat = #165',0.00;'#165'-,0.00'
            Properties.ValueType = vtFloat
            Properties.OnValidate = tViewCol8PropertiesValidate
            Width = 70
          end
          object tViewCol15: TcxGridDBColumn
            Caption = #25240#25187#29575
            DataBinding.FieldName = 'DisCount'
            PropertiesClassName = 'TcxCurrencyEditProperties'
            Properties.DisplayFormat = '0.##;0.##'
            Properties.OnValidate = tViewCol15PropertiesValidate
            Width = 60
          end
          object tViewCol16: TcxGridDBColumn
            Caption = #25240#25187#21518#21333#20215
            DataBinding.FieldName = 'DisPrice'
            PropertiesClassName = 'TcxCurrencyEditProperties'
            Properties.OnValidate = tViewCol16PropertiesValidate
            Width = 75
          end
          object tViewCol10: TcxGridDBColumn
            Caption = #37329#39069
            DataBinding.FieldName = 'SumMonery'
            PropertiesClassName = 'TcxCurrencyEditProperties'
            Properties.AssignedValues.EditFormat = True
            Properties.DecimalPlaces = 0
            Properties.DisplayFormat = #165',0.00;'#165'-,0.00'
            Properties.OnValidate = tViewCol10PropertiesValidate
            Width = 120
          end
          object tViewColumn14: TcxGridDBColumn
            Caption = #22823#20889
            DataBinding.FieldName = 'LargeMonery'
            Visible = False
            Width = 117
          end
          object tViewCol11: TcxGridDBColumn
            Caption = #22791#27880
            DataBinding.FieldName = 'Remarks'
            Width = 118
          end
          object tViewCol12: TcxGridDBColumn
            Caption = #22791#27880'1'
            DataBinding.FieldName = 'Remarks1'
            Width = 126
          end
          object tViewCol13: TcxGridDBColumn
            Caption = #22791#27880'2'
            DataBinding.FieldName = 'Remarks2'
            Width = 100
          end
          object tViewCol14: TcxGridDBColumn
            Caption = #22791#27880'3'
            DataBinding.FieldName = 'Remarks3'
            Width = 92
          end
          object tViewColumn15: TcxGridDBColumn
            Caption = #26465#24418#30721
            DataBinding.FieldName = 'BarCode'
          end
        end
        object tRuning: TcxGridDBTableView
          Navigator.Buttons.CustomButtons = <>
          DataController.DataSource = DataSource1
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsView.DataRowHeight = 23
          OptionsView.GroupByBox = False
          OptionsView.HeaderHeight = 23
          OptionsView.Indicator = True
          object tRuningCol9: TcxGridDBColumn
            Caption = #29366#24577
            DataBinding.FieldName = 'status'
            Width = 42
          end
          object tRuningCol1: TcxGridDBColumn
            Caption = #32534#21495
            DataBinding.FieldName = 'numbers'
            Visible = False
            Width = 133
          end
          object tRuningCol12: TcxGridDBColumn
            Caption = #26085#26399
            DataBinding.FieldName = 'cxDate'
            SortIndex = 0
            SortOrder = soAscending
            Width = 48
          end
          object tRuningCol2: TcxGridDBColumn
            Caption = #25910#27454#21333#20301
            DataBinding.FieldName = 'Receivables'
            Width = 131
          end
          object tRuningCol3: TcxGridDBColumn
            Caption = #20184#27454#21333#20301
            DataBinding.FieldName = 'PaymentType'
            Width = 131
          end
          object tRuningCol4: TcxGridDBColumn
            Caption = #39033#30446'/'#21517#31216
            DataBinding.FieldName = 'ProjectName'
            Width = 135
          end
          object tRuningCol11: TcxGridDBColumn
            Caption = #21046#34920#31867#21035
            DataBinding.FieldName = 'TabType'
            Width = 65
          end
          object tRuningCol5: TcxGridDBColumn
            Caption = #20132#26131#26041#24335
            DataBinding.FieldName = 'TradeType'
            Width = 65
          end
          object tRuningCol6: TcxGridDBColumn
            Caption = #36153#29992#31185#30446
            DataBinding.FieldName = 'CategoryName'
            Width = 65
          end
          object tRuningCol7: TcxGridDBColumn
            Caption = #37329#39069
            DataBinding.FieldName = 'SumMoney'
            PropertiesClassName = 'TcxCurrencyEditProperties'
            Width = 113
          end
          object tRuningCol8: TcxGridDBColumn
            Caption = #22823#20889
            DataBinding.FieldName = 'CapitalMoney'
            Width = 209
          end
          object tRuningCol10: TcxGridDBColumn
            Caption = #20998#31867
            DataBinding.FieldName = 'fromType'
            Visible = False
          end
        end
        object tGroup: TcxGridDBTableView
          Navigator.Buttons.CustomButtons = <>
          Navigator.Buttons.First.Visible = True
          Navigator.Buttons.PriorPage.Visible = True
          Navigator.Buttons.Prior.Visible = True
          Navigator.Buttons.Next.Visible = True
          Navigator.Buttons.NextPage.Visible = True
          Navigator.Buttons.Last.Visible = True
          Navigator.Buttons.Insert.Visible = True
          Navigator.Buttons.Append.Visible = False
          Navigator.Buttons.Delete.Visible = True
          Navigator.Buttons.Edit.Visible = True
          Navigator.Buttons.Post.Visible = True
          Navigator.Buttons.Cancel.Visible = True
          Navigator.Buttons.Refresh.Visible = True
          Navigator.Buttons.SaveBookmark.Visible = True
          Navigator.Buttons.GotoBookmark.Visible = True
          Navigator.Buttons.Filter.Visible = True
          OnEditing = tGroupEditing
          DataController.DataSource = DataGroupUnitPrice
          DataController.DetailKeyFieldNames = 'ParentId'
          DataController.KeyFieldNames = 'ParentId'
          DataController.MasterKeyFieldNames = 'OddNumbers'
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsBehavior.DragDropText = True
          OptionsBehavior.DragFocusing = dfDragDrop
          OptionsBehavior.FocusCellOnTab = True
          OptionsBehavior.FocusFirstCellOnNewRecord = True
          OptionsBehavior.GoToNextCellOnEnter = True
          OptionsBehavior.NavigatorHints = True
          OptionsBehavior.FocusCellOnCycle = True
          OptionsBehavior.PullFocusing = True
          OptionsCustomize.ColumnMoving = False
          OptionsCustomize.ColumnsQuickCustomization = True
          OptionsCustomize.DataRowSizing = True
          OptionsData.Deleting = False
          OptionsData.DeletingConfirmation = False
          OptionsSelection.InvertSelect = False
          OptionsSelection.MultiSelect = True
          OptionsSelection.CellMultiSelect = True
          OptionsView.CellAutoHeight = True
          OptionsView.DataRowHeight = 23
          OptionsView.GroupByBox = False
          OptionsView.GroupRowStyle = grsOffice11
          OptionsView.HeaderHeight = 23
          OptionsView.Indicator = True
          OptionsView.IndicatorWidth = 16
          object tGroupColumn1: TcxGridDBColumn
            Caption = #32452#21512#31867#22411
            DataBinding.FieldName = 'CaptionType'
            Width = 150
          end
          object tGroupColumn2: TcxGridDBColumn
            Caption = #32452#21512#35268#26684
            DataBinding.FieldName = 'spec'
            Width = 70
          end
          object tGroupColumn3: TcxGridDBColumn
            Caption = #21333#20215
            DataBinding.FieldName = 'UnitPrice'
            PropertiesClassName = 'TcxCurrencyEditProperties'
            Width = 60
          end
          object tGroupColumn4: TcxGridDBColumn
            Caption = #32534#21495
            DataBinding.FieldName = 'NoId'
            Visible = False
            Width = 80
          end
        end
        object Lv: TcxGridLevel
          Caption = #21830#21697#20449#24687
          GridView = tView
          object Lv1: TcxGridLevel
            GridView = tGroup
          end
        end
        object Lv2: TcxGridLevel
          Caption = #29616#37329#27969#27700
          GridView = tRuning
        end
      end
      object Navigator: TDBNavigator
        Left = 781
        Top = 232
        Width = 240
        Height = 25
        DataSource = MasterSource
        TabOrder = 1
        Visible = False
      end
      object cxDBCheckBox1: TcxDBCheckBox
        Left = 888
        Top = 69
        Caption = #29366#24577
        DataBinding.DataField = 'status'
        DataBinding.DataSource = MasterSource
        TabOrder = 2
        Visible = False
      end
      object cxDBTextEdit1: TcxDBTextEdit
        Left = 832
        Top = 96
        DataBinding.DataField = 'TreeId'
        DataBinding.DataSource = MasterSource
        TabOrder = 3
        Visible = False
        Width = 199
      end
      object cxDBTextEdit2: TcxDBTextEdit
        Left = 832
        Top = 123
        DataBinding.DataField = 'Code'
        DataBinding.DataSource = MasterSource
        TabOrder = 4
        Visible = False
        Width = 199
      end
    end
  end
  object Timer1: TTimer
    Interval = 100
    OnTimer = Timer1Timer
    Left = 32
    Top = 416
  end
  object pm: TPopupMenu
    Images = DM.cxImageList1
    Left = 80
    Top = 416
    object N16: TMenuItem
      Caption = #26465#30721#24405#20837
      OnClick = N16Click
    end
    object N14: TMenuItem
      Caption = #28155#21152#26126#32454
      OnClick = N14Click
    end
    object N13: TMenuItem
      Caption = '-'
    end
    object N1: TMenuItem
      Caption = #26032#12288#22686
      ImageIndex = 37
      OnClick = N1Click
    end
    object N15: TMenuItem
      Caption = #32534#12288#36753
      ImageIndex = 0
      OnClick = N15Click
    end
    object N2: TMenuItem
      Caption = #30830#12288#23450
      ImageIndex = 17
      OnClick = N2Click
    end
    object N3: TMenuItem
      Caption = #20445#12288#23384
      ImageIndex = 3
      OnClick = N3Click
    end
    object N4: TMenuItem
      Caption = #21024#12288#38500
      ImageIndex = 51
      OnClick = N4Click
    end
    object N10: TMenuItem
      Caption = '-'
    end
    object N11: TMenuItem
      Caption = #38468#12288#20214
      ImageIndex = 27
      OnClick = N11Click
    end
    object N5: TMenuItem
      Caption = '-'
    end
    object N6: TMenuItem
      Caption = #19978#19968#39029
      ImageIndex = 29
      OnClick = N6Click
    end
    object N7: TMenuItem
      Caption = #19979#19968#39029
      ImageIndex = 31
      OnClick = N7Click
    end
    object N8: TMenuItem
      Caption = '-'
    end
    object N9: TMenuItem
      Caption = #36864#12288#20986
      ImageIndex = 8
      OnClick = RzToolButton3Click
    end
  end
  object Master: TClientDataSet
    Aggregates = <>
    Params = <>
    AfterInsert = MasterAfterInsert
    Left = 112
    Top = 176
  end
  object MasterSource: TDataSource
    DataSet = Master
    Left = 112
    Top = 240
  end
  object DetailedSource: TDataSource
    DataSet = Detailed
    Left = 32
    Top = 360
  end
  object Detailed: TClientDataSet
    Aggregates = <>
    FieldDefs = <>
    IndexDefs = <>
    IndexFieldNames = 'BillNumber'
    MasterFields = 'BillNumber'
    MasterSource = MasterSource
    PacketRecords = 0
    Params = <>
    StoreDefs = True
    AfterInsert = DetailedAfterInsert
    OnCalcFields = DetailedCalcFields
    Left = 32
    Top = 296
  end
  object CompanySource: TDataSource
    DataSet = Company
    Left = 112
    Top = 344
  end
  object Company: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 112
    Top = 296
  end
  object Print: TPopupMenu
    Images = DM.cxImageList1
    Left = 176
    Top = 176
    object N12: TMenuItem
      Caption = #25171#21360#25253#34920
      ImageIndex = 6
    end
  end
  object dsRuning: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 128
    Top = 416
  end
  object DataSource1: TDataSource
    DataSet = dsRuning
    Left = 192
    Top = 418
  end
  object dsGroupUnitPrice: TClientDataSet
    Aggregates = <>
    Filtered = True
    IndexFieldNames = 'ParentId'
    MasterFields = 'OddNumbers'
    MasterSource = DetailedSource
    PacketRecords = 0
    Params = <>
    AfterInsert = dsGroupUnitPriceAfterInsert
    Left = 400
    Top = 288
  end
  object DataGroupUnitPrice: TDataSource
    DataSet = dsGroupUnitPrice
    Left = 400
    Top = 344
  end
end
