unit ufrmTurnover;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, dxSkinsCore,
  dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray, dxSkinOffice2013White,
  dxSkinOffice2016Colorful, dxSkinOffice2016Dark, Vcl.ComCtrls, dxCore,
  cxDateUtils, Vcl.Menus, cxSpinEdit, cxMemo, Vcl.StdCtrls, cxButtons,
  cxDropDownEdit, cxCalendar, cxTextEdit, cxMaskEdit, cxCurrencyEdit, cxLabel,
  cxGroupBox;

type
  TfrmTurnover = class(TForm)
    cxGroupBox1: TcxGroupBox;
    cxLabel1: TcxLabel;
    cxCurrencyEdit1: TcxCurrencyEdit;
    cxLabel2: TcxLabel;
    cxComboBox1: TcxComboBox;
    cxLabel3: TcxLabel;
    cxLabel4: TcxLabel;
    cxLabel5: TcxLabel;
    cxTextEdit1: TcxTextEdit;
    cxTextEdit2: TcxTextEdit;
    cxLabel6: TcxLabel;
    cxLabel7: TcxLabel;
    cxDateEdit1: TcxDateEdit;
    cxDateEdit2: TcxDateEdit;
    cxLabel8: TcxLabel;
    cxButton1: TcxButton;
    cxTextEdit3: TcxTextEdit;
    cxLabel9: TcxLabel;
    cxMemo1: TcxMemo;
    cxButton2: TcxButton;
    cxButton3: TcxButton;
    cxCurrencyEdit2: TcxCurrencyEdit;
    procedure cxButton1Click(Sender: TObject);
    procedure cxButton3Click(Sender: TObject);
    procedure cxCurrencyEdit1PropertiesChange(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    g_PostCode    : string;
    g_ProjectDir  : string;
    g_PostNumbers : string;
    g_TableName   : string;
    g_IsModify    : Boolean;
  end;

var
  frmTurnover: TfrmTurnover;

implementation

uses
  global,uDataModule;

{$R *.dfm}

procedure TfrmTurnover.cxButton1Click(Sender: TObject);
begin
  CreateOpenDir(Handle,Concat(g_ProjectDir,'\' + g_PostNumbers),True);
end;

procedure TfrmTurnover.cxButton2Click(Sender: TObject);
var
  szCaption : string;
  szContent : string;
  szPrice   : Currency;
  szValue   : Currency;
  szRemarks : string;
  szUnit    : string;
  s : string;
begin
  szCaption := Self.cxTextEdit1.Text;
  if Length(szCaption ) =  0 then
  begin
    Application.MessageBox('名称不可为空!!',m_title,MB_OK + MB_ICONQUESTION );
  end else
  begin
    szContent := Self.cxTextEdit2.Text;
    if Length(szContent) = 0 then
    begin
      Application.MessageBox('内容不可为空',m_title,MB_OK + MB_ICONQUESTION );
    end else
    begin
      szPrice := Self.cxCurrencyEdit1.Value;
      szValue := Self.cxCurrencyEdit2.Value;
      szUnit  := Self.cxComboBox1.Text;
      szRemarks := Self.cxMemo1.Text;  //备注
      if g_IsModify then
      begin
        s := Format('Update '+ g_TableName +' set caption="%s",'  +
                                             'content="%s",'  +
                                             'pay_time="%s",' +
                                             'End_time="%s",' +
                                             'Price=''%m'','  +
                                             'num=''%f'','    +
                                             'Units="%s",'    +
                                             'remarks="%s"'  +
                                              ' where numbers="%s"',[
                                                           szCaption,
                                                           szContent,
                                                           FormatDateTime('yyyy-MM-dd',Self.cxDateEdit1.Date),
                                                           FormatDateTime('yyyy-MM-dd',Self.cxDateEdit2.Date),
                                                           szPrice,
                                                           szValue,
                                                           szUnit,
                                                           szRemarks,
                                                           g_PostNumbers
                                                           ]);

        OutputLog(s);
        with DM.Qry do
        begin
          Close;
          SQL.Clear;
          SQL.Text := s;
          if ExecSQL <> 0 then
          begin
            m_IsModify := True;
            Application.MessageBox('修改成功',m_title,MB_OK + MB_ICONQUESTION );
          end else
          begin
            Application.MessageBox('修改失败',m_title,MB_OK + MB_ICONQUESTION );
          end;
        end;

      end else
      begin
          if DM.AddPayInfo(g_TableName,
                                g_PostCode,
                                '',
                                szCaption,
                                szContent,
                                szUnit,
                                szRemarks,
                                Self.cxDateEdit1.Date,
                                Self.cxDateEdit2.Date,
                                szPrice,
                                1,
                                g_PostNumbers,
                                szValue,
                                g_pacttype) <> 0 then
          begin
            g_PostNumbers := DM.getDataMaxDate(g_TableName);
            m_IsModify := True;
            Application.MessageBox('添加成功',m_title,MB_OK + MB_ICONQUESTION );
          end
          else
          begin
            Application.MessageBox('添加失败',m_title,MB_OK + MB_ICONQUESTION );
          end;
      end;


    end;

  end;
end;

procedure TfrmTurnover.cxButton3Click(Sender: TObject);
begin
  Close;
end;

procedure TfrmTurnover.cxCurrencyEdit1PropertiesChange(Sender: TObject);
begin
  Self.cxTextEdit3.Text := MoneyConvert(Self.cxCurrencyEdit1.Value);
end;

procedure TfrmTurnover.FormCreate(Sender: TObject);
var
  s : string;
begin

  with DM.Qry do
  begin
    Close;
    SQL.Clear;
    SQL.Text := 'Select * from unit where chType=0';
    Open;

    if 0 <> RecordCount then
     begin
       Self.cxComboBox1.Clear;
       while not Eof do
       begin
         s := FieldByName('name').AsString;
         Self.cxComboBox1.Properties.Items.Add( s );
         Next;
       end;
       Self.cxComboBox1.ItemIndex := 0;
     end;

  end;

end;

end.
