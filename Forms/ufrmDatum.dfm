object frmDatum: TfrmDatum
  Left = 0
  Top = 0
  ActiveControl = Tv1
  Caption = #21512#21516#31649#29702
  ClientHeight = 502
  ClientWidth = 975
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter
    Left = 185
    Top = 28
    Width = 10
    Height = 454
    Color = 16250613
    ParentColor = False
    ResizeStyle = rsUpdate
    ExplicitLeft = 178
    ExplicitTop = 0
    ExplicitHeight = 335
  end
  object RzPanel2: TRzPanel
    Left = 195
    Top = 28
    Width = 780
    Height = 454
    Align = alClient
    BorderInner = fsFlat
    BorderOuter = fsNone
    BorderSides = [sdLeft]
    TabOrder = 0
    object RzToolbar2: TRzToolbar
      Left = 1
      Top = 0
      Width = 779
      AutoStyle = False
      Images = DM.cxImageList1
      RowHeight = 28
      BorderInner = fsNone
      BorderOuter = fsFlat
      BorderSides = [sdTop]
      BorderWidth = 0
      GradientColorStyle = gcsCustom
      TabOrder = 0
      VisualStyle = vsGradient
      ToolbarControls = (
        RzSpacer5
        cxLabel2
        cxLookupComboBox2
        RzSpacer7
        RzToolButton7
        RzSpacer10
        RzToolButton2
        RzSpacer15
        RzToolButton8
        RzSpacer9
        RzToolButton9
        RzSpacer8
        RzToolButton6
        RzSpacer11
        RzToolButton15)
      object RzSpacer5: TRzSpacer
        Left = 4
        Top = 4
      end
      object RzToolButton7: TRzToolButton
        Left = 281
        Top = 4
        Width = 24
        ImageIndex = 37
        ShowCaption = False
        UseToolbarButtonSize = False
        UseToolbarShowCaption = False
        OnClick = RzToolButton7Click
      end
      object RzToolButton8: TRzToolButton
        Left = 346
        Top = 4
        Width = 24
        ImageIndex = 3
        ShowCaption = False
        UseToolbarButtonSize = False
        UseToolbarShowCaption = False
        Caption = #20445#23384
        OnClick = RzToolButton7Click
      end
      object RzSpacer9: TRzSpacer
        Left = 370
        Top = 4
      end
      object RzToolButton9: TRzToolButton
        Left = 378
        Top = 4
        Width = 24
        ImageIndex = 2
        ShowCaption = False
        UseToolbarButtonSize = False
        UseToolbarShowCaption = False
        Caption = #21024#38500
        OnClick = RzToolButton7Click
      end
      object RzSpacer10: TRzSpacer
        Left = 305
        Top = 4
      end
      object RzSpacer15: TRzSpacer
        Left = 338
        Top = 4
      end
      object RzToolButton15: TRzToolButton
        Left = 457
        Top = 4
        ImageIndex = 8
        OnClick = RzToolButton15Click
      end
      object RzSpacer7: TRzSpacer
        Left = 265
        Top = 4
        Width = 16
      end
      object RzSpacer8: TRzSpacer
        Left = 402
        Top = 4
      end
      object RzToolButton2: TRzToolButton
        Left = 313
        Top = 4
        ImageIndex = 0
        OnClick = RzToolButton2Click
      end
      object RzSpacer11: TRzSpacer
        Left = 449
        Top = 4
      end
      object RzToolButton6: TRzToolButton
        Left = 410
        Top = 4
        Width = 39
        DropDownMenu = Print
        ImageIndex = 5
        ToolStyle = tsDropDown
      end
      object cxLookupComboBox2: TcxLookupComboBox
        Left = 76
        Top = 6
        RepositoryItem = DM.SignNameBox
        Properties.DropDownListStyle = lsFixedList
        Properties.ImmediateDropDownWhenActivated = True
        Properties.ListColumns = <>
        TabOrder = 0
        OnEnter = cxLookupComboBox2Enter
        OnMouseDown = cxLookupComboBox2MouseDown
        Width = 189
      end
      object cxLabel2: TcxLabel
        Left = 12
        Top = 8
        Caption = #24448#26469#21333#20301#65306
        Transparent = True
      end
    end
    object RzPageControl1: TRzPageControl
      Left = 1
      Top = 32
      Width = 779
      Height = 422
      Hint = ''
      ActivePage = TabSheet1
      Align = alClient
      ParentShowHint = False
      ShowHint = False
      ShowShadow = False
      TabOverlap = -6
      TabIndex = 0
      TabOrder = 1
      TabStyle = tsSquareCorners
      TabWidth = 60
      FixedDimension = 19
      object TabSheet1: TRzTabSheet
        Caption = #32771#21220
        object Grid: TcxGrid
          Left = 0
          Top = 0
          Width = 777
          Height = 398
          Align = alClient
          TabOrder = 0
          object tvWork: TcxGridDBTableView
            OnKeyDown = tvWorkKeyDown
            Navigator.Buttons.CustomButtons = <>
            OnEditing = tvWorkEditing
            OnEditKeyDown = tvWorkEditKeyDown
            OnEditValueChanged = tvWorkEditValueChanged
            DataController.DataSource = DataWork
            DataController.Filter.AutoDataSetFilter = True
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <
              item
                Kind = skCount
                Column = tvWorkCol1
              end>
            DataController.Summary.SummaryGroups = <>
            FilterRow.Visible = True
            OptionsBehavior.FocusCellOnTab = True
            OptionsBehavior.FocusFirstCellOnNewRecord = True
            OptionsBehavior.GoToNextCellOnEnter = True
            OptionsBehavior.FocusCellOnCycle = True
            OptionsData.CancelOnExit = False
            OptionsData.Deleting = False
            OptionsData.DeletingConfirmation = False
            OptionsSelection.InvertSelect = False
            OptionsSelection.MultiSelect = True
            OptionsSelection.CellMultiSelect = True
            OptionsView.DataRowHeight = 26
            OptionsView.Footer = True
            OptionsView.GroupByBox = False
            OptionsView.HeaderHeight = 23
            OptionsView.Indicator = True
            OptionsView.IndicatorWidth = 20
            object tvWorkCol1: TcxGridDBColumn
              Caption = #24207#21495
              PropertiesClassName = 'TcxTextEditProperties'
              Properties.Alignment.Horz = taCenter
              OnGetDisplayText = tvWorkCol1GetDisplayText
              HeaderAlignmentHorz = taCenter
              Options.Filtering = False
              Width = 40
            end
            object tvWorkCol2: TcxGridDBColumn
              Caption = #21171#21153#20844#21496
              DataBinding.FieldName = 'SignName'
              RepositoryItem = DM.SignNameBox
              HeaderAlignmentHorz = taCenter
              Width = 101
            end
            object tvWorkCol3: TcxGridDBColumn
              Caption = #26085#26399
              DataBinding.FieldName = 'InputDate'
              HeaderAlignmentHorz = taCenter
              Width = 65
            end
            object tvWorkCol4: TcxGridDBColumn
              Caption = #24615#21035
              DataBinding.FieldName = 'sex'
              PropertiesClassName = 'TcxComboBoxProperties'
              Properties.ImmediateDropDownWhenActivated = True
              OnGetPropertiesForEdit = tvWorkCol4GetPropertiesForEdit
              HeaderAlignmentHorz = taCenter
              Width = 65
            end
            object tvWorkColumn2: TcxGridDBColumn
              Caption = #24448#26469#21333#20301
              DataBinding.FieldName = 'WrokCompany'
              Width = 93
            end
            object tvWorkColumn3: TcxGridDBColumn
              Caption = #24037#31181
              DataBinding.FieldName = 'WrokType'
              RepositoryItem = DM.WorkType
              Width = 60
            end
            object tvWorkCol5: TcxGridDBColumn
              Caption = #37096#38376
              DataBinding.FieldName = 'section'
              PropertiesClassName = 'TcxComboBoxProperties'
              Properties.ImmediateDropDownWhenActivated = True
              OnGetPropertiesForEdit = tvWorkCol5GetPropertiesForEdit
              HeaderAlignmentHorz = taCenter
              Width = 65
            end
            object tvWorkCol6: TcxGridDBColumn
              Caption = #32844#21153
              DataBinding.FieldName = 'WorkType'
              PropertiesClassName = 'TcxComboBoxProperties'
              Properties.ImmediateDropDownWhenActivated = True
              OnGetPropertiesForEdit = tvWorkCol6GetPropertiesForEdit
              HeaderAlignmentHorz = taCenter
              Width = 65
            end
            object tvWorkCol7: TcxGridDBColumn
              Caption = #21333#20215'/'#24037#26085
              DataBinding.FieldName = 'WorkPrice'
              PropertiesClassName = 'TcxTextEditProperties'
              HeaderAlignmentHorz = taCenter
              Width = 80
            end
            object tvWorkCol8: TcxGridDBColumn
              Caption = #21333#20215'/'#21152#29677
              DataBinding.FieldName = 'OverPrice'
              PropertiesClassName = 'TcxTextEditProperties'
              HeaderAlignmentHorz = taCenter
              Width = 59
            end
            object tvWorkCol9: TcxGridDBColumn
              Caption = #22791#27880
              DataBinding.FieldName = 'Remarks'
              Width = 153
            end
            object tvWorkColumn1: TcxGridDBColumn
              DataBinding.FieldName = 'Id'
              Visible = False
            end
          end
          object Lv0: TcxGridLevel
            GridView = tvWork
          end
        end
      end
      object TabSheet2: TRzTabSheet
        Caption = #26448#26009
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object MakingGrid: TcxGrid
          Left = 0
          Top = 0
          Width = 777
          Height = 398
          Align = alClient
          TabOrder = 0
          object tvMaking: TcxGridDBTableView
            OnKeyDown = tvMakingKeyDown
            Navigator.Buttons.CustomButtons = <>
            OnEditing = tvMakingEditing
            OnEditKeyDown = tvMakingEditKeyDown
            OnEditValueChanged = tvMakingEditValueChanged
            DataController.DataSource = DataMaking
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <
              item
                Kind = skCount
                Column = tvMakingCol1
              end>
            DataController.Summary.SummaryGroups = <>
            FilterRow.Visible = True
            OptionsBehavior.FocusCellOnTab = True
            OptionsBehavior.FocusFirstCellOnNewRecord = True
            OptionsBehavior.GoToNextCellOnEnter = True
            OptionsBehavior.FocusCellOnCycle = True
            OptionsData.CancelOnExit = False
            OptionsData.Deleting = False
            OptionsData.DeletingConfirmation = False
            OptionsSelection.InvertSelect = False
            OptionsSelection.MultiSelect = True
            OptionsSelection.CellMultiSelect = True
            OptionsView.DataRowHeight = 26
            OptionsView.Footer = True
            OptionsView.GroupByBox = False
            OptionsView.HeaderHeight = 23
            OptionsView.Indicator = True
            OptionsView.IndicatorWidth = 20
            object tvMakingCol1: TcxGridDBColumn
              Caption = #24207#21495
              PropertiesClassName = 'TcxTextEditProperties'
              Properties.Alignment.Horz = taCenter
              OnGetDisplayText = tvWorkCol1GetDisplayText
              HeaderAlignmentHorz = taCenter
              Options.Filtering = False
              Options.Sorting = False
              Width = 40
            end
            object tvMakingCol2: TcxGridDBColumn
              Caption = #24448#26469#21333#20301
              DataBinding.FieldName = 'SignName'
              RepositoryItem = DM.SignNameBox
              HeaderAlignmentHorz = taCenter
              Width = 80
            end
            object tvMakingCol3: TcxGridDBColumn
              Caption = #21517#31216
              DataBinding.FieldName = 'MakingCaption'
              PropertiesClassName = 'TcxLookupComboBoxProperties'
              Properties.DropDownListStyle = lsEditList
              Properties.DropDownWidth = 300
              Properties.ImmediateDropDownWhenActivated = True
              Properties.KeyFieldNames = 'MakingCaption'
              Properties.ListColumns = <
                item
                  Caption = #26448#26009#21517#31216
                  Width = 70
                  FieldName = 'MakingCaption'
                end
                item
                  Caption = #22411#21495
                  Width = 40
                  FieldName = 'ModelIndex'
                end
                item
                  Caption = #35268#26684
                  Width = 40
                  FieldName = 'spec'
                end
                item
                  Caption = #21697#29260
                  Width = 40
                  FieldName = 'Brand'
                end>
              Properties.ListSource = DataList
              Properties.OnCloseUp = tvMakingCol3PropertiesCloseUp
              Properties.OnPopup = tvMakingCol3PropertiesPopup
              HeaderAlignmentHorz = taCenter
              Width = 80
            end
            object tvMakingCol4: TcxGridDBColumn
              Caption = #26085#26399
              DataBinding.FieldName = 'InputDate'
              HeaderAlignmentHorz = taCenter
              Width = 77
            end
            object tvMakingCol5: TcxGridDBColumn
              Caption = #22411#21495
              DataBinding.FieldName = 'ModelIndex'
              PropertiesClassName = 'TcxTextEditProperties'
              Properties.Alignment.Horz = taCenter
              HeaderAlignmentHorz = taCenter
              Width = 40
            end
            object tvMakingCol6: TcxGridDBColumn
              Caption = #35268#26684
              DataBinding.FieldName = 'spec'
              PropertiesClassName = 'TcxTextEditProperties'
              Properties.Alignment.Horz = taCenter
              HeaderAlignmentHorz = taCenter
              Width = 40
            end
            object tvMakingCol7: TcxGridDBColumn
              Caption = #21697#29260
              DataBinding.FieldName = 'Brand'
              PropertiesClassName = 'TcxTextEditProperties'
              Properties.Alignment.Horz = taCenter
              HeaderAlignmentHorz = taCenter
              Width = 85
            end
            object tvMakingCol8: TcxGridDBColumn
              Caption = #21333#20301
              DataBinding.FieldName = 'Company'
              PropertiesClassName = 'TcxComboBoxProperties'
              Properties.ImmediateDropDownWhenActivated = True
              OnGetPropertiesForEdit = tvMakingCol8GetPropertiesForEdit
              HeaderAlignmentHorz = taCenter
              Width = 50
            end
            object tvMakingCol9: TcxGridDBColumn
              Caption = #21333#20215
              DataBinding.FieldName = 'UnitPrice'
              PropertiesClassName = 'TcxCurrencyEditProperties'
              Properties.Alignment.Horz = taCenter
              HeaderAlignmentHorz = taCenter
              Width = 77
            end
            object tvMakingCol10: TcxGridDBColumn
              Caption = #22791#27880
              Width = 139
            end
            object tvMakingCol11: TcxGridDBColumn
              DataBinding.FieldName = 'Id'
              Visible = False
            end
          end
          object Lv1: TcxGridLevel
            GridView = tvMaking
          end
        end
      end
      object TabSheet3: TRzTabSheet
        Caption = #20998#21253
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object cxGrid2: TcxGrid
          Left = 0
          Top = 0
          Width = 777
          Height = 398
          Align = alClient
          TabOrder = 0
          object tvSubContract: TcxGridDBTableView
            OnKeyDown = tvSubContractKeyDown
            Navigator.Buttons.CustomButtons = <>
            OnEditing = tvSubContractEditing
            OnEditKeyDown = tvSubContractEditKeyDown
            OnEditValueChanged = tvSubContractEditValueChanged
            DataController.DataSource = DataSubContract
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <
              item
                Kind = skCount
                Column = tvSubContractCol1
              end>
            DataController.Summary.SummaryGroups = <>
            FilterRow.Visible = True
            OptionsBehavior.FocusCellOnTab = True
            OptionsBehavior.FocusFirstCellOnNewRecord = True
            OptionsBehavior.GoToNextCellOnEnter = True
            OptionsBehavior.FocusCellOnCycle = True
            OptionsData.CancelOnExit = False
            OptionsData.Deleting = False
            OptionsData.DeletingConfirmation = False
            OptionsSelection.InvertSelect = False
            OptionsSelection.MultiSelect = True
            OptionsSelection.CellMultiSelect = True
            OptionsView.DataRowHeight = 26
            OptionsView.Footer = True
            OptionsView.GroupByBox = False
            OptionsView.HeaderHeight = 23
            OptionsView.Indicator = True
            OptionsView.IndicatorWidth = 20
            object tvSubContractCol1: TcxGridDBColumn
              Caption = #24207#21495
              PropertiesClassName = 'TcxTextEditProperties'
              Properties.Alignment.Horz = taCenter
              OnGetDisplayText = tvWorkCol1GetDisplayText
              HeaderAlignmentHorz = taCenter
              Options.Filtering = False
              Options.Sorting = False
              Width = 40
            end
            object tvSubContractColumn1: TcxGridDBColumn
              DataBinding.FieldName = 'Id'
              Visible = False
            end
            object tvSubContractCol2: TcxGridDBColumn
              Caption = #24448#26469#21333#20301
              DataBinding.FieldName = 'SignName'
              RepositoryItem = DM.SignNameBox
              HeaderAlignmentHorz = taCenter
              Width = 90
            end
            object tvSubContractCol3: TcxGridDBColumn
              Caption = #26085#26399
              DataBinding.FieldName = 'InputDate'
              HeaderAlignmentHorz = taCenter
              Width = 70
            end
            object tvSubContractCol4: TcxGridDBColumn
              Caption = #24037#31181
              DataBinding.FieldName = 'WorkType'
              PropertiesClassName = 'TcxComboBoxProperties'
              Properties.ImmediateDropDownWhenActivated = True
              OnGetPropertiesForEdit = tvSubContractCol4GetPropertiesForEdit
              HeaderAlignmentHorz = taCenter
              Width = 65
            end
            object tvSubContractCol5: TcxGridDBColumn
              Caption = #20998#21253#20869#23481
              DataBinding.FieldName = 'content'
              PropertiesClassName = 'TcxTextEditProperties'
              HeaderAlignmentHorz = taCenter
              Width = 120
            end
            object tvSubContractCol6: TcxGridDBColumn
              Caption = #26045#24037#24037#33402
              DataBinding.FieldName = 'technics'
              PropertiesClassName = 'TcxTextEditProperties'
              HeaderAlignmentHorz = taCenter
              Width = 120
            end
            object tvSubContractCol7: TcxGridDBColumn
              Caption = #21333#20301
              DataBinding.FieldName = 'unit'
              PropertiesClassName = 'TcxComboBoxProperties'
              Properties.ImmediateDropDownWhenActivated = True
              OnGetPropertiesForEdit = tvSubContractCol7GetPropertiesForEdit
              HeaderAlignmentHorz = taCenter
              Width = 50
            end
            object tvSubContractCol8: TcxGridDBColumn
              Caption = #21333#20215
              DataBinding.FieldName = 'UnitPrice'
              PropertiesClassName = 'TcxCurrencyEditProperties'
              HeaderAlignmentHorz = taCenter
              Width = 70
            end
            object tvSubContractCol9: TcxGridDBColumn
              Caption = #22791#27880
              DataBinding.FieldName = 'Remarks'
              Width = 119
            end
          end
          object Lv2: TcxGridLevel
            GridView = tvSubContract
          end
        end
      end
      object TabSheet4: TRzTabSheet
        Caption = #21830#28151
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object GridConcrete: TcxGrid
          Left = 0
          Top = 0
          Width = 777
          Height = 398
          Align = alClient
          TabOrder = 0
          object tvConcrete: TcxGridDBTableView
            Navigator.Buttons.CustomButtons = <>
            OnEditing = tvConcreteEditing
            OnEditKeyDown = tvConcreteEditKeyDown
            DataController.DataSource = DataConcrete
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <>
            DataController.Summary.SummaryGroups = <>
            FilterRow.Visible = True
            OptionsBehavior.FocusCellOnTab = True
            OptionsBehavior.FocusFirstCellOnNewRecord = True
            OptionsBehavior.GoToNextCellOnEnter = True
            OptionsBehavior.FocusCellOnCycle = True
            OptionsData.CancelOnExit = False
            OptionsData.Deleting = False
            OptionsData.DeletingConfirmation = False
            OptionsSelection.InvertSelect = False
            OptionsSelection.MultiSelect = True
            OptionsSelection.CellMultiSelect = True
            OptionsView.DataRowHeight = 23
            OptionsView.GroupByBox = False
            OptionsView.HeaderHeight = 25
            OptionsView.Indicator = True
            OptionsView.IndicatorWidth = 15
            object tvConcreteCol1: TcxGridDBColumn
              Caption = #24207#21495
              PropertiesClassName = 'TcxTextEditProperties'
              Properties.Alignment.Horz = taCenter
              OnGetDisplayText = tvWorkCol1GetDisplayText
              HeaderAlignmentHorz = taCenter
              Options.Filtering = False
              Options.Sorting = False
              Width = 40
            end
            object tvConcreteCol2: TcxGridDBColumn
              Caption = #24448#26469#21333#20301
              DataBinding.FieldName = 'SignName'
              RepositoryItem = DM.SignNameBox
              HeaderAlignmentHorz = taCenter
              Width = 181
            end
            object tvConcreteCol3: TcxGridDBColumn
              Caption = #31867#21035#21517#31216
              DataBinding.FieldName = 'CaptionType'
              PropertiesClassName = 'TcxComboBoxProperties'
              Properties.DropDownListStyle = lsFixedList
              Properties.ImmediateDropDownWhenActivated = True
              Properties.OnChange = tvConcreteCol3PropertiesChange
              OnGetPropertiesForEdit = tvConcreteCol3GetPropertiesForEdit
              HeaderAlignmentHorz = taCenter
              Width = 139
            end
            object tvConcreteCol4: TcxGridDBColumn
              Caption = #35268#26684
              DataBinding.FieldName = 'spec'
              PropertiesClassName = 'TcxComboBoxProperties'
              Properties.Alignment.Horz = taCenter
              Properties.DropDownListStyle = lsFixedList
              Properties.ImmediateDropDownWhenActivated = True
              HeaderAlignmentHorz = taCenter
              Width = 74
            end
            object tvConcreteCol5: TcxGridDBColumn
              Caption = #21333#20215
              DataBinding.FieldName = 'UnitPrice'
              PropertiesClassName = 'TcxCurrencyEditProperties'
              HeaderAlignmentHorz = taCenter
              Width = 73
            end
          end
          object Lv3: TcxGridLevel
            GridView = tvConcrete
          end
        end
      end
    end
  end
  object RzPanel1: TRzPanel
    Left = 0
    Top = 28
    Width = 185
    Height = 454
    Align = alLeft
    BorderInner = fsFlat
    BorderOuter = fsNone
    BorderSides = [sdRight]
    TabOrder = 1
    object Tv1: TTreeView
      Left = 0
      Top = 0
      Width = 184
      Height = 454
      Align = alClient
      BevelInner = bvNone
      BevelOuter = bvNone
      DragMode = dmAutomatic
      HideSelection = False
      Images = DM.il1
      Indent = 19
      RowSelect = True
      TabOrder = 0
      OnChange = Tv1Change
      OnEditing = Tv1Editing
      Items.NodeData = {
        0301000000260000000000000000000000FFFFFFFFFFFFFFFF00000000000000
        0004000000010406527B7CA17B0674260000000000000000000000FFFFFFFFFF
        FFFFFF0000000000000000000000000104B352A152A17B067426000000000000
        0000000000FFFFFFFFFFFFFFFF000000000000000000000000010450679965A1
        7B0674260000000000000000000000FFFFFFFFFFFFFFFF000000000000000000
        000000010406520553A17B0674260000000000000000000000FFFFFFFFFFFFFF
        FF00000000000000000000000001044655F76DA17B0674}
    end
  end
  object RzPanel5: TRzPanel
    Left = 0
    Top = 0
    Width = 975
    Height = 28
    Align = alTop
    BorderOuter = fsNone
    TabOrder = 2
    object RzToolbar1: TRzToolbar
      Left = 0
      Top = 0
      Width = 975
      Height = 29
      AutoStyle = False
      BorderInner = fsFlat
      BorderOuter = fsNone
      BorderSides = [sdTop]
      BorderWidth = 0
      GradientColorStyle = gcsCustom
      TabOrder = 0
      VisualStyle = vsGradient
      ToolbarControls = (
        RzSpacer1
        cxLabel1
        cxLookupComboBox1
        RzSpacer2
        RzToolButton1
        RzSpacer6
        RzPanel3)
      object RzSpacer1: TRzSpacer
        Left = 4
        Top = 2
        Width = 23
      end
      object RzSpacer2: TRzSpacer
        Left = 282
        Top = 2
      end
      object RzToolButton1: TRzToolButton
        Left = 290
        Top = 2
        ImageIndex = 10
        Images = DM.cxImageList1
        OnClick = RzToolButton1Click
      end
      object RzSpacer6: TRzSpacer
        Left = 315
        Top = 2
      end
      object cxLabel1: TcxLabel
        Left = 27
        Top = 6
        Caption = #39033#30446#21517#31216':'
        Transparent = True
      end
      object cxLookupComboBox1: TcxLookupComboBox
        Left = 83
        Top = 4
        Properties.ImmediateDropDownWhenActivated = True
        Properties.KeyFieldNames = 'SignName'
        Properties.ListColumns = <
          item
            Caption = #24037#31243#21517#31216
            FieldName = 'SignName'
          end
          item
            Caption = #31616#30721
            FieldName = 'ProjectPYCode'
          end>
        Properties.ListOptions.GridLines = glVertical
        Properties.ListOptions.ShowHeader = False
        Properties.ListOptions.SyncMode = True
        Properties.ListSource = DataSource1
        Properties.OnChange = cxLookupComboBox1PropertiesChange
        Properties.OnCloseUp = cxLookupComboBox1PropertiesCloseUp
        TabOrder = 1
        OnEnter = cxLookupComboBox1Enter
        OnMouseDown = cxLookupComboBox1MouseDown
        Width = 199
      end
      object RzPanel3: TRzPanel
        Left = 323
        Top = -6
        Width = 630
        Height = 41
        Align = alClient
        BorderOuter = fsNone
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 2
        Transparent = True
      end
    end
  end
  object dxStatusBar1: TdxStatusBar
    Left = 0
    Top = 482
    Width = 975
    Height = 20
    Panels = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
  end
  object ADOMaking: TADOQuery
    Connection = DM.ADOconn
    AfterOpen = ADOMakingAfterOpen
    AfterInsert = ADOMakingAfterInsert
    CommandTimeout = 1
    Parameters = <>
    Left = 608
    Top = 232
  end
  object DataMaking: TDataSource
    DataSet = ADOMaking
    Left = 608
    Top = 280
  end
  object ADOList: TADOQuery
    Connection = DM.ADOconn
    Parameters = <>
    Left = 528
    Top = 224
  end
  object DataList: TDataSource
    DataSet = ADOList
    Left = 528
    Top = 280
  end
  object ADOSign: TADOQuery
    Connection = DM.ADOconn
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    Parameters = <>
    Left = 272
    Top = 152
  end
  object DataSign: TDataSource
    DataSet = ADOSign
    Left = 328
    Top = 152
  end
  object ADOCompany: TADOQuery
    Connection = DM.ADOconn
    CursorType = ctStatic
    Parameters = <>
    Left = 440
    Top = 224
  end
  object DataCompany: TDataSource
    DataSet = ADOCompany
    Left = 440
    Top = 280
  end
  object ADOWork: TADOQuery
    Connection = DM.ADOconn
    AfterOpen = ADOWorkAfterOpen
    AfterInsert = ADOWorkAfterInsert
    CommandTimeout = 1
    Parameters = <>
    Left = 72
    Top = 248
  end
  object DataWork: TDataSource
    DataSet = ADOWork
    Left = 72
    Top = 304
  end
  object ADOSubContract: TADOQuery
    Connection = DM.ADOconn
    AfterOpen = ADOSubContractAfterOpen
    AfterInsert = ADOSubContractAfterInsert
    CommandTimeout = 1
    Parameters = <>
    Left = 272
    Top = 224
  end
  object DataSubContract: TDataSource
    DataSet = ADOSubContract
    Left = 272
    Top = 272
  end
  object ADOQuery1: TADOQuery
    Connection = DM.ADOconn
    Parameters = <>
    Left = 136
    Top = 248
  end
  object DataSource1: TDataSource
    DataSet = ADOQuery1
    Left = 136
    Top = 304
  end
  object ADOConcrete: TADOQuery
    Connection = DM.ADOconn
    AfterOpen = ADOConcreteAfterOpen
    AfterInsert = ADOConcreteAfterInsert
    Parameters = <>
    Left = 432
    Top = 360
  end
  object DataConcrete: TDataSource
    DataSet = ADOConcrete
    Left = 432
    Top = 408
  end
  object ADOTypeName: TADOQuery
    Connection = DM.ADOconn
    Parameters = <>
    Left = 528
    Top = 360
  end
  object DataTypeName: TDataSource
    DataSet = ADOTypeName
    Left = 528
    Top = 416
  end
  object Print: TPopupMenu
    Images = DM.cxImageList1
    Left = 688
    Top = 136
    object Excel1: TMenuItem
      Caption = #36755#20986'Excel'
      OnClick = Excel1Click
    end
    object N2: TMenuItem
      Caption = #25171#21360#25253#34920
      ImageIndex = 6
      OnClick = N2Click
    end
  end
end
