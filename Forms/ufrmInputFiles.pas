unit ufrmInputFiles;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.Mask, RzEdit, Vcl.ComCtrls,TreeUtils,
  RzButton, Vcl.StdCtrls;

type
  TfrmInputFiles = class(TForm)
    GroupBox1: TGroupBox;
    Label12: TLabel;
    Label10: TLabel;
    Label9: TLabel;
    RzMemo1: TRzMemo;
    RzBitBtn1: TRzBitBtn;
    RzBitBtn2: TRzBitBtn;
    dtp1: TDateTimePicker;
    RzEdit1: TRzEdit;
    procedure FormActivate(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure RzBitBtn1Click(Sender: TObject);
    procedure RzBitBtn1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure RzBitBtn2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    g_ProjectDir : string;
    g_PatTree  : TTreeUtils;
    g_IsModify : Boolean;
    g_PostCode : string;
    g_TableName: string;
  end;

var
  frmInputFiles: TfrmInputFiles;

implementation

uses
   uDataModule,global;

{$R *.dfm}

procedure TfrmInputFiles.FormActivate(Sender: TObject);
begin
  if not g_IsModify then
  begin
    g_PostCode := DM.getDataMaxDate(g_TableName);
  end;
end;

procedure TfrmInputFiles.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  case Key of
    13:
    begin
      Self.RzBitBtn1.Click;
    end;
    27:
    begin
      Close;
    end;
  end;
end;

procedure TfrmInputFiles.RzBitBtn1Click(Sender: TObject);
var
  szCaption:string;
  szRemarks:string;
  i : Integer;
  m_title : PWideChar;
begin
  m_title := '提示:';
  if Length(g_PostCode) = 0 then
  begin
    Application.MessageBox('请输入合同编号!',m_title,MB_OK + MB_ICONQUESTION)
  end
  else
  begin

    szCaption := Self.RzEdit1.Text;
    if Length(szCaption) = 0 then
    begin
      Application.MessageBox('请输入合同名称!',m_title,MB_OK + MB_ICONQUESTION)
    end
    else
    begin
      szRemarks := Self.RzMemo1.Text;
      if g_IsModify then
      begin
        //编辑
        if g_PatTree.ModifyNodeData(szCaption,g_PostCode,0,0,0,'',szRemarks,dtp1.Date,Now,g_PatTree.getTreeData) then
           Application.MessageBox('修改成功!',m_title,MB_OK + MB_ICONQUESTION)
        else
           Application.MessageBox('修改失败!',m_title,MB_OK + MB_ICONQUESTION)

      end else
      begin
        //新增
        i := DM.SelectCode(g_TableName,g_PostCode);
        if i <> 0 then
        begin
          Application.MessageBox('合同编号已存在',m_title,MB_OK + MB_ICONQUESTION) ;
        end else
        begin

          if g_PatTree.AddChildNode( g_PostCode,szRemarks,0,0,0,'',dtp1.Date,Now,g_pacttype,szCaption) then
          begin
            Self.FormActivate(Sender);
            Application.MessageBox('新增成功!',m_title,MB_OK + MB_ICONQUESTION)
          end
          else
            Application.MessageBox('新增失败!',m_title,MB_OK + MB_ICONQUESTION)

        end;


      end;

    end;

  end;

end;

procedure TfrmInputFiles.RzBitBtn1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = 27 then
  begin
    Self.RzBitBtn1.Click;
  end;
end;

procedure TfrmInputFiles.RzBitBtn2Click(Sender: TObject);
begin
  Close;
end;

end.


