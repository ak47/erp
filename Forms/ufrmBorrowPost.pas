unit ufrmBorrowPost;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, RzEdit, ComCtrls, Mask, RzButton, RzTabs,TreeFillThrd,TreeUtils,
  ExtCtrls, RzPanel, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, dxSkinsCore,
  dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  cxTextEdit, cxDBEdit, cxMemo, cxCurrencyEdit, cxMaskEdit, cxDropDownEdit,
  cxCalendar, DB, ADODB, Menus, cxButtons, RzBckgnd, cxCalc, cxClasses, ImgList,
  RzLabel, dxSkinBlueprint, dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle,
  dxSkinHighContrast, dxSkinMetropolis, dxSkinMetropolisDark,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2013White, dxSkinOffice2016Colorful,
  dxSkinOffice2016Dark, dxSkinSevenClassic, dxSkinSharpPlus,
  dxSkinTheAsphaltWorld, dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxSkinVS2010, dxSkinWhiteprint;

type
  TfrmBorrowPost = class(TForm)
    RzPanel3: TRzPanel;
    GroupBox3: TGroupBox;
    Label19: TLabel;
    Label3: TLabel;
    Label13: TLabel;
    Label1: TLabel;
    Label33: TLabel;
    Label34: TLabel;
    Label35: TLabel;
    Label37: TLabel;
    Label38: TLabel;
    RzBitBtn2: TRzBitBtn;
    RzBitBtn3: TRzBitBtn;
    AddStartDate: TDateTimePicker;
    RzBitBtn7: TRzBitBtn;
    cxCurrencyEdit2: TcxCurrencyEdit;
    cxCurrencyEdit1: TcxCurrencyEdit;
    cxTextEdit2: TcxTextEdit;
    cxMemo1: TcxMemo;
    cxMemo2: TcxMemo;
    procedure FormCreate(Sender: TObject);
    procedure RzBitBtn2Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure RzBitBtn3Click(Sender: TObject);
    procedure cxCurrencyEdit1PropertiesChange(Sender: TObject);
    procedure RzBitBtn7Click(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
    g_ProjectDir   : string;
    g_PostCode     : string;
    g_PostNumbers  : string;
    g_IsModifyFile : Boolean;
  end;

var
  frmBorrowPost: TfrmBorrowPost;
  g_TableName  : string = 'sk_BorrowMoney';


implementation

uses
   global,ufrmBorrowMoney,uDataModule,DateUtils;

{$R *.dfm}

procedure TfrmBorrowPost.cxCurrencyEdit1PropertiesChange(Sender: TObject);
begin
  Self.Label38.Caption := MoneyConvert(Self.cxCurrencyEdit1.Value);
end;

procedure TfrmBorrowPost.FormCreate(Sender: TObject);
begin
  Self.Label38.Caption  := '';
end;

procedure TfrmBorrowPost.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = 27 then Close;
end;

procedure TfrmBorrowPost.FormShow(Sender: TObject);
var
  Node: PNodeData;

begin

  Node := frmBorrowMoney.g_PatTree.getNodeData;
  if Assigned(Node) then
  begin
    g_PostCode := Node^.Code;
    Self.cxTextEdit2.Text := Node^.Caption;
  end;
  g_PostNumbers := DM.getDataMaxDate(g_TableName);
  Self.AddStartDate.Date := Date;

end;

procedure TfrmBorrowPost.RzBitBtn2Click(Sender: TObject);
var
  s : string;
  szCaption: string;
  szContent : string;
  szRemarks: string;
  pData : PPaytype;
  szPrincipal : Currency; //本金
  szRate : Currency; //利率
  szDate : TDateTime;//起始日期
  szLoanName : string; //借款人/单位

begin
  if Length(g_PostCode) = 0 then
  begin
    Application.MessageBox('请选择合同',m_title,MB_OK + MB_ICONHAND);
    Close;
  end
  else
  begin
    if DM.IsNumbers(g_TableName,g_PostNumbers) then
    begin
       Application.MessageBox('编号已存在',m_title,MB_OK + MB_ICONHAND);
       Exit;
    end else
    begin
      szPrincipal := Self.cxCurrencyEdit1.Value;
      if szPrincipal = 0 then
      begin
        Application.MessageBox('请输入本金!!',m_title,MB_OK + MB_ICONHAND);
      end
      else
      begin
        szRate     := Self.cxCurrencyEdit2.value;
      
        szCaption  := Self.cxTextEdit2.Text;  //借款人或单位
        szContent  := Self.cxMemo1.Text;      //借款事由
        szRemarks  := Self.cxMemo2.Text;      //支付内容
        szDate     := Self.AddStartDate.Date;

        s := Format( 'Insert into '+ g_TableName +' (code,'+
                                                    'numbers,'+
                                                    'chPrincipal,' +
                                                    'chRate,' +
                                                    'chStartDate,' +
                                                    'chEndDate,' +
                                                    'chName,'  +
                                                    'chContent,' +
                                                    'chRemarks,'+
                                                    'PatType' +
                                                    ') values("%s","%s",%f,%f,"%s","%s","%s","%s","%s",%d)',[
                                                                g_PostCode,
                                                                g_PostNumbers,
                                                                szPrincipal,
                                                                szRate,
                                                                FormatDateTime('yyyy-MM-dd',szDate),
                                                                FormatDateTime('yyyy-MM-dd',szDate),
                                                                szCaption,
                                                                szContent,
                                                                szRemarks,
                                                                g_pacttype
                                                                ]);

        OutputLog(s);
         with DM.Qry do
         begin
           Close;
           SQL.Clear;
           SQL.Text := s;
           if ExecSQL <> 0 then
           begin
             m_IsModify := True;
             Self.FormShow(Sender);
             Application.MessageBox('款项添加成功!',m_title,MB_OK + MB_ICONQUESTION);
           end
           else
           begin
             Application.MessageBox('款项添加失败!',m_title,MB_OK + MB_ICONQUESTION);
           end;
         end;
      end;

    end;
  end;
end;

procedure TfrmBorrowPost.RzBitBtn3Click(Sender: TObject);
begin
  Close;
end;

procedure TfrmBorrowPost.RzBitBtn7Click(Sender: TObject);
var
  s : string;
begin
  //打开目录
  s := Concat(g_ProjectDir,'\' + g_PostNumbers);
  CreateOpenDir(Handle,s,True);
end;

end.
