object frmCompleteManage: TfrmCompleteManage
  Left = 0
  Top = 0
  Caption = #31459#24037#31649#29702
  ClientHeight = 482
  ClientWidth = 1045
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter
    Left = 265
    Top = 0
    Width = 10
    Height = 482
    Color = 16250613
    ParentColor = False
    ResizeStyle = rsUpdate
    ExplicitLeft = 178
    ExplicitHeight = 335
  end
  object RzPanel1: TRzPanel
    Left = 275
    Top = 0
    Width = 770
    Height = 482
    Align = alClient
    BorderOuter = fsFlat
    Color = 16250613
    TabOrder = 0
    object RzToolbar12: TRzToolbar
      Left = 1
      Top = 1
      Width = 768
      Height = 29
      AutoStyle = False
      Images = DM.cxImageList1
      BorderInner = fsNone
      BorderOuter = fsNone
      BorderSides = [sdBottom]
      BorderWidth = 0
      GradientColorStyle = gcsCustom
      TabOrder = 0
      VisualStyle = vsGradient
      ToolbarControls = (
        RzSpacer100
        RzToolButton75
        RzSpacer101
        RzToolButton76
        RzSpacer102
        RzToolButton77
        RzSpacer103
        cxLabel50
        cxDateEdit1
        RzSpacer107
        cxLabel51
        cxDateEdit2
        RzSpacer110
        RzToolButton81
        RzSpacer106
        RzToolButton80
        RzSpacer109)
      object RzSpacer100: TRzSpacer
        Left = 4
        Top = 2
      end
      object RzToolButton75: TRzToolButton
        Left = 12
        Top = 2
        Width = 65
        SelectionColorStop = 16119543
        SelectionFrameColor = clBtnShadow
        ImageIndex = 39
        ShowCaption = True
        UseToolbarButtonSize = False
        UseToolbarShowCaption = False
        Caption = #28155#21152
        OnClick = RzToolButton75Click
      end
      object RzSpacer101: TRzSpacer
        Left = 77
        Top = 2
      end
      object RzToolButton76: TRzToolButton
        Left = 85
        Top = 2
        Width = 67
        SelectionColorStop = 16119543
        SelectionFrameColor = clBtnShadow
        ImageIndex = 3
        ShowCaption = True
        UseToolbarButtonSize = False
        UseToolbarShowCaption = False
        Caption = #20445#23384
        OnClick = RzToolButton76Click
      end
      object RzSpacer102: TRzSpacer
        Left = 152
        Top = 2
      end
      object RzToolButton77: TRzToolButton
        Left = 160
        Top = 2
        Width = 65
        SelectionColorStop = 16119543
        SelectionFrameColor = clBtnShadow
        ImageIndex = 2
        ShowCaption = True
        UseToolbarButtonSize = False
        UseToolbarShowCaption = False
        Caption = #21024#38500
        OnClick = RzToolButton77Click
      end
      object RzSpacer103: TRzSpacer
        Left = 225
        Top = 2
      end
      object RzToolButton80: TRzToolButton
        Left = 574
        Top = 2
        Width = 65
        SelectionColorStop = 16119543
        SelectionFrameColor = clBtnShadow
        ImageIndex = 8
        ShowCaption = True
        UseToolbarButtonSize = False
        UseToolbarShowCaption = False
        Caption = #36864#20986
        OnClick = RzToolButton80Click
      end
      object RzSpacer106: TRzSpacer
        Left = 566
        Top = 2
      end
      object RzSpacer107: TRzSpacer
        Left = 379
        Top = 2
        Width = 5
      end
      object RzToolButton81: TRzToolButton
        Left = 538
        Top = 2
        Width = 28
        SelectionColorStop = 16119543
        SelectionFrameColor = clBtnShadow
        ImageIndex = 10
        ShowCaption = True
        UseToolbarShowCaption = False
        OnClick = RzToolButton81Click
      end
      object RzSpacer109: TRzSpacer
        Left = 639
        Top = 2
      end
      object RzSpacer110: TRzSpacer
        Left = 530
        Top = 2
      end
      object cxLabel50: TcxLabel
        Left = 233
        Top = 6
        Caption = #36215#22987#26085#26399':'
        Transparent = True
      end
      object cxDateEdit1: TcxDateEdit
        Left = 289
        Top = 4
        TabOrder = 1
        Width = 90
      end
      object cxLabel51: TcxLabel
        Left = 384
        Top = 6
        Caption = #32467#26463#26085#26399':'
        Transparent = True
      end
      object cxDateEdit2: TcxDateEdit
        Left = 440
        Top = 4
        TabOrder = 3
        Width = 90
      end
    end
    object Grid: TcxGrid
      Left = 1
      Top = 30
      Width = 768
      Height = 451
      Align = alClient
      TabOrder = 1
      object TvTableView: TcxGridDBTableView
        OnDblClick = TvTableViewDblClick
        OnKeyDown = TvTableViewKeyDown
        Navigator.Buttons.CustomButtons = <>
        OnEditing = TvTableViewEditing
        DataController.DataSource = DataSource1
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        FilterRow.Visible = True
        OptionsData.Deleting = False
        OptionsData.DeletingConfirmation = False
        OptionsSelection.InvertSelect = False
        OptionsSelection.MultiSelect = True
        OptionsSelection.CellMultiSelect = True
        OptionsView.DataRowHeight = 23
        OptionsView.GroupByBox = False
        OptionsView.HeaderHeight = 26
        OptionsView.Indicator = True
        OptionsView.IndicatorWidth = 20
        object TvCol1: TcxGridDBColumn
          Caption = #24207#21495
          PropertiesClassName = 'TcxTextEditProperties'
          Properties.Alignment.Horz = taCenter
          OnGetDisplayText = TvCol1GetDisplayText
          HeaderAlignmentHorz = taCenter
          Width = 42
        end
        object TvCol2: TcxGridDBColumn
          Caption = #32534#21495
          DataBinding.FieldName = 'Numbers'
          HeaderAlignmentHorz = taCenter
          Width = 156
        end
        object TvCol5: TcxGridDBColumn
          Caption = #26085#26399
          DataBinding.FieldName = 'chDate'
          Width = 96
        end
        object TvCol3: TcxGridDBColumn
          Caption = #24037#31243#21517#31216
          DataBinding.FieldName = 'ProjectName'
          PropertiesClassName = 'TcxTextEditProperties'
          HeaderAlignmentHorz = taCenter
          Width = 181
        end
        object TvCol4: TcxGridDBColumn
          Caption = #36164#36136#21333#20301
          DataBinding.FieldName = 'chContent'
          PropertiesClassName = 'TcxTextEditProperties'
          Width = 139
        end
        object TvTableViewColumn1: TcxGridDBColumn
          Caption = #31867#22411
          DataBinding.FieldName = 'chType'
          PropertiesClassName = 'TcxComboBoxProperties'
          Properties.ImmediateDropDownWhenActivated = True
          OnGetPropertiesForEdit = TvTableViewColumn1GetPropertiesForEdit
          Width = 87
        end
        object TvTableViewColumn2: TcxGridDBColumn
          Caption = #39033#30446#37096
          DataBinding.FieldName = 'chRemarks'
          Width = 192
        end
      end
      object Lv: TcxGridLevel
        GridView = TvTableView
      end
    end
  end
  object RzPanel4: TRzPanel
    Left = 0
    Top = 0
    Width = 265
    Height = 482
    Align = alLeft
    BorderInner = fsFlat
    BorderOuter = fsNone
    BorderSides = [sdLeft, sdRight]
    TabOrder = 1
  end
  object DataSource1: TDataSource
    DataSet = ADOQuery1
    Left = 856
    Top = 544
  end
  object ADOQuery1: TADOQuery
    Connection = DM.ADOconn
    AfterOpen = ADOQuery1AfterOpen
    AfterInsert = ADOQuery1AfterInsert
    Parameters = <>
    Left = 888
    Top = 544
  end
end
